/*
| Central configuration file.
|
| Example for a local development system
*/
import cfg from 'config';

let devel = true;

cfg.admin = 'admin';
cfg.network.main.protocol = 'http';
cfg.shell.bundle.enable = !devel;
cfg.server.sensitive = devel;
cfg.server.cache = false;
cfg.server.report = !devel;
