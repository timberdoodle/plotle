/*
| Adds 'login' and 'signup' actuators to plotle:home.
*/
def.abstract = true;

import { loadConfig } from '../../Server/config/load.js';

import { Self as ChangeTreeGrow } from '{ot:Change/Tree/Grow}';
import { Self as ChangeWrap     } from '{Shared/ChangeWrap/Self}';
import { Self as ChangeWrapList } from '{Shared/ChangeWrap/List}';
import { Self as FabricActuator } from '{Shared/Fabric/Item/Actuator}';
import { Self as Point          } from '{gleam:Point}';
import { Self as Rect           } from '{gleam:Rect}';
import { Self as Repository     } from '{Server/Database/Repository}';
import { Self as Sequence       } from '{ot:Change/Sequence}';
import { Self as TraceRoot      } from '{Shared/Trace/Root}';
import { Self as Uid            } from '{Shared/Session/Uid}';

/*
| Runs the tool.
*/
def.static.run =
	async function( )
{
	await loadConfig( 'src/Server/config/template.js', 'config.js' );

	await Repository.connect( );
	const meta = await Repository.getSpaceMeta( 'spaces:plotle:home' );
	const uidSpace = meta.uid;
	console.log( 'meta:', meta );

	let space;
	let seq = 1;
	{
		console.log( 'replaying space' );
		for(;;)
		{
			const changeSkid = await Repository.getSpaceChange( uidSpace, seq );
			if( changeSkid === 'notFound' )
			{
				break;
			}
			seq++;
			space = changeSkid.changes.changeTree( space );
		}
	}

	console.log( 'adding change #', seq );

	const chgGrowALogin =
		ChangeTreeGrow.create(
			'trace',
				TraceRoot.space( uidSpace ).add( 'items', Uid.newUid( ) ),
			'rank', 0,
			'val',
				FabricActuator.create(
					'name', 'login',
					'zone',
						Rect.PosWidthHeight(
							Point.XY( 900, 705 ),
							180,
							45,
						),
				),
		);

	const chgGrowASignup =
		ChangeTreeGrow.create(
			'trace',
				TraceRoot.space( uidSpace ).add( 'items', Uid.newUid( ) ),
			'rank', 0,
			'val',
				FabricActuator.create(
					'name', 'signup',
					'zone',
						Rect.PosWidthHeight(
							Point.XY( 900, 765 ),
							180,
							45,
						),
				),
		);

	const chgWrap =
		ChangeWrap.Wrapped(
			Sequence.Elements( chgGrowALogin, chgGrowASignup )
		);

	Repository.sendChanges(
		ChangeWrapList.Elements( chgWrap ),
		uidSpace,
		':AddActuators',
		seq,
		Date.now( ),
	);

	console.log( 'DONE' );
};
