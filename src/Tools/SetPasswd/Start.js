/*
| Sets the password of an user.
*/
Error.stackTraceLimit = 15;

global.NODE = true;
global.CHECK = true;
global.TI2C_RETENTION = false;

import * as util from 'node:util';
util.inspect.defaultOptions.depth = null;

await import( 'ti2c' );
await import( 'ti2c-gleam' );
await import( 'ti2c-ot' );
await import( 'ti2c-web' );

const pkg =
	await ti2c.register(
		'name',    'plotle',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', 'Tools/SetPasswd/Start',
		'codegen', 'codegen/'
	);
const Root = await pkg.import( 'Tools/SetPasswd/Root' );
await Root.run( pkg.rootDir );
