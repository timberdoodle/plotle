/*
| Sets the password of an user.
*/
def.abstract = true;

import { read } from 'read';

import { loadConfig } from '../../Server/config/load.js';

import { Self as Repository   } from '{Server/Database/Repository}';
import { Self as UserPassHash } from '{Shared/User/PassHash}';

/*
| Does the init.
*/
def.static.run =
	async function( )
{
	if( process.argv.length < 3 )
	{
		console.log( 'give username as argument' );
	}
	const username = process.argv[ 2 ];

	await loadConfig( 'src/Server/config/template.js', 'config.js' );

	await Repository.connect( );
	let userInfo;
	try
	{
		userInfo = await Repository.getUser( username );
	}
	catch( e )
	{
		console.log( 'user not found' );
		return;
	}

	const passwd = await read( { prompt: 'password: ', silent: true } );
	console.log( );

	userInfo = userInfo.create( 'passhash', UserPassHash.calc( passwd ) );
	await Repository.saveUser( userInfo );
	console.log( 'DONE' );
};
