/*
| Demaps a backtrace from uglified pack.
*/
import { argv, exit }           from 'node:process';
import { readFile }             from 'node:fs/promises';
import { default as SourceMap } from 'source-map';

if( argv.length !== 4 )
{
	console.log(
		'Usage: ' + argv[ 0 ] + ' ' + argv[ 1 ] + ' sourceMap traceFile'
	);
	exit( 0 );
}

const trace = [ ];

let fsTrace = ( await readFile( argv[ 3 ] ) ) + '';
fsTrace = fsTrace.split( /[ \n]/ );

for( let line of fsTrace )
{
	const r = line.match( ':([0-9]*):([0-9]*)' );
	if( !r ) continue;
	trace.push(
		{
			line : parseInt( r[ 1 ], 10 ),
			column : parseInt( r[ 2 ], 10 )
		}
	);
}

let map = await readFile( argv[ 2 ] );
map = JSON.parse( map );
const smc = new SourceMap.SourceMapConsumer( map );

for( let t of trace )
{
	const io = smc.originalPositionFor( t );
	console.log( io.source + ':' + io.line + ':' + io.column );
}
