/*
| User credentials.
*/
def.attributes =
{
	// the username
	name: { type: 'string', json: true },

	// password hash
	passhash: { type: 'string', json: true },
};

def.json = true;

import { Self as Uid } from '{Shared/Session/Uid}';

/*
| Creates the credentials from the local storage.
*/
def.static.createFromLocalStorage =
	function( )
{
	const name = window.localStorage.getItem( 'username' );

	if( !name ) return undefined;

	// if for some buggy reason visitor credentials
	// made it into the local storage ignore that
	if( name && name.substr( 0, 7 ) === 'visitor' ) return undefined;

	return(
		Self.create(
			'name', name,
			'passhash', window.localStorage.getItem( 'passhash' )
		)
	);
};

/*
| Clears the user information from local storage.
*/
def.static.clearLocalStorage =
	function( )
{
	window.localStorage.removeItem( 'username' );
	window.localStorage.removeItem( 'passhash' );
};

/*
| Returns true if this user is a visitor
*/
def.lazy.isVisitor =
	function( )
{
	return this.name.substr( 0, 7 ) === 'visitor';
};

/*
| Creates a visitor user.
*/
def.static.NewVisitor =
	function( )
{
	return(
		Self.create(
			'name', 'visitor',
			'passhash', Uid.newUid( )
		)
	);
};

/*
| Saves this user tim to local storage.
*/
def.proto.saveToLocalStorage =
	function( )
{
	window.localStorage.setItem( 'username', this.name );
	window.localStorage.setItem( 'passhash', this.passhash );
};
