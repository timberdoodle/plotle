/*
| Hashes a password.
*/
def.abstract = true;

import { Self as Sha1 } from '{web:Sha1}';

/*
| Hashes the password.
*/
def.static.calc =
	function( password )
{
	return Sha1.calc( password + '-meshcraft-8833' );
};
