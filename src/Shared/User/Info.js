/*
| Extended user info.
*/
def.attributes =
{
	// if the user checked okay with news emails
	// undefined for visitors
	newsletter: { type: [ 'undefined', 'boolean' ], json: true },

	// the spaces the user owns
	listSpacesOwned: { type: 'list@Shared/Ref/Space', json: true },
};

def.json = true;
