/*
| Shortcuts for trace roots.
*/
def.abstract = true;

import { Self as Plan  } from '{Shared/Trace/Plan}';
import { Self as Trace } from '{ti2c:Trace}';

def.staticLazy.root =
	( ) =>
	Trace.root( Plan.root );

def.staticLazyFunc.space =
	( uid ) =>
	Trace.root( Plan.space, uid );

def.staticLazy.userInfo =
	( ) =>
	Trace.root( Plan.userInfo );
