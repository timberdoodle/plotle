/*
| A range.
*/
def.extend = 'Shared/Mark/Base';

def.attributes =
{
	// begin offset of the range
	traceBegin: { type: 'ti2c:Trace' },

	// end offset of the range
	traceEnd: { type: 'ti2c:Trace' },

	// x-position of the caret kept
	retainX: { type: [ 'undefined', 'number' ] },
};

import { Self as MarkItems } from '{Shared/Mark/Items}';

/*
| For debugging shows the mark as string.
*/
def.lazy.asString =
	function( )
{
	return 'MarkRange: ' + this.traceBegin.asString + ' -- ' + this.traceEnd.asString;
};

/*
| The offset where the caret is.
*/
def.lazy.traceVCaret =
	function( )
{
	return this.traceEnd;
};

/*
| True if the mark is empty.
*/
def.lazy.isEmpty =
	function( )
{
	return this.traceBegin === this.traceEnd;
};

/*
| Returns true if this mark encompasses the trace.
*/
def.proto.encompasses =
	function( trace )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( CHECK && trace.backward( 'paras' ) ) throw new Error( );

	return this.traceBegin.hasTrace( trace );

	/*
	const tPara = trace.backward( 'paras' );
	if( tPara ) return this._encompassesPara( tPara );
	return this.traceBegin.hasTrace( trace );
	*/
};

/*
| Ranges also have caret capabilities.
| The caretMark is identical to end.
*/
def.proto.hasCaret = true;

/*
| The item traces.
| This is either undefined or MarkItems containing the parenting item.
*/
def.lazy.itemsMark =
	function( )
{
	const bTrace = this.traceBegin;

	if( !bTrace.forward( 'view' ) )
	{
		return undefined;
	}
	else
	{
		return MarkItems.TraceVs( bTrace.backward( 'items' ) );
	}
};

/*
| Creates a range mark beginning with this
| as traceBegin and ending with given traceV.
*/
def.proto.rangeMarkEnding =
	function( traceEnd )
{
	return this.create( 'traceEnd', traceEnd );
};

/*
| Begin or end trace, depending on which is in back.
*/
def.lazy.traceBack =
	function( )
{
	this._orderTraces( );
	return this.traceBack;
};

/*
| Begin or end trace, depending on which is in front.
*/
def.lazy.traceFront =
	function( )
{
	this._orderTraces( );
	return this.traceFront;
};

/*
| Sets traceBegin and traceEnd accordinly.
*/
def.proto._orderTraces =
	function( )
{
	const traceBegin = this.traceBegin;
	const traceEnd = this.traceEnd;

	const traceBeginItems = traceBegin.forward( 'items' );
	const traceEndItems = traceEnd.forward( 'items' );

	if( traceBeginItems !== traceEndItems )
	{
		// on a docview a range can begin/end at a different item offset.
		const beginItemsRank = traceBeginItems.last.at;
		const endItemsRank = traceEndItems.last.at;

		if( beginItemsRank < endItemsRank )
		{
			ti2c.aheadValue( this, 'traceFront', traceBegin );
			ti2c.aheadValue( this, 'traceBack', traceEnd );
		}
		else
		{
			ti2c.aheadValue( this, 'traceFront', traceEnd );
			ti2c.aheadValue( this, 'traceBack', traceBegin );
		}
		return;
	}

	const beginParaRank = traceBegin.backward( 'paras' ).last.at;
	const endParaRank = traceEnd.backward( 'paras' ).last.at;

	if( beginParaRank < endParaRank )
	{
		ti2c.aheadValue( this, 'traceFront', traceBegin );
		ti2c.aheadValue( this, 'traceBack', traceEnd );
	}
	else if( beginParaRank > endParaRank )
	{
		ti2c.aheadValue( this, 'traceFront', traceEnd );
		ti2c.aheadValue( this, 'traceBack', traceBegin );
	}
	else
	{
		const bOffset = traceBegin.last.at;
		const eOffset = traceEnd.last.at;
		if( bOffset <= eOffset )
		{
			ti2c.aheadValue( this, 'traceFront', traceBegin );
			ti2c.aheadValue( this, 'traceBack', traceEnd );
		}
		else
		{
			ti2c.aheadValue( this, 'traceFront', traceEnd );
			ti2c.aheadValue( this, 'traceBack', traceBegin );
		}
	}
};
