/*
| A single widget marked ( without caret or range )
*/
def.extend = 'Shared/Mark/Base';

def.attributes =
{
	// trace to the widget
	traceV: { type: 'ti2c:Trace' },
};

/*
| For debugging shows the mark as string.
*/
def.lazy.asString =
	function( )
{
	return 'MarkWidget: ' + this.traceV.asString;
};

/*
| Returns true if this mark encompasses the traceV.
*/
def.proto.encompasses =
	function( traceV )
{
	return this.traceV.hasTrace( traceV );
};

/*
| Shortcut.
*/
def.static.TraceV =
	( traceV ) =>
	Self.create( 'traceV', traceV );

/*
| The widget's trace.
*/
def.lazy.traceVWidget =
	function( )
{
	return this.traceV;
};

/*
| Widget marks have no carets.
*/
def.proto.hasCaret = false;
