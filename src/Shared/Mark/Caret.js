/*
| The virtual caret.
*/
def.extend = 'Shared/Mark/Base';

def.attributes =
{
	// x-position of the caret kept
	retainX: { type: [ 'undefined', 'number' ] },

	// the visual trace of the caret
	traceV: { type: 'ti2c:Trace' },
};

import { Self as MarkItems } from '{Shared/Mark/Items}';
import { Self as MarkRange } from '{Shared/Mark/Range}';

/*
| For debugging shows the mark as string.
*/
def.lazy.asString =
	function( )
{
	return 'MarkCaret: ' + this.traceV.asString;
};

/*
| The caret with offset - 1.
*/
def.lazy.backward =
	function( )
{
	const traceV = this.traceV;
	const at = traceV.last.at;
	if( at === 0 ) return undefined;
	const btrace = traceV.back.add( 'offset', at - 1 );
	const r = this.create( 'traceV', btrace );
	ti2c.aheadValue( r, 'forward', this );
	return r;
};

/*
| When the caret mark turns into a range,
| the traceBegin is synonymous to a traceV.
*/
def.lazy.traceBegin =
	function( )
{
	return this.traceV;
};

/*
| The vtrace of the caret.
*/
def.lazy.traceVCaret =
	function( )
{
	return this.traceV;
};

/*
| Returns true if this mark encompasses the traceV.
*/
def.proto.encompasses =
	function( traceV )
{
	return this.traceV.hasTrace( traceV );
};

/*
| The caret with offset + 1.
*/
def.lazy.forward =
	function( )
{
	const traceV = this.traceV;
	const at = traceV.last.at;
	const r = this.create( 'traceV', this.traceV.back.add( 'offset', at + 1 ) );
	ti2c.aheadValue( r, 'backward', this );
	return r;
};

/*
| A caret mark has a caret.
| The range is the other mark which has this too.
*/
def.proto.hasCaret = true;

/*
| Creates a range mark beginning with this
| as traceBegin and ending with given traceV.
*/
def.proto.rangeMarkEnding =
	function( traceEnd )
{
	return(
		MarkRange.create(
			'traceBegin', this.traceVCaret,
			'traceEnd', traceEnd,
		)
	);
};

/*
| The item traces.
| This is either undefined or MarkItems containing the parenting item.
*/
def.lazy.itemsMark =
	function( )
{
	const traceV = this.traceV;
	const traceVItems = traceV.backward( 'items' );

	return(
		traceVItems
		? MarkItems.TraceVs( traceVItems )
		: undefined
	);
};

/*
| Shortcut.
*/
def.static.TraceV =
	( traceV ) =>
	Self.create( 'traceV', traceV );

/*
| The widget the caret is in.
*/
def.lazy.traceVWidget =
	function( )
{
	return this.traceV.backward( 'widgets' );
};

/*
| The caret with offset = 0.
*/
def.lazy.zero =
	function( )
{
	return(
		this.create(
			'traceV', this.traceV.back.add( 'offset', 0 ),
			'retainX', undefined
		)
	);
};

/*
| Additional checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK && !this.traceV.backward( 'root' ) ) throw new Error( );
};
