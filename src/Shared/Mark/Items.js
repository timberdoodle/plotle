/*
| One or several items marked ( without caret or range )
*/
def.extend = 'Shared/Mark/Base';

def.attributes =
{
	traces: { type: 'set@ti2c:Trace' },
};

import { Self as SetTraces } from '{set@ti2c:Trace}';

/*
| For debugging shows the mark as string.
*/
def.lazy.asString =
	function( )
{
	return 'MarkItems: ...';
};

/*
| Combines the items mark (set) with another one.
*/
def.proto.combine =
	function( imark )
{
/**/if( CHECK && imark.ti2ctype !== Self ) throw new Error( );

	const set = this.traces.clone( );
	for( let t of imark.traces )
	{
		if( !this.containsItemTraceV( t ) ) set.add( t );
	}

	return Self.create( 'set:init', set );
};

/*
| Returns true if an entity of this mark
| contains 'path'.
*/
def.proto.containsItemTraceV =
	function( traceV )
{
	for( let t of this.traces )
	{
		if( t === traceV ) return true;
	}
	return false;
};

/*
| Returns true if this mark encompasses the trace.
*/
def.proto.encompasses =
	function( traceV )
{
	for( let t of this.traces )
	{
		if( t.hasTrace( traceV ) ) return true;
	}
	return false;
};

/*
| Item marks do not have a caret.
*/
def.proto.hasCaret = false;

/*
| The items mark of an items mark is itself.
*/
def.lazy.itemsMark =
	function( )
{
	return this;
};

/*
| Returns a items-mark with the path added
| when it isn't part of this mark, or the
| path removed when it is.
*/
def.proto.toggle =
	function( traceV )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( traceV.last.name !== 'items' ) throw new Error( );
/**/}

	const traces = this.traces;
	for( let t of traces )
	{
		if( traceV === t )
		{
			if( traces.size === 1 )
			{
				// toggled everything away
				return undefined;
			}

			return this.create( 'traces', traces.create( 'set:remove', t ) );
		}
	}

	return this.create( 'traces', traces.create( 'set:add', traceV ) );
};

/*
| Creates the list with one item.
*/
def.static.TraceVs =
	function( ...args )
{
	return Self.create( 'traces', SetTraces.create( 'set:init', new Set( args ) ) );
};

/*
| Creates the list with one item.
*/
def.static.TraceVSet =
	function( traceVSet )
{
	return Self.create( 'traces', SetTraces.create( 'set:init', traceVSet ) );
};

/*
| Additional checking
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	for( let traceV of this.traces )
/**/	{
/**/		if( !traceV.backward( 'root' ) ) throw new Error( );
/**/	}
/**/}
};
