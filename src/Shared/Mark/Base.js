/*
| The base of marks.
*/
def.abstract = true;

/*
| The item traces.
| This is either undefined or MarkItems containing the parenting item.
*/
def.proto.itemsMark = undefined;

/*
| By default a mark does not have a caret.
*/
def.proto.traceVCaret = undefined;

/*
| By default it's not part of a widget.
*/
def.proto.traceVWidget = undefined;
