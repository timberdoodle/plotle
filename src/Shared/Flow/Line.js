/*
| A line of tokens.
*/
def.attributes =
{
	// token offset in string
	offset: { type: 'integer' },

	// the tokens
	tokens: { type: 'list@Shared/Flow/Token' },

	// y position of line
	y: { type: 'number' },
};
