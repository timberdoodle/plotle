/*
| A flow block consists of flow lines.
*/
def.attributes =
{
	// colorless font family and size
	fontBland: { type: 'gleam:Font/Bland' },

	// string to flow
	string: { type: 'string' },

	// width the flow can take
	// zero is unrestricted
	widthFlow: { type: 'number' },
};

import { Self as FlowLine      } from '{Shared/Flow/Line}';
import { Self as FlowToken     } from '{Shared/Flow/Token}';
import { Self as Font          } from '{gleam:Font/Root}';
import { Self as ListFlowLine  } from '{list@Shared/Flow/Line}';
import { Self as ListFlowToken } from '{list@Shared/Flow/Token}';

/*
| Height of the flow.
*/
def.lazy.height =
	function( )
{
	this._calculate;
	return this.height;
};

/*
| The flowed lines.
*/
def.lazy.lines =
	function( )
{
	this._calculate;
	return this.lines;
};

/*
| Width (actually used) of the flow.
*/
def.lazy.width =
	function( )
{
	this._calculate;
	return this.width;
};

/*
| Calculates the flow.
|
| Sets the lazy values 'lines', 'height' and 'width'.
*/
def.lazy._calculate =
	function( )
{
	const string = this.string;
	const fontBland = this.fontBland;
	const widthFlow = this.widthFlow;

	// width really used.
	let widthReal = 0;

	// current positon including last tokens width
	let x = 0;
	let y = fontBland.size;

	const space = fontBland.StringBland( ' ' ).advanceWidth;
	const lines = [ ];

	let currentLineOffset = 0;
	let currentLineList = [ ];

	const reg = ( /(\S+\s*$|\s*\S+|^\s+$)(\s?)(\s*)/g );
	// !pre ? (/(\s*\S+|\s+$)\s?(\s*)/g) : (/(.+)()$/g);

	for( let ca = reg.exec( string ); ca; ca = reg.exec( string ) )
	{
		// a token is a word plus following hard spaces
		const word = ca[ 1 ] + ca[ 3 ];

		const sb = fontBland.StringBland( word );
		const w = sb.width;

		if( widthFlow > 0 && x + w > widthFlow )
		{
			if( x > 0 )
			{
				// soft break
				lines.push(
					FlowLine.create(
						'tokens', ListFlowToken.Array( currentLineList ),
						'y', y,
						'offset', currentLineOffset,
					)
				);

				x = 0;
				currentLineList = [ ];
				y += fontBland.size * ( 1 + Font.bottomBox );
				currentLineOffset = ca.index;
			}
			else
			{
				// horizontal overflow
				// ('HORIZONTAL OVERFLOW'); // TODO
			}
		}

		currentLineList.push(
			FlowToken.create(
				'x', x,
				'width', w,
				'offset', ca.index,
				'word', word,
			)
		);

		widthReal = Math.max( widthReal, x + w );
		x = x + sb.advanceWidth + space;
	}

	lines.push(
		FlowLine.create(
			'tokens', ListFlowToken.Array( currentLineList ),
			'offset', currentLineOffset,
			'y', y
		)
	);

	ti2c.aheadValue( this, 'height', y );
	ti2c.aheadValue( this, 'lines', ListFlowLine.Array( lines ) );
	ti2c.aheadValue( this, 'width', widthReal );

	return true;
};
