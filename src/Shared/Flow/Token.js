/*
| A flow token.
*/
def.attributes =
{
	// x position
	x: { type: 'number' },

	// width of the token
	width: { type: 'number' },

	// offset in string
	offset: { type: 'integer' },

	// token word
	word: { type: 'string' },
};
