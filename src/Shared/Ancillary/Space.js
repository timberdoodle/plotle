/*
| A space.
*/
def.attributes =
{
	// the space items
	items: { type: 'twig@<Shared/Ancillary/Item/Types' },

	// this space is public readable
	publicReadable: { type: 'boolean' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },
};

import { Self as FabricSpace     } from '{Shared/Fabric/Space}';
import { Self as TraceRoot       } from '{Shared/Trace/Root}';
import { Self as TwigAItem       } from '{Shared/Ancillary/Item/Twig}';
import { Self as TwigFabricItems } from '{twig@<Shared/Fabric/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	const items = this.items;
	const twig  = { };
	for( let key of items.keys )
	{
		twig[ key ] = items.get( key ).asFabric;
	}

	return( FabricSpace.create(
		'items',          TwigFabricItems.create( 'twig:init', twig, items.keys ),
		'publicReadable', this.publicReadable,
	) );
};

/*
| Returns the ancillary changes.
*/
def.proto.changes =
	function( affectedTwigItems )
{
	let chgs;

	// FUTURE for( let key of affectedTwigItems )
	for( let item of this.items )
	{
		let ci = item.changes( this );
		if( !ci ) continue;

/**/	if( CHECK && ci.length === 0 ) throw new Error( );

		if( !chgs )
		{
			chgs = ci;
		}
		else
		{
			// other ancillary changes might affect this change
			// notably the ranking in case of "shrinks"
			ci = chgs.transform( ci );
			chgs = chgs.appendList( ci );
		}
	}

	return chgs;
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( space, uid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( space.ti2ctype !== FabricSpace ) throw new Error( );
/**/	if( typeof( uid ) !== 'string' ) throw new Error( );
/**/}

	const trace = TraceRoot.space( uid );

	return( Self.create(
		'items',          TwigAItem.FromFabric( space.items, trace ),
		'publicReadable', space.publicReadable,
		'trace',          trace,
	) );
};
