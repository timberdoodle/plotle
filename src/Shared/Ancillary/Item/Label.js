/*
| An item with resizing text.
*/
def.extend = 'Shared/Ancillary/Item/Base';

def.attributes =
{
	// the fontsize of the label
	fontSize: { type: 'number' },

	// the labels text
	text: { type: 'Shared/Ancillary/Text/Self' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// the items zone
	zone: { type: 'gleam:Rect' },
};

import { Self as AText            } from '{Shared/Ancillary/Text/Self}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as FabricLabel      } from '{Shared/Fabric/Item/Label}';
import { Self as GroupString      } from '{group@string}';
import { Self as Margin           } from '{gleam:Margin}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as Size             } from '{gleam:Size}';
import { Self as Trace            } from '{ti2c:Trace}';
import { Self as TwigItem         } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return( FabricLabel.create(
		'fontSize', this.fontSize,
		'text',     this.text.asFabric,
		'zone',     this.zone,
	) );
};

/*
| The changes needed for secondary data to adapt to primary.
*/
def.proto.changes =
	function( )
{
	const zone = this.zone;
	const sc = this.sizeComputed;

	// no ancillary changes needed?
	if( sc === zone.size )
	{
		return undefined;
	}
	else
	{
		return(
			Sequence.Elements(
				ChangeTreeAssign.create(
					'trace', this.trace.backward( 'items' ).add( 'zone' ),
					'prev',  zone,
					'val',   zone.Size( sc ),
				),
			)
		);
	}
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricLabel ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	const fontSize = item.fontSize;

	return( Self.create(
		'fontSize', fontSize,
		'text',
			AText.FromFabric( item.text, trace.add( 'text' ), fontSize, 20, 0 ),
		'trace',    trace,
		'zone',     item.zone,
	) );
};

/*
| Inner margins of the label.
*/
def.staticLazy.marginInner =
	( ) =>
	Margin.NESW( 1, 1, 1, 1 );

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.zone;
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const zone = this.zone.add( offset );
	if( colItems )
	{
		for( let item of colItems )
		{
			if( item.ti2ctype !== Self ) continue;
			if( item.zone === zone ) return false;
		}
	}

	return this.create( 'zone', zone );
};

/*
| Computed size of the label.
*/
def.lazy.sizeComputed =
	function( )
{
	const sizeText = this.text.sizeFull;
	const height = sizeText.height + 2;

	return(
		Size.WH(
			Math.max( sizeText.width + 4, height / 4 ),
			height
		)
	);
};
