/*
| A list of joints.
*/
def.extend = 'list@Shared/Ancillary/Item/Stroke/Joint/Self';

import { Self as GroupString } from '{group@string}';
import { Self as Point       } from '{gleam:Point}';
import { Self as TwigItem    } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const list = [ ];
	for( let joint of this )
	{
		const pJoint = joint.paste( mapping, offset );
		if( !pJoint ) return false;
		list.push( pJoint );
	}

	if( colItems )
	{
		const j0 = list[ 0 ];
		const j1 = list[ 1 ];
		const p0 = j0.pos;
		const p1 = j1.pos;
		if( p0.ti2ctype === Point && p1.ti2ctype === Point )
		{
			for( let item of colItems )
			{
				const ij0 = item.get( 0 );
				const ij1 = item.get( 1 );
				if( ij0.pos === p0 && ij1.pos === p1 ) return false;
			}
		}
	}

	return this.create( 'list:init', list );
};
