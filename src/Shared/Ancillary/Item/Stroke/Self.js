/*
| A stroke (with possible arrow heads)
*/
def.attributes =
{
	// stroke joints
	joints: { type: 'Shared/Ancillary/Item/Stroke/Joint/List' },

	// "arrow" or "none"
	styleBegin: { type: 'string' },

	// "arrow" or "none"
	styleEnd: { type: 'string' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },
};

import { Self as AJoint           } from '{Shared/Ancillary/Item/Stroke/Joint/Self}';
import { Self as Arrow            } from '{gleam:Arrow}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as ChangeTreeShrink } from '{ot:Change/Tree/Shrink}';
import { Self as FabricStroke     } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as GroupString      } from '{group@string}';
import { Self as ListAJoint       } from '{Shared/Ancillary/Item/Stroke/Joint/List}';
import { Self as ListFabricJoint  } from '{Shared/Fabric/Item/Stroke/Joint/List}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Segment          } from '{gleam:Segment}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as Trace            } from '{ti2c:Trace}';
import { Self as TwigAItem        } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	const list = [ ];
	for( let joint of this.joints )
	{
		list.push( joint.asFabric );
	}

	return(
		FabricStroke.create(
			'joints', ListFabricJoint.Array( list ),
			'styleBegin', this.styleBegin,
			'styleEnd',   this.styleEnd,
		)
	);
};

/*
| The changes needed for secondary data to adapt to primary.
|
| ~space: ancillary space
*/
def.proto.changes =
	function( space )
{
	const joints = this.joints;
	const items = space.items;

	let j0 = joints.get( 0 ).pos;
	let j1 = joints.get( 1 ).pos;

	if( j0.ti2ctype === Trace )
	{
		j0 = items.get( j0.last.key );
	}

	if( j1.ti2ctype === Trace )
	{
		j1 = items.get( j1.last.key );
	}

	// deletes the stroke if one of the joints disappeared
	if( !j0 || !j1 )
	{
		return(
			Sequence.Elements(
				ChangeTreeShrink.create(
					'trace', this.trace,
					'prev',  this.asFabric,
					'rank',  items.rankOf( this.trace.last.key )
				)
			)
		);
	}

	return(
		this.changesByOutlines(
			j0.ti2ctype === Point ? undefined : j0.outline,
			j1.ti2ctype === Point ? undefined : j1.outline,
		)
	);
};

/*
| The changes needed for secondary data to adapt to primary.
| Also used by Action.
|
| ~outline0: outline0 (or undefined)
| ~outline1: outline1 (or undefined)
*/
def.proto.changesByOutlines =
	function( outline0, outline1 )
{
	let achgs = Sequence.Empty;

	const joints = this.joints;

	const tj0Outline = joints.get( 0 ).outline;
	const tj1Outline = joints.get( 1 ).outline;

	if( outline0 && outline0 !== tj0Outline || ( !outline0 && tj0Outline ) )
	{
		const ch =
			ChangeTreeAssign.create(
				'trace', this.trace.add( 'joints', 0 ).add( 'outline' ),
				'prev', tj0Outline,
				'val', outline0
			);

		achgs = achgs.append( ch );
	}

	if( outline1 && outline1 !== tj1Outline || ( !outline1 && tj1Outline ) )
	{
		const ch =
			ChangeTreeAssign.create(
				'trace', this.trace.add( 'joints', 1 ).add( 'outline' ),
				'prev', tj1Outline,
				'val', outline1
			);

		achgs = achgs.append( ch );
	}

	return(
		achgs !== Sequence.Empty
		? achgs
		: undefined
	);
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricStroke ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	const joints = item.joints;
	const list = [ ];
	for( let joint of joints )
	{
		list.push( AJoint.FromFabric( joint ) );
	}

	return( Self.create(
		'styleBegin', item.styleBegin,
		'styleEnd',   item.styleEnd,
		'joints', ListAJoint.Array( list ),
		'trace', trace,
	) );
};

/*
| The basic connection of the stroke.
*/
def.lazy.segment =
	function( )
{
	const j0 = this.joints.get( 0 );
	const j1 = this.joints.get( 1 );

	return Segment.Connection( j0.outline || j0.pos, j1.outline || j1.pos );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	const segment = this.segment;

	return(
		Arrow.create(
			'arrowHeadSize', 12,
			'end0',          this.styleBegin,
			'end1',          this.styleEnd,
			'joint0',        segment.p0,
			'joint1',        segment.p1,
			'widthBase',     10,
		)
	);
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigAItem ) throw new Error( );
/**/}

	const joints = this.joints.paste( mapping, offset, colItems );
	if( !joints ) return false;
	return this.create( 'joints', joints );
};

/*
| The items zone.
*/
def.lazy.zone =
	function( )
{
	return this.segment.zone;
};
