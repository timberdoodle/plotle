/*
| Tools for a twig of items.
*/
def.abstract = true;

import { Self as AActuator      } from '{Shared/Ancillary/Item/Actuator}';
import { Self as ADocPath       } from '{Shared/Ancillary/Item/DocPath/Self}';
import { Self as ALabel         } from '{Shared/Ancillary/Item/Label}';
import { Self as ANote          } from '{Shared/Ancillary/Item/Note}';
import { Self as AStroke        } from '{Shared/Ancillary/Item/Stroke/Self}';
import { Self as ASubspace      } from '{Shared/Ancillary/Item/Subspace}';
import { Self as FabricActuator } from '{Shared/Fabric/Item/Actuator}';
import { Self as FabricDocPath  } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as FabricLabel    } from '{Shared/Fabric/Item/Label}';
import { Self as FabricNote     } from '{Shared/Fabric/Item/Note}';
import { Self as FabricStroke   } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as FabricSubspace } from '{Shared/Fabric/Item/Subspace}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TwigAItem      } from '{twig@<Shared/Ancillary/Item/Types}';
import { Self as TwigFabricItem } from '{twig@<Shared/Fabric/Item/Types}';

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( twig, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( twig.ti2ctype !== TwigFabricItem ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	const keys = twig.keys;
	const group = { };
	for( let key of keys )
	{
		const item = twig.get( key );
		const traceItem = trace.add( 'items', key );

		switch( item.ti2ctype )
		{
			case FabricActuator:
				group[ key ] = AActuator.FromFabric( item, traceItem );
				break;

			case FabricDocPath:
				group[ key ] = ADocPath.FromFabric( item, traceItem );
				break;

			case FabricLabel:
				group[ key ] = ALabel.FromFabric( item, traceItem );
				break;

			case FabricNote:
				group[ key ] = ANote.FromFabric( item, traceItem );
				break;

			case FabricStroke:
				group[ key ] = AStroke.FromFabric( item, traceItem );
				break;

			case FabricSubspace:
				group[ key ] = ASubspace.FromFabric( item, traceItem );
				break;

			default: throw new Error( );
		}
	}

	return TwigAItem.create( 'twig:init', group, keys );
};

