/*
| A link to another space.
*/
def.extend = 'Shared/Ancillary/Item/Base';

def.attributes =
{
	// the space linked
	link: { type: 'string' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// the portals zone
	zone: { type: 'gleam:Rect' },
};

import { Self as Ellipse        } from '{gleam:Ellipse}';
import { Self as FabricSubspace } from '{Shared/Fabric/Item/Subspace}';
import { Self as GroupString    } from '{group@string}';
import { Self as Point          } from '{gleam:Point}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TwigItem       } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return( FabricSubspace.create(
		'link',     this.link,
		'zone',     this.zone,
	) );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricSubspace ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	return( Self.create(
		'link',     item.link,
		'trace',    trace,
		'zone',     item.zone,
	) );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return Ellipse.Zone( this.zone );
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const zone = this.zone.add( offset );
	if( colItems )
	{
		for( let item of colItems )
		{
			if( item.ti2ctype !== Self ) continue;
			if( item.zone === zone ) return false;
		}
	}
	return this.create( 'zone', this.zone.add( offset ) );
};
