/*
| A paragraph.
*/
def.attributes =
{
	// the fontsize
	fontSize: { type: 'number' },

	// the paragraphs string
	string: { type: 'string' },

	// the trace of the para
	trace: { type: 'ti2c:Trace' },

	// width available to fill
	widthFlow: { type: 'number' },
};

import { Self as DesignFont } from '{Shell/Design/Font}';
import { Self as FabricPara } from '{Shared/Fabric/Text/Para}';
import { Self as FlowBlock  } from '{Shared/Flow/Block}';
import { Self as Trace      } from '{ti2c:Trace}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return FabricPara.create( 'string', this.string );
};

/*
| The para's flow, the position of all chunks.
*/
def.lazy.flow =
	function( )
{
	return(
		FlowBlock.create(
			'fontBland', this.fontBland,
			'string',    this.string,
			'widthFlow', this.widthFlow,
		)
	);
};

/*
| The fontBland for this para.
*/
def.lazy.fontBland =
	function( )
{
	return DesignFont.SizeBland( this.fontSize );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( para, trace, fontSize, widthFlow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( para.ti2ctype !== FabricPara ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/	if( typeof( fontSize ) !== 'number' ) throw new Error( );
/**/	if( typeof( widthFlow ) !== 'number' ) throw new Error( );
/**/}

	return( Self.create(
		'fontSize',  fontSize,
		'string',    para.string,
		'trace',     trace,
		'widthFlow', widthFlow,
	) );
};

/*
| True if the para is effectively empty or has only blank characters.
*/
def.lazy.isBlank =
	function( )
{
	return /^\s*$/.test( this.string );
};
