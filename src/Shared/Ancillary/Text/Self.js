/*
| A sequence of paragraphs.
*/
def.attributes =
{
	// the fontsize
	fontSize: { type: 'number' },

	// the paragraphs
	paras: { type: [ 'list@Shared/Ancillary/Text/Para' ] },

	// vertical seperation of paragraphs as inverse factor of fontSize
	sepParaFact: { type: 'number' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// width available to fill( 0 for labels is infinite )
	widthFlow: { type: 'number' },
};

import { Self as APara          } from '{Shared/Ancillary/Text/Para}';
import { Self as FabricText     } from '{Shared/Fabric/Text/Self}';
import { Self as FontRoot       } from '{gleam:Font/Root}';
import { Self as ListAPara      } from '{list@Shared/Ancillary/Text/Para}';
import { Self as ListFabricPara } from '{list@Shared/Fabric/Text/Para}';
import { Self as Size           } from '{gleam:Size}';
import { Self as Trace          } from '{ti2c:Trace}';

const max = Math.max;

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	const list = [ ];
	for( let para of this.paras )
	{
		list.push( para.asFabric );
	}

	return FabricText.create( 'paras', ListFabricPara.Array( list ) );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( text, trace, fontSize, sepParaFact, widthFlow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( text.ti2ctype !== FabricText ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/	if( typeof( fontSize ) !== 'number' ) throw new Error( );
/**/	if( typeof( sepParaFact ) !== 'number' ) throw new Error( );
/**/	if( typeof( widthFlow ) !== 'number' ) throw new Error( );
/**/}

	const list = [ ];
	const paras = text.paras;
	for( let a = 0, alen = paras.length; a < alen; a++ )
	{
		const para = paras.get( a );
		list.push( APara.FromFabric( para, trace.add( 'paras', a ), fontSize, widthFlow ) );
	}

	return( Self.create(
		'fontSize',    fontSize,
		'paras',       ListAPara.Array( list ),
		'sepParaFact', sepParaFact,
		'trace',       trace,
		'widthFlow',   widthFlow,
	) );
};

/*
| True if all paras are effectively empty
| (have only blank characters).
*/
def.lazy.isBlank =
	function( )
{
	for( let para of this.paras )
	{
		if( !para.isBlank ) return false;
	}

	return true;
};

/*
| Full size of the text.
|
| Disregards clipping in notes.
*/
def.lazy.sizeFull =
	function( )
{
	let height = 0;
	let width = 0;
	const fs = this.fontSize;
	const sepPara = fs / this.sepParaFact;
	let first = true;

	for( let para of this.paras )
	{
		const flow = para.flow;
		width = max( width, flow.width );

		if( !first )
		{
			height += sepPara;
		}
		else
		{
			first = false;
		}

		height += flow.height;
	}

	height += Math.round( fs * FontRoot.bottomBox );
	return Size.WH( width, height );
};
