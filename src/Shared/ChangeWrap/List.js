/*
| A list of wraped change(lists).
*/
def.extend = 'list@Shared/ChangeWrap/Self';
def.json = true;

/*
| Performes the wrapped change-(lists) on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	// iterates through the change list
	for( let cw of this )
	{
		tree = cw.changeTree( tree );
	}
	return tree;
};

/*
| Performes the reversion of the
| wrapped-change (lists) on a tree.
*/
def.proto.changeTreeReversed =
	function( tree )
{
	for( let cw of this.reverse( ) )
	{
		tree = cw.changeTreeReversed( tree );
	}
	return tree;
};

/*
| Transform an 'other' change on this list of wrapped changes.
*/
def.proto.transform =
	function( other )
{
	for( let cw of this )
	{
		other = cw.transform( other );
	}
	return other;
};
