/*
| Parses locations (URLs) to Refs.
|
| Server and Client need to have a shared understanding of this.
*/
def.attributes =
{
	// true if devel mode
	devel: { type: 'boolean' },

	// the space reference
	ref: { type: 'Shared/Ref/Space' },
};

import { Self as RefSpace } from '{Shared/Ref/Space}';

/*
| Parses locations (URLs) to Refs.
|
| ~pathx: pathname as string (or path parts)
|
| ~returns: a location object
|           -or- undefined (some other parse)
|           -or- false (broken parse)
*/
def.static.Path =
	function( pathx )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( typeof( pathx ) === 'string' )
	{
		if( pathx.startsWith( '/' ) )
		{
			pathx = pathx.substring( 1 );
		}
		pathx = pathx.split( '/' );
	}

/**/if( CHECK && !Array.isArray( pathx ) ) throw new Error( );

	if( pathx.length === 1 )
	{
		switch( pathx[ 0 ]  )
		{
			case 'devel.html':
				return(
					Self.create(
						'devel', true,
						'ref', RefSpace.PlotleHome,
					)
				);

			case '':
			case 'index.html':
				return(
					Self.create(
						'devel', false,
						'ref', RefSpace.PlotleHome,
					)
				);
		}

		return undefined;
	}

	if( pathx[ 0 ] === 'sp' )
	{
		let devel = false;
		let len = pathx.length;

		switch( pathx[ len - 1 ] )
		{
			case 'devel.html':
				devel = true;
				len--;
				break;

			case '':
			case 'index.html':
				len--;
				break;
		}

		const link = pathx.slice( 1, len ).join( '/' );
		const ref = RefSpace.Link( link );

		if( ref === undefined ) return undefined;

		return(
			Self.create(
				'devel', devel,
				'ref', ref,
			)
		);
	}

	return undefined;
};

