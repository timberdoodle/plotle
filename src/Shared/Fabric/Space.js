/*
| A space.
*/
def.attributes =
{
	// the space items
	items: { type: 'twig@<Shared/Fabric/Item/Types', json: true },

	// this space is public readable
	publicReadable: { type: 'boolean', defaultValue: 'false', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
