/*
| A fix sized text item.
|
| Has potentionaly a scrollbar.
*/
def.attributes =
{
	// the fontsize of the note
	fontSize: { type: 'number', json: true },

	// the notes text
	text: { type: 'Shared/Fabric/Text/Self', json: true },

	// the notes zone
	zone: { type: 'gleam:Rect', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
