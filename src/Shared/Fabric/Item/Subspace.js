/*
| A link to another space.
*/
def.attributes =
{
	// the space linked
	link: { type: 'string', json: true },

	// the portals zone
	zone: { type: 'gleam:Rect', json: true }
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
