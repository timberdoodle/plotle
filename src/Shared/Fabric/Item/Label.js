/*
| An item with resizing text.
*/
def.attributes =
{
	// the fontsize of the label
	fontSize: { type: 'number', json: true },

	// the labels text
	text: { type: 'Shared/Fabric/Text/Self', json: true },

	// the items zone
	zone: { type: 'gleam:Rect', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
