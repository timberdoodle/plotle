/*
| A document path.
*/
def.attributes =
{
	// the name of the docpath
	name: { type: 'string', json: true },

	// joints of the docpath
	joints: { type: 'Shared/Fabric/Item/DocPath/Joint/List', json: true },

	// the zone of the start marker
	zone: { type: 'gleam:Rect', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
