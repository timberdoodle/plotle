/*
| A stroke (with possible arrow heads)
*/
def.attributes =
{
	// the joint position (point or item reference)
	pos: { type: [ 'gleam:Point', 'ti2c:Trace' ], json: true },

	// ancillary outline of the joint
	outline: { type: [ 'undefined', '<gleam:Shape/Types' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
