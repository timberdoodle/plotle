/*
| A sequence of paragraphs.
*/
def.attributes =
{
	// the paragraphs
	paras: { type: [ 'list@Shared/Fabric/Text/Para' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
