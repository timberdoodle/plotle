/*
| The servers replies to a clients alter request.
*/
def.attributes =
{
	results: { type: 'list@string', json: true },
};

def.json = true;
