/*
| The servers replies to a clients to change a users newsletter optin.
*/
def.singleton = true;

// there aren't any attributes,
// the answer is simply an "Okay, I got that"
// instead of an error.

def.json = true;
