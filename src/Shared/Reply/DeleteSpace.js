/*
| The reply of a delete Space request.
| (Successfully otherwise an error would have been returned.)
*/
def.attributes =
{
	// uid of the space that has been deleted
	uid: { type: 'string', json: true },
};

def.json = true;
