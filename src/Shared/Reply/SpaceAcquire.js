/*
| The servers replies to a clients (space-)acquire request.
*/
def.attributes =
{
	// access level
	// 'ro' : readonly
	// 'rw' : read-write
	// 'no' : none
	access: { type: 'string', json: true },

	// the space dynamic
	space:
	{
		type: [ 'undefined', 'Shared/Dynamic/Current/Space' ],
		json: true,
	},

	// true if current user is an owner of the space
	owner: { type: [ 'undefined', 'boolean' ], json: true },

	// the acquire result
	status: { type: 'string', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
