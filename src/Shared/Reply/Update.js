/*
| The servers replies to a clients update request.
*/
def.extend = 'list@<Shared/Dynamic/Update/Types';
def.json = true;
def.fromJsonArgs = [ 'plan' ];
