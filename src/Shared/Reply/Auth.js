/*
| The servers replies to a succesfull clients auth request.
*/
def.attributes =
{
	// visitors get their real id here
	userCreds: { type: 'Shared/User/Creds', json: true },

	// the list of spaces the user has
	// undefined for visitors
	userInfo:
	{
		type: [ 'undefined', 'Shared/Dynamic/Current/UserInfo' ],
		json: true,
	},
};

def.json = true;
