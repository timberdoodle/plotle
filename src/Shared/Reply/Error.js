/*
| The servers encountered an error with the request.
*/
def.attributes =
{
	// the error message
	message: { type: 'string', json: true },
};

def.json = true;

/*
| Shortcut.
*/
def.static.Message =
	function( message )
{
	return Self.create( 'message', message );
};
