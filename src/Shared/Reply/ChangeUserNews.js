/*
| The servers replies to a clients request to change a users newsletter opt-in.
*/
def.singleton = true;

// there aren't any attributes,
// the answer is simply an "Okay, I got that"
// instead of an error.

def.json = true;
