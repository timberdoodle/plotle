/*
| A list of changes to be applied on a space dynamic.
*/
def.attributes =
{
	// the changes
	changeWrapList: { type: 'Shared/ChangeWrap/List', json: true },

	// the moment to apply changes to
	moment: { type: 'Shared/Dynamic/Moment/Space', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeWrapList } from '{Shared/ChangeWrap/List}';
import { Self as MomentSpace    } from '{Shared/Dynamic/Moment/Space}';

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( json.$type !== this.$type ) throw new Error( );

	let changeWrapList = json.changeWrapList;
	const moment = MomentSpace.FromJson( json.moment );
	if( changeWrapList )
	{
		if( changeWrapList.$type !== ChangeWrapList.$type )
		{
			throw new Error( );
		}
		changeWrapList = ChangeWrapList.FromJson( changeWrapList, plan.get( 'space' ) );
	}

	return(
		Self.create(
			'changeWrapList', changeWrapList,
			'moment', moment,
		)
	);
};
