/*
| A list of changes to be applied on a user info.
*/
def.attributes =
{
	// the changes
	changeWrapList: { type: [ 'undefined', 'Shared/ChangeWrap/List' ], json: true },

	// the moment the update apply changes to
	moment: { type: 'Shared/Dynamic/Moment/UserInfo', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeWrapList } from '{Shared/ChangeWrap/List}';
import { Self as MomentUserInfo } from '{Shared/Dynamic/Moment/UserInfo}';

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( json.$type !== this.$type ) throw new Error( );

	let changeWrapList = json.changeWrapList;
	const moment = MomentUserInfo.FromJson( json.moment );

	if( changeWrapList )
	{
		if( changeWrapList.$type !== ChangeWrapList.$type )
		{
			throw new Error( );
		}

		changeWrapList =
			ChangeWrapList
			.FromJson( changeWrapList, plan.get( 'userInfo' ) );
	}

	return(
		Self.create(
			'changeWrapList', changeWrapList,
			'moment', moment,
		)
	);
};
