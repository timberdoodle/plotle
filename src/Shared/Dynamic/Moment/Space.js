/*
| A moment in a space dynamic.
*/
def.attributes =
{
	// sequence number the dynamic is at
	seq: { type: 'integer', json: true },

	// the uid of the space
	uid: { type: 'string', json: true },
};

def.json = true;
