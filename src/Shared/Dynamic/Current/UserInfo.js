/*
| A current state of a dynamic user info.
*/
def.attributes =
{
	// the current stat to the dynamic entity
	current: { type: 'Shared/User/Info', json: true },

	// the momement referencing current state
	moment: { type: 'Shared/Dynamic/Moment/UserInfo', json: true },
};

def.json = true;
