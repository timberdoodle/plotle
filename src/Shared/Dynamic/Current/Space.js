/*
| A current state of a dynamic space.
*/
def.attributes =
{
	// the current stat to the dynamic entity
	current: { type: 'Shared/Fabric/Space', json: true },

	// the momement referencing current state
	moment: { type: 'Shared/Dynamic/Moment/Space', json: true },
};

def.fromJsonArgs = [ 'plan' ];
def.json = true;
