/*
| A client requests for a user to change their password.
*/
def.attributes =
{
	// new passhash
	passhashNew: { type: 'string', json: true },

	// user/pass credentials to be registered
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
