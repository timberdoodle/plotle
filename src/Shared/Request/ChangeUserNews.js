/*
| A client requests for a user to change the newsletter opt-in.
*/
def.attributes =
{
	// true if the user is okay with the newsletter
	newsletter: { type: 'boolean', json: true },

	// user/pass credentials to be registered
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
