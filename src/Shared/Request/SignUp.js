/*
| A client requests a new user to be signed up / registered.
*/
def.attributes =
{
	// email address of the user, can be empty
	mail: { type: 'string', json: true },

	// true if the user is okay with the newsletter
	newsletter: { type: 'boolean', json: true },

	// user/pass credentials to be registered
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
