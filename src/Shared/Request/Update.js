/*
| A client requests updates on dynamics.
|
| The server might hold back the answer until something happens.
*/
def.attributes =
{
	// the references to moments in dynamics to get updates for
	moments: { type: 'list@<Shared/Dynamic/Moment/Types', json: true },

	// user creds
	userCreds: { type: 'Shared/User/Creds', json: true, },
};

def.json = true;
