/*
| A client request authentication to be checked,
| or to be assigned a visitor-id.
*/
def.attributes =
{
	// user credentials to be authenticated
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
