/*
| Holds a space.
*/
def.attributes =
{
	// latest sequence number
	seq: { type: 'integer' },

	// latest space version
	space: { type: 'Shared/Fabric/Space' },

	// reference to the space
	refSpace: { type: 'Shared/Ref/Space' },

	// unique id of the space
	uid: { type: 'string' },

	// changeWraps cached in RAM
	_changeWraps: { type: 'Shared/ChangeWrap/List' },

	// the offset of the stored changeWraps
	// on server load the past isn't kept in memory
	_changesOffset: { type: 'integer' },

	// marker for this space to be deleted
	_deleted: { type: 'boolean', defaultValue: 'false' },
};

import { Self as ChangeWrap     } from '{Shared/ChangeWrap/Self}';
import { Self as ChangeWrapList } from '{Shared/ChangeWrap/List}';
import { Self as CurrentSpace   } from '{Shared/Dynamic/Current/Space}';
import { Self as FabricActuator } from '{Shared/Fabric/Item/Actuator}';
import { Self as FabricDocPath  } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as FabricLabel    } from '{Shared/Fabric/Item/Label}';
import { Self as FabricNote     } from '{Shared/Fabric/Item/Note}';
import { Self as FabricSpace    } from '{Shared/Fabric/Space}';
import { Self as FabricStroke   } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as FabricSubspace } from '{Shared/Fabric/Item/Subspace}';
import { Self as MomentSpace    } from '{Shared/Dynamic/Moment/Space}';
import { Self as RefSpace       } from '{Shared/Ref/Space}';
import { Self as Repository     } from '{Server/Database/Repository}';
import { Self as TwigItem       } from '{twig@<Shared/Fabric/Item/Types}';
import { Self as Uid            } from '{Shared/Session/Uid}';

/*
| Appends a change wrap list.
|
| This currently initiates a write to database and forgets about it.
*/
def.proto.appendChanges =
	function( changeWrapList, username )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( changeWrapList.length === 0 ) throw new Error( );
/**/	if( typeof( username ) !== 'string' ) throw new Error( );
/**/}

	Repository.sendChanges( changeWrapList, this.uid, username, this.seq, Date.now( ) );

	return(
		this.create(
			'seq', this.seq + changeWrapList.length,
			'space', changeWrapList.changeTree( this.space ),
			'_changeWraps', this._changeWraps.appendList( changeWrapList ),
		)
	);
};

/*
| Turns the space into html content.
| Used for search engines.
*/
def.lazy.searchEngineContent =
	function( )
{
	const content = [ ];
	const space = this.space;
	const items = space.items;

	// sorted items from up to down
	const keysUD = [ ];
	for( let iKey of items.keys )
	{
		const item = items.get( iKey );

		switch( item.ti2ctype )
		{
			case FabricLabel:
			case FabricNote:
			case FabricSubspace:
				keysUD.push( iKey );
				continue;

			case FabricActuator:
			case FabricStroke:
			case FabricDocPath:
				// is not put in search engine content
				continue;

			default: throw new Error( );
		}
	}

	keysUD.sort(
		( keyA, keyB ) =>
		{
			const itemA = items.get( keyA );
			const itemB = items.get( keyB );

			const zoneA = itemA.zone;
			const zoneB = itemB.zone;

			const posA = zoneA.pos;
			const posB = zoneB.pos;

			const pay = posA.y;
			const pby = posB.y;

			if( pay > pby ) return 1;
			if( pay < pby ) return -1;

			const pax = posA.x;
			const pbx = posB.x;

			if( pax > pbx ) return 1;
			if( pax < pbx ) return -1;

			return 0;
		}
	);

	for( let iKey of keysUD )
	{
		const item = items.get( iKey );

		switch( item.ti2ctype )
		{
			case FabricLabel:
			case FabricNote:
			{
				const text = item.text;
				const paras = text.paras;
				content.push( '<div id="' + iKey + '">' );
				for( let pi = 0, plen = paras.length; pi < plen; pi++ )
				{
					const para = paras.get( pi );
					content.push(
						'<p id="' + iKey + '_' + pi + '">',
						para.string,
						'</p>',
					);
				}
				content.push( '</div>' );
				continue;
			}

			case FabricSubspace:
			{
				const ref = RefSpace.Link( item.link, this.refSpace );
				content.push(
					'<a'
					+ ' id="' + iKey + '"'
					+ ' href="/sp/'
					+ ref.fullname
					+ '">subspace</a>'
				);
				continue;
			}

			default: throw new Error( );
		}
	}

	return content.join( '' );
};

/*
| Creates a new space and returns the spaceBox for it.
*/
def.static.createSpace =
	async function( refSpace )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const fabric =
		FabricSpace.create(
			'items', TwigItem.create( ),
		);

	const uid = await Repository.establishSpace( refSpace, fabric );

	return(
		Self.create(
			'space', fabric,
			'refSpace', refSpace,
			'seq', 2,
			'uid', uid,
			'_changeWraps', ChangeWrapList.create( ),
			'_changesOffset', 2,
		)
	);
};

/*
| Current space dynamic.
*/
def.lazy.current =
	function( )
{
	return(
		CurrentSpace.create(
			'current', this.space,
			'moment',
				MomentSpace.create(
					'seq', this.seq,
					'uid', this.uid,
				),
		)
	);
};

/*
| Public deleted marker.
*/
def.lazy.deleted =
	function( )
{
	return this._deleted;
};

/*
| Returns the change wrap by its sequence.
*/
def.proto.getChangeWrap =
	function( seq )
{
	return this._changeWraps.get( seq - this._changesOffset );
};

/*
| Returns the change waps from 'seq' up to current
*/
def.proto.getChangeWrapsUp2Current =
	function( seq )
{
	const seqT = this.seq;
	const list = [ ];
	for( ; seq < seqT; seq++ )
	{
		list.push( this.getChangeWrap( seq ) );
	}

	return ChangeWrapList.Array( list );
};

/*
| Loads a space from the db and returns the spaceBox for it.
|
| ~refSpace: reference to space
| ~uid:      unique id for space
*/
def.static.loadSpace =
	async function( refSpace, uid )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	let seq = 1;
	let space = undefined;

	for(;;)
	{
		const changeSkid = await Repository.getSpaceChange( uid, seq );
		if( changeSkid === 'notFound' )
		{
			break;
		}
		seq++;
		space = changeSkid.changes.changeTree( space );
	}

	return(
		Self.create(
			'space',          space,
			'refSpace',       refSpace,
			'seq',            seq,
			'uid',            uid,
			'_changeWraps',   ChangeWrapList.Empty,
			'_changesOffset', seq
		)
	);
};

/*
| Sets the deleted marker.
*/
def.proto.setDeleted =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 0 ) throw new Error( );
/**/	if( this._deleted ) throw new Error( );
/**/}

	const seqp1 = this.seq + 1;

	const cw =
		ChangeWrap.create(
			'deleted', true,
			'id', Uid.newUid( ),
			'seq', seqp1,
		);

	return(
		this.create(
			'_changeWraps', this._changeWraps.append( cw ),
			'_deleted', true,
			'seq', seqp1,
		)
	);
};
