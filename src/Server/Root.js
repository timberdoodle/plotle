/*
| Starts the server.
*/
def.abstract = true;

import { loadConfig } from './config/load.js';

import { Self as Log        } from '{Server/Log}';
import { Self as RefSpace   } from '{Shared/Ref/Space}';
import { Self as Repository } from '{Server/Database/Repository}';
import { Self as Spaces     } from '{Server/Spaces}';
import { Self as UpSleeps   } from '{Server/UpSleep/Manager}';
import { Self as Users      } from '{Server/User/Manager}';
import { Self as Web        } from '{Server/Web}';

/*
| Root dir.
*/
let _dir;

/*
| Does the init.
*/
async function doInit( filenameConfig, codeConfig )
{
	await loadConfig( 'src/Server/config/template.js', filenameConfig, codeConfig );

	// Server checking.
	globalThis.CHECK = config.server.check;

	await Repository.connect( );
	await Users.init( );
	await Web.prepare( _dir );

	UpSleeps.init( );
	await Spaces.init( );
	await Web.start( );

	Log.log( 'server running' );

/**/if( PUPPET )
/**/{
/**/	PUPPET( { event: 'running' } );
/**/}
}

/*
| Interprocess communication message when being a puppet.
*/
async function puppetMessage( msg )
{
	switch( msg.cmd )
	{
		case 'config':
			doInit( 'config', msg.config );
			break;

		case 'getSpaceFabric':
		{
			const ref = RefSpace.Fullname( msg.ref );
			const space = Spaces.getByRef( ref );
			PUPPET(
				{
					reply: msg.cmd,
					space: space.space.jsonfy( ),
				}
			);
			break;
		}

		default: throw new Error( );
	}
}

/*
| Starts the server.
|
| ~dir: root dir of plotle server.
*/
def.static.init =
	async function( dir )
{
	_dir = dir;
	globalThis.PUPPET = false;

	if( process.argv.length >= 3 )
	{
		switch( process.argv[ 2 ] )
		{
			case 'puppet':
				globalThis.PUPPET = process.send.bind( process );
				break;

			default: throw new Error( 'invalid argument' );
		}
	}

	if( PUPPET )
	{
		// inter process communicationn mode
		process.on( 'disconnect', ( ) => { process.exit(1); } );
		process.on( 'message', puppetMessage );
	}
	else
	{
		doInit( 'config.js' );
	}
};
