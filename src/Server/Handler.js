/*
| Handles all client requests.
*/

// Provides only static functions.
def.abstract = true;

import process from 'node:process';
import timers  from 'node:timers/promises';

import { Self as ListDynamicMoment     } from '{list@<Shared/Dynamic/Moment/Types}';
import { Self as ListString            } from '{list@string}';
import { Self as Log                   } from '{Server/Log}';
import { Self as MomentSpace           } from '{Shared/Dynamic/Moment/Space}';
import { Self as MomentUserInfo        } from '{Shared/Dynamic/Moment/UserInfo}';
import { Self as Plan                  } from '{Shared/Trace/Plan}';
import { Self as ReplyAlter            } from '{Shared/Reply/Alter}';
import { Self as ReplyAuth             } from '{Shared/Reply/Auth}';
import { Self as ReplyChangePwd        } from '{Shared/Reply/ChangePwd}';
import { Self as ReplyChangeUserNews   } from '{Shared/Reply/ChangeUserNews}';
import { Self as ReplyDeleteSpace      } from '{Shared/Reply/DeleteSpace}';
import { Self as ReplyError            } from '{Shared/Reply/Error}';
import { Self as ReplySignUp           } from '{Shared/Reply/SignUp}';
import { Self as ReplySpaceAcquire     } from '{Shared/Reply/SpaceAcquire}';
import { Self as ReplyUpdate           } from '{Shared/Reply/Update}';
import { Self as RequestAlter          } from '{Shared/Request/Alter}';
import { Self as RequestAuth           } from '{Shared/Request/Auth}';
import { Self as RequestChangePwd      } from '{Shared/Request/ChangePwd}';
import { Self as RequestChangeUserNews } from '{Shared/Request/ChangeUserNews}';
import { Self as RequestDeleteSpace    } from '{Shared/Request/DeleteSpace}';
import { Self as RequestSignUp         } from '{Shared/Request/SignUp}';
import { Self as RequestSpaceAcquire   } from '{Shared/Request/SpaceAcquire}';
import { Self as RequestUpdate         } from '{Shared/Request/Update}';
import { Self as Spaces                } from '{Server/Spaces}';
import { Self as UpdateSpace           } from '{Shared/Dynamic/Update/Space}';
import { Self as UpdateUserInfo        } from '{Shared/Dynamic/Update/UserInfo}';
import { Self as UpSleeps              } from '{Server/UpSleep/Manager}';
import { Self as Users                 } from '{Server/User/Manager}';
import { Self as UserInfo              } from '{Server/User/Info}';

/*
| Creates a reject error for all
| serve* functions
*/
const replyError =
	function( message )
{
	Log.log( 'reject', message );
	return ReplyError.Message( message );
};

/*
| Returns a result for an update operation.
|
| ~moments: references to moments in dynamics to get updates for
*/
def.static.conveyUpdate =
	async function( moments )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( moments.ti2ctype !== ListDynamicMoment ) throw new Error( );
/**/}

	const list = [ ];
	for( let moment of moments )
	{
		const seq = moment.seq;

		switch( moment.ti2ctype )
		{
			case MomentSpace:
			{
				const spaceBox = Spaces.getByUid( moment.uid );
				if( !spaceBox ) continue;

				const boxSeq = spaceBox.seq;
				if( seq >= boxSeq ) continue;

				list.push(
					UpdateSpace.create(
						'moment', moment,
						'changeWrapList', spaceBox.getChangeWrapsUp2Current( seq ),
					)
				);

				continue;
			}

			case MomentUserInfo :
			{
				const userInfoServer = Users.getByName( moment.username );
				if( !userInfoServer ) continue;

				const dynamicUserInfo = await Users.getDynamicUserInfo( userInfoServer );
				const changeWraps = dynamicUserInfo.changeWraps;

				if( seq - 1 < changeWraps.length )
				{
					list.push(
						UpdateUserInfo.create(
							'moment', moment,
							'changeWrapList', changeWraps.slice( seq - 1 ),
						)
					);
				}
				continue;
			}

			default :
				// invalid request behind server checks should never be possible to happen.
				throw new Error( );
		}
	}

	return(
		list.length > 0
		? ReplyUpdate.Array( list )
		: undefined
	);
};

/*
| Serves a serveRequest.
*/
def.static.serve =
	async function( request, result )
{
	const handler = Self[ Self._serveTable[ request.$type ] ];

	if( !handler )
	{
		Log.log( 'unknown command', request );
		return replyError( 'unknown command' );
	}

	const delay = config.server.delayAjax;
	if( delay !== 0 )
	{
		await timers.setTimeout( delay );
	}

	return await handler( request, result );
};

/*
| Serves an alter request.
*/
def.static._serveAlter =
	async function( request, result )
{
	try
	{
		request = RequestAlter.FromJson( request, Plan.update );
	}
	catch( err )
	{
		console.log( err );
		return replyError( 'request json translation failed' );
	}

	const userCreds = request.userCreds;
	if( !( await Users.testUserCreds( userCreds ) ) )
	{
		return replyError( 'invalid creds' );
	}

	const updates = request.updates;
	const results = [ ];
	for( let update of updates )
	{
		const moment = update.moment;
		let seq = moment.seq;
		const uid = moment.uid;
		let changeWrapList = update.changeWrapList;
		let spaceBox = Spaces.getByUid( uid );

		if( !spaceBox )
		{
			results.push( 'invalid space' );
			continue;
		}

		if( spaceBox.deleted )
		{
			results.push( 'space deleted' );
			continue;
		}

		const refSpace = spaceBox.refSpace;
		if( Spaces.testAccess( userCreds, refSpace ) !== 'rw' )
		{
			results.push( 'no access' );
			continue;
		}

		const boxSeq = spaceBox.seq;
		if( seq === -1 )
		{
			seq = boxSeq;
		}

		if( seq < 0 || seq > boxSeq )
		{
			return replyError( 'invalid seq' );
		}

		try
		{
			// translates the changes if not most recent
			for( let a = seq; a < boxSeq; a++ )
			{
				changeWrapList = spaceBox.getChangeWrap( a ).transform( changeWrapList );
			}

			// this does not await, it is send & pray.
			spaceBox = spaceBox.appendChanges( changeWrapList, userCreds.name );

			Spaces.set( spaceBox );
		}
		catch( error )
		{
			if( error.nonFatal )
			{
				results.push( error.message );
				continue;
			}
			else
			{
				throw error;
			}
		}

		results.push( 'ok' );

		process.nextTick(
			( ) =>
			UpSleeps.wake( MomentSpace, uid )
		);
	}

	return ReplyAlter.create( 'results', ListString.Array( results ) );
};

/*
| Serves an auth request.
*/
def.static._serveAuth =
	async function( request, result )
{
	try
	{
		request = RequestAuth.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	let userCreds = request.userCreds;
	if( userCreds.isVisitor )
	{
		userCreds = Users.NewVisitor( userCreds );
		return(
			userCreds
			? ReplyAuth.create( 'userCreds', userCreds )
			: replyError( 'invalid visitor request' )
		);
	}

	const userInfo = await Users.testUserCreds( userCreds );
	if( !userInfo )
	{
		return replyError( 'invalid password' );
	}

	const dynUserInfo = await Users.getDynamicUserInfo( userInfo );

	return(
		ReplyAuth.create(
			'userCreds',  userCreds,
			'userInfo',   dynUserInfo.shared,
		)
	);
};

/*
| Serves a delete space request.
*/
def.static._serveDeleteSpace =
	async function( request, result )
{
	try
	{
		request = RequestDeleteSpace.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	const userCreds = request.userCreds;
	if( !( await Users.testUserCreds( userCreds ) ) )
	{
		return replyError( 'invalid creds' );
	}

	const uid = request.uid;
	const spaceBox = Spaces.getByUid( uid );
	if( !spaceBox )
	{
		return replyError( 'invalid space' );
	}

	const refSpace = spaceBox.refSpace;

	if(
		(
			refSpace.username === userCreds.name
			||
			(
				refSpace.username === 'plotle'
				&& userCreds.name === config.admin
				&& refSpace.tag !== 'sandbox'
			)
		)
		&& refSpace.tag !== 'home'
	)
	{
		Log.log( 'deleting space on request: ', refSpace.fullname, uid );
		await Spaces.deleteSpace( uid );
		return ReplyDeleteSpace.create( 'uid', uid );
	}
	else
	{
		return replyError( 'not allowed' );
	}
};

/*
| Serves a signUp request.
*/
def.static._serveSignUp =
	async function( request, result )
{
	try
	{
		request = RequestSignUp.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	const userCreds = request.userCreds;
	const mail = request.mail;
	const newsletter = request.newsletter;

	if( userCreds.isVisitor )
	{
		return replyError( 'username must not start with "visitor"' );
	}

	if( userCreds.name.length < 4 )
	{
		return replyError( 'username too short, min. 4 characters' );
	}

	if( userCreds.name === 'plotle' )
	{
		return replyError( 'nope, nice try' );
	}

	const userInfoServer =
		UserInfo.create(
			'name',       userCreds.name,
			'passhash',   userCreds.passhash,
			'mail',       mail,
			'newsletter', newsletter,
		);

	const sUser = await Users.signUp( userInfoServer );
	if( !sUser )
	{
		return replyError( 'Username already taken' );
	}

	const dynUserInfo = await Users.getDynamicUserInfo( userInfoServer );
	return ReplySignUp.create( 'userInfo', dynUserInfo.shared );
};

/*
| Serves an acquire request.
*/
def.static._serveSpaceAcquire =
	async function( request, result )
{
	try
	{
		request = RequestSpaceAcquire.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	const userCreds = request.userCreds;
	if( !( await Users.testUserCreds( userCreds ) ) )
	{
		return replyError( 'invalid creds' );
	}

	const refSpace = request.refSpace;
	const access = Spaces.testAccess( userCreds, refSpace );
	if( access === 'no' )
	{
		return(
			ReplySpaceAcquire.create(
				'access', access,
				'status', 'no access',
			)
		);
	}

	let spaceBox = Spaces.getByRef( refSpace );
	if( spaceBox )
	{
		return(
			ReplySpaceAcquire.create(
				'access', access,
				'owner',
					refSpace.username === userCreds.name
					|| userCreds.name === config.admin,
				'space', spaceBox.current,
				'status', 'served',
			)
		);
	}
	else
	{
		if( request.createMissing )
		{
			spaceBox = await Spaces.createSpace( request.refSpace );
			return(
				ReplySpaceAcquire.create(
					'access', access,
					'owner',
						refSpace.username === userCreds.name
						|| userCreds.name === config.admin,
					'space', spaceBox.current,
					'status', 'served',
				)
			);
		}
		else
		{
			return(
				ReplySpaceAcquire.create(
					'access', access,
					'status', 'nonexistent',
				)
			);
		}
	}
};

/*
| Maps the request types to functions.
*/
def.staticLazy._serveTable =
	( ) =>
	( {
		[ RequestAlter         .$type ]: '_serveAlter',
		[ RequestAuth          .$type ]: '_serveAuth',
		[ RequestChangePwd     .$type ]: '_serveChangePwd',
		[ RequestChangeUserNews.$type ]: '_serveChangeUserNews',
		[ RequestDeleteSpace   .$type ]: '_serveDeleteSpace',
		[ RequestSignUp        .$type ]: '_serveSignUp',
		[ RequestSpaceAcquire  .$type ]: '_serveSpaceAcquire',
		[ RequestUpdate        .$type ]: '_serveUpdate',
	} );

/*
| Gets new changes or waits for them.
*/
def.static._serveUpdate =
	async function( request, result )
{
	try
	{
		request = RequestUpdate.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	const userInfo = await Users.testUserCreds( request.userCreds );
	if( !userInfo )
	{
		return replyError( 'invalid creds' );
	}
	const moments = request.moments;
	let asw = Self._testUpdate( userInfo, moments );

	// if testUpdate failed return the error
	if( asw )
	{
		return asw;
	}

	asw = await Self.conveyUpdate( moments );

	// immediate answer?
	if( asw )
	{
		return asw;
	}

	UpSleeps.putToSleep( moments, result );
	return undefined;
};

/*
| Serves the request to change a users news opt-in.
*/
def.static._serveChangePwd =
	async function( request, result )
{
	try
	{
		request = RequestChangePwd.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	let userInfo = await Users.testUserCreds( request.userCreds );
	if( !userInfo )
	{
		return replyError( 'invalid creds' );
	}

	await Users.changePasshash( userInfo.name, request.passhashNew );

	return ReplyChangePwd.singleton;
};

/*
| Serves the request to change a users news opt-in.
*/
def.static._serveChangeUserNews =
	async function( request, result )
{
	try
	{
		request = RequestChangeUserNews.FromJson( request );
	}
	catch( err )
	{
		Log.log( err.stack );
		return replyError( 'request json translation failed' );
	}

	let userInfo = await Users.testUserCreds( request.userCreds );
	if( !userInfo )
	{
		return replyError( 'invalid creds' );
	}

	await Users.changeNewsletter( userInfo.name, request.newsletter );

	return ReplyChangeUserNews.singleton;
};

/*
| Tests if an update request is legitimate.
|
| ~user:    user info
| ~moments: references to space dynamics to get updates for
*/
def.static._testUpdate =
	function( user, moments )
{
	for( let moment of moments )
	{
		const seq = moment.seq;

		switch( moment.ti2ctype )
		{
			case MomentSpace:
			{
				const spaceBox = Spaces.getByUid( moment.uid );
				if( !spaceBox )
				{
					return replyError( 'unknown space or no access' );
				}

				const refSpace = spaceBox.refSpace;
				const access = Spaces.testAccess( user, refSpace );

				if( access !== 'rw' && access !== 'ro' )
				{
					return replyError( 'unknown space or no access' );
				}

				if( !( seq >= 0 && seq <= spaceBox.seq ) )
				{
					return replyError( 'invalid or missing seq: ' + seq );
				}

				return undefined;
			}

			case MomentUserInfo:
				return(
					moment.username !== user.name
					? replyError( 'userInfo not allowed' )
					: undefined
				);

			default:
				return replyError( 'invalid dynamic reference' );
		}
	}

	// should not happen
	return replyError( 'moment list empty' );
};
