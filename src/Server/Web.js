/*
| Handles plotle specific adaptions to ti2c-web.
*/
def.abstract = true;

import constants from 'node:constants';
import fs        from 'node:fs';
import http      from 'node:http';
import https     from 'node:https';
import process   from 'node:process';
import util      from 'node:util';

import { Self as Bundle         } from '{web:Bundle}';
import { Self as GroupBoolean   } from '{group@boolean}';
import { Self as Handler        } from '{Server/Handler}';
import { Self as Log            } from '{Server/Log}';
import { Self as RefLocation    } from '{Shared/Ref/Location}';
import { Self as RefSpace       } from '{Shared/Ref/Space}';
import { Self as ResourceAjax   } from '{web:Resource/Ajax}';
import { Self as ResourceFile   } from '{web:Resource/File}';
import { Self as ResourceMemory } from '{web:Resource/Memory}';
import { Self as ResourceTwig   } from '{web:Resource/Twig}';
import { Self as Sha1           } from '{web:Sha1}';
import { Self as Spaces         } from '{Server/Spaces}';
import { Self as Ti2cWeb        } from '{web:Self}';
import { Self as UpSleeps       } from '{Server/UpSleep/Manager}';

import '{Shell/Start}';

// The web bundle
let _bundle;

// Hash of docx.js
let _hashDocx;

// Hash of eruda (if used)
let _hashEruda;

// Hash of opentype
let _hashOpentype;

// Hash of minimized opentype
let _hashOpentypeMin;

// the ti2c-web instance
let _tw;

/*
| To be called by when a resource file changes.
|
| ~aFile:      absolute file path
*/
function notify( aFile )
{
	console.log( aFile.asString + ' file event; restarting' );

	// for now just exit for a full restart
	process.exit( 0 );
}

/*
| Creates the basic roster.
|
| ~base: the base dir (absolute dir)
*/
def.static._getBasicRoster =
	async function( base )
{
	const aDirFont = base.d( 'font' );
	const aDirMedia = base.d( 'media' );
	const aDirModules = base.d( 'node_modules' );
	//const aDirOTDist = aDirModules.d( 'opentype.js' ).d( 'dist' );
	const aDirOTDist = base.d( 'dist' );
	const aDirDocx = aDirModules.d( 'docx' ).d( 'dist' );

	let rt =
		ResourceTwig.create(
			'twig:add', 'eruda.js',
			ResourceFile.APathLong( aDirModules.dir( 'eruda' ).f( 'eruda.js' ) ),

			'twig:add', 'docx.js',
			ResourceFile.APathLong( aDirDocx.f( 'index.cjs' ) ),

			'twig:add', 'opentype.js',
			ResourceFile.APathLong( aDirOTDist.f( 'opentype.js' ) ),

			'twig:add', 'opentype.min.js',
			ResourceFile.APathLong( aDirOTDist.f( 'opentype.min.js' ) ),

			'twig:add', 'favicon.ico',
			ResourceFile.APathLong( aDirMedia.f( 'favicon.ico' ) ),

			'twig:add', 'cursor.css',
			ResourceFile.APathLong( aDirMedia.f( 'cursor.css' ) ),

			'twig:add', 'font-DejaVuSans-Regular.ttf',
			ResourceFile.APathLong( aDirFont.f( 'DejaVuSans-Regular.ttf' ) ),

			'twig:add', 'font-DejaVuSans-Regular.woff',
			ResourceFile.APathLong( aDirFont.f( 'DejaVuSans-Regular.woff' ) ),

			'twig:add', 'font-Roboto-Medium.ttf',
			ResourceFile.APathLong( aDirFont.f( 'Roboto-Medium.ttf' ) ),

			'twig:add', 'font-Roboto-Regular.ttf',
			ResourceFile.APathLong( aDirFont.f( 'Roboto-Regular.ttf' ) ),

			'twig:add', 'font-NotoSans-Regular.ttf',
			ResourceFile.APathLong( aDirFont.f( 'NotoSans-Regular.ttf' ) ),

			'twig:add', 'font-NotoSans-Regular.woff',
			ResourceFile.APathLong( aDirFont.f( 'NotoSans-Regular.woff' ) ),

			'twig:add', 'x',
			ResourceAjax.Handler(
				async( type, json, result ) =>
			{
				return await Self.ajax( type, json, result );
			} ),
		);

	return await rt.prepare( );
};

/*
| Handles an ajax call.
|
| ~type:   'close' or 'json'
| ~json:   if json the parsed json
| ~result: the http(s) result object
*/
def.static.ajax =
	async function( type, json, result )
{
	if( type === 'close' )
	{
		if( result.sleepId )
		{
			UpSleeps.closeSleep( result.sleepId );
		}
		return undefined;
	}

	if( type !== 'json' ) throw new Error( );

	return await Handler.serve( json, result );
};

/*
| Prepares ti2c-web.
| Also builds the bundle.
|
| ~base: root directory of server
*/
def.static.prepare =
	async function( base )
{
	Log.log( 'preparing ti2c-web' );

	_tw =
		Ti2cWeb.create(
			'log', Log.log,
			'requestCaching', config.server.cache,
		);

	_bundle =
		await Bundle.build(
			'beautify',         config.shell.bundle.beautify,
			'entry',            'plotle:Shell/Start',
			'extra',            Self._shellExtra( ),
			'globalFlagsDevel', Self._shellGlobals( 'devel' ),
			'globalFlagsIndex', Self._shellGlobals( 'bundle' ),
			'log',              Log.log,
			'minify',           config.shell.bundle.minify,
			'name',             'plotle',
			'offerIndex',       config.shell.bundle.enable,
			'offerDevel',       config.shell.devel.enable,
			'reportDir',
				config.server.report
				? base.d( 'report' )
				: undefined,
		);

	let crt = await Self._getBasicRoster( base );
	if( config.shell.devel.enable )
	{
		crt = crt.appendTwig( _bundle.scripts );
	}

	if( config.shell.bundle.enable )
	{
		crt = crt.appendTwig( _bundle.combine );
	}

	const resOpentype    = crt.get( 'opentype.js' );
	const resOpentypeMin = crt.get( 'opentype.min.js' );
	let resDocx          = crt.get( 'docx.js' );
	resDocx =
		resDocx.create(
			'data',
				'var docx={};(()=>{let exports=docx;'
				+ resDocx.data
				+ '})();'
		);
	crt = crt.set( 'docx.js', resDocx );

	_hashOpentype    = Sha1.calc( resOpentype.data + '' );
	_hashOpentypeMin = Sha1.calc( resOpentypeMin.data + '' );
	_hashDocx        = Sha1.calc( resDocx.data + '' );

	const resEruda = crt.get( 'eruda.js' );
	_hashEruda = Sha1.calc( resEruda.data + '' );

	crt =
		crt
		.rename( 'opentype.js', 'opentype-' + _hashOpentype + '.js' )
		.rename( 'opentype.min.js', 'opentype.min-' + _hashOpentypeMin + '.js' )
		.rename( 'eruda.js', 'eruda-' + _hashEruda + '.js' )
		.rename( 'docx.js', 'docx-' + _hashDocx + '.js' );

	if( config.server.watch )
	{
		for( let res of crt )
		{
			const aPath = res.aPath;
			if( aPath )
			{
				fs.watch( aPath.asString, notify.bind( undefined, aPath ) );
			}
		}
	}

	_tw = _tw.create( 'resources', crt );
};

/*
| Handles https requests.
*/
def.static._handle =
	async function( request, result )
{
	const pathname = _tw.pathname( request );
	if( pathname === undefined )
	{
		_tw.webError( result, 400, 'invalid URL' );
		return;
	}

	const parts = pathname.split( '/' );

	const refLoc = RefLocation.Path( parts );
	if( refLoc === false )
	{
		_tw.webError( result, 404, 'invalid space link' );
		return;
	}

	if( parts.length === 1 )
	{
		switch( parts[ 0 ] )
		{
			case 'robots.txt':
			{
				const header =
				{
					'content-type': 'text/plain',
					'date': new Date( ).toUTCString( ),
				};
				result.writeHead( 200, header );
				let lines;
				switch( request.headers.host )
				{
					case 'plotle.org':
					case 'www.plotle.org':
						lines =
						[
							'User-agent: *',
							'Disallow: /',
						];
						break;

					default:
						lines =
						[
							'User-agent: *',
							'Disallow: /sp/plotle/sandbox',
							'Allow: /',
							'Sitemap: https://www.plotle.com/sitemap.xml',
						];
						break;
				}

				result.end( lines.join( '\n' ), 'utf-8' );
				return;
			}
			case 'sitemap.xml':
			{
				const header =
				{
					'content-type': 'application/xml',
					'date': new Date( ).toUTCString( ),
				};
				result.writeHead( 200, header );
				const data = Spaces.sitemap( );
				result.end( data, 'utf-8' );
				return;
			}
		}
	}

	if( refLoc )
	{
		if( refLoc.devel === false && !config.shell.bundle.enable )
		{
			_tw.webError( result, 404, 'bundle not enabled' );
			return;
		}

		if( refLoc.devel === true && !config.shell.devel.enable )
		{
			_tw.webError( result, 404, 'devel not enabled' );
			return;
		}

		const header =
		{
			'content-type': 'text/html',
			'cache-control': 'no-cache',
			'date': new Date( ).toUTCString( ),
			'Cross-Origin-Embedder-Policy': 'require-corp',
			'Cross-Origin-Opener-Policy': 'same-origin',
		};

		result.writeHead( 200, header );
		const data = Self._pageSpace( refLoc );
		result.end( data, 'utf-8' );
		return;
	}

	await _tw.requestListener( request, result, pathname );
};

def.static.start =
	async function( )
{
	await _startMainServer( );
	await _startRedirectServer( );
};

/*
| Creates the index html site for a space "page".
|
| ~refLoc: reference to the location for the page
*/
def.static._pageSpace =
	function( refLoc )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( refLoc.ti2ctype !== RefLocation ) throw new Error( );
/**/}

	const devel = refLoc.devel;
	const refSpace = refLoc.ref;

	let spaceBox = Spaces.getByRef( refSpace );
	let content;
	if( !spaceBox || !spaceBox.space.publicReadable )
	{
		content = 'This plotle space does not exist or is private.';
	}
	else
	{
		content = spaceBox.searchEngineContent;
	}

	const data = [ ];

	data.push(
		'<!DOCTYPE HTML>',
		'<html style="height: 100%">',
		'<head>',
		'<meta charset="utf-8"/>',
	);

	switch( refSpace )
	{
		case RefSpace.PlotleHome:
			data.push( '<title>plotle</title>' );
			data.push( '<link rel="canonical" href="https://plotle.com/">' );
			break;

		case RefSpace.PlotleSandbox:
			data.push( '<title>plotle – sandbox</title>' );
			data.push( '<link rel="canonical" href="https://plotle.com/sp/plotle/sandbox/">' );
			break;

		default:
			data.push( '<title>plotle – ' + refSpace.fullname + '</title>' );
			data.push(
				'<link rel="canonical" href="https://plotle.com/sp'
				+ '/' + refSpace.fullname
				+ '/">'
			);
			break;
	}

	data.push(
		'<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">',
		'<link rel="icon" href="/favicon.ico" type="image/x-icon">',
		'<link rel="stylesheet" href="/cursor.css" type="text/css">',
	);

	if( devel )
	{
		data.push(
			'<script src="/opentype-'
			+ _hashOpentype
			+ '.js" type="text/javascript" defer></script>'
		);
	}
	else
	{
		data.push(
			'<script src="/opentype.min-'
			+ _hashOpentypeMin
			+ '.js" type="text/javascript" defer></script>'
		);
	}

	// TODO create uglified version
	data.push(
		'<script src="/docx-'
			+ _hashDocx
			+ '.js" type="text/javascript" defer></script>'
	);

	if( config.shell.eruda )
	{
		data.push(
			'<script src="/eruda-'
			+ _hashEruda
			+ '.js" type="text/javascript"></script>'
			+ '<script>eruda.init();</script>'
		);
	}

	data.push(
		'<meta id="viewport"'
		+ ' name="viewport"'
		+ ' content="width=device-width,'
		+    ' height=device-height,'
		+    ' initial-scale=1,'
		+    ' maximum-scale=1'
		+ '">'
	);

	data.push(
		'<style>'
		+ 'body{ height: 100%; margin: 0; overflow: hidden; background: white;}'
		+ '@media (prefers-color-scheme: dark) {'
		+   'body{ background: black;}'
		+   '#canvas{ background-color:black; }'
		+ '}'
		+ '</style>'
	);

	if( devel )
	{
		const scripts = _bundle.scripts;
		data.push( _bundle.importMap );
		for( let key of scripts.keys )
		{
			data.push( '<script src="/' + key + '" type="module"></script>' );
		}
	}
	else
	{
		data.push( '<script src="/' + _bundle.name + '" type="module"></script>' );
	}

	data.push(
		'</head>',
		'<body>',
		'<canvas id="canvas" tabindex="-1">',
		content,
		'</canvas>',
		'<textarea'
		+ ' id="input"'
		+ ' tabindex="-1"'
		+ ' style="position: absolute; top: 0px; left:-200px; height:10px; width: 100px;"'
//		+ ' style="position: absolute; top: 0px; left: 0px; height:10px; width: 100px;"'
		+ ' autocapitalize="off"'
		+ ' autocorrect="off"'
		+ ' autocomplete="off"'
		+ ' spellcheck="false">',
		'</textarea>',
		'<span'
		+ ' id="span"'
		+ ' tabindex="-1">',
		'</span>',
		'</body>',
		'</html>',
	);

	return data.join( '' );
};


/*
| The shell config as extra resource.
*/
def.static._shellExtra =
	function( )
{
	let t =
	[
		'globalThis.config = Object.freeze( { ',
		'  dragBox: '            + config.shell.dragBox + ',',
		'  dragTime: '           + config.shell.dragTime + ',',
		'  glintCacheLimit: '    + config.shell.glintCacheLimit + ',',
		'  lineClickDistance: '  + config.shell.lineClickDistance + ',',
		'  splitClickDistance: ' + config.shell.splitClickDistance + ',',
		'  superSampling: '      + config.shell.superSampling + ',',
		'  textWheelSpeed: '     + config.shell.textWheelSpeed + ',',
		'  maxUndo: '            + config.shell.maxUndo + ',',
		'  zoomMax: '            + config.shell.zoomMax + ',',
		'  zoomMin: '            + config.shell.zoomMin + ',',
		'} );',
	];
	const res = ResourceMemory.JsData( t.join( '\n' ) );
	return ResourceTwig.create( 'twig:add', 'plotle/config-shell.js', res );
};

/*
| The shell's globals.
*/
def.static._shellGlobals =
	function( mode )
{
/**/if( CHECK )
/**/{
/**/	if( mode !== 'bundle' && mode !== 'devel' ) throw new Error( );
/**/}

	return(
		GroupBoolean.Table( {
			CHECK:      config.shell[ mode ].check,
			ERUDA:      config.shell.eruda,
			FAILSCREEN: config.shell[ mode ].failScreen,
			NODE:       false,
		} )
	);
};


/*
| Starts the web server.
*/
async function _startMainServer( )
{
	const protocol = config.network.main.protocol;
	let port = config.network.main.port;
	const listen = config.network.listen;

	if( port === 0 )
	{
		port = protocol === 'http' ? 8833 : 443;
	}

	switch( protocol )
	{
		case 'https':
		{
			Log.log( 'starting server @ https://' + ( listen || '*' ) + '/:' + port );

			const cert = ( fs.readFileSync( config.https.cert ) ) + '';
			const key = ( fs.readFileSync( config.https.key ) ) + '';
			const options =
			{
				secureOptions: constants.SSL_OP_NO_SSLv3 | constants.SSL_OP_NO_SSLv2,
				cert: cert,
				key: key
			};
			const server = https.createServer( options, Self._handle );
			const promise = util.promisify( server.listen.bind( server ) );
			await promise( port, listen );
			break;
		}

		case 'http':
		{
			Log.log( 'starting server @ http://' + ( listen || '*' ) + '/:' + port );
			const server = http.createServer( Self._handle );
			const promise = util.promisify( server.listen.bind( server ) );
			await promise( port, listen );
			break;
		}

		default: throw new Error( );
	}
}

/*
| Starts the redirect server.
*/
async function _startRedirectServer( )
{
	const protocol = config.network.redirect.protocol;

	if( protocol === '' ) return;
	if( protocol !== 'http' ) throw new Error( );

	const port = config.network.redirect.port;
	const destination = config.network.redirect.destination;
	const listen = config.network.listen;
	Log.log( 'starting redirect @ http://' + ( listen || '*' ) + '/:' + port );

	const handler =
		( request, result ) =>
	{
		result.writeHead( 307, { Location: destination + request.headers.host + request.url } );
		result.end( 'go use https' );
	};

	const server = http.createServer( handler );
	const promise = util.promisify( server.listen.bind( server ) );
	await promise( port, listen );
}
