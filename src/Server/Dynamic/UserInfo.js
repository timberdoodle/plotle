/*
| Dynamic user information for the client.
*/
def.extend = 'Shared/Dynamic/Current/UserInfo';

def.attributes =
{
	// cached changeWraps
	changeWraps:
	{
		type: [ 'undefined', 'Shared/ChangeWrap/List' ],
		defaultValue: 'import( "Shared/ChangeWrap/List" ).Empty',
	},
};

import { Self as ChangeWrap      } from '{Shared/ChangeWrap/Self}';
import { Self as CurrentUserInfo } from '{Shared/Dynamic/Current/UserInfo}';
import { Self as Sequence        } from '{ot:Change/Sequence}';

/*
| Returns the altered dynamic.
|
| ~arg: change, several changes or array of changes
*/
def.proto.alter =
	function( ...args )
{
	const a0 = args[ 0 ];

	let sq;
	if( a0.ti2ctype === Sequence )
	{
		sq = a0;
	}
	else if( Array.isArray( a0 ) )
	{
		sq = Sequence.Array( a0 );
	}
	else
	{
		sq = Sequence.Array( args );
	}

	const changeWrap = ChangeWrap.Wrapped( sq );
	const moment = this.moment;
	return(
		this.create(
			'current', changeWrap.changeTree( this.current ),
			'moment', moment.create( 'seq', moment.seq + 1 ),
			'changeWraps', this.changeWraps.append( changeWrap ),
		)
	);
};

/*
| The client/server shared version.
*/
def.lazy.shared =
	function( )
{
	return(
		CurrentUserInfo.create(
			'current', this.current,
			'moment',  this.moment,
		)
	);
};
