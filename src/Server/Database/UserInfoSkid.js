/*
| A user on a skid for database storage.
*/
def.attributes =
{
	// the users email
	mail: { type: 'string', defaultValue: '""', json: true },

	// the username
	name: { type: 'string', json: true },

	// true if the user checked okay with news emails
	news: { type: 'boolean', json: true },

	// password hash
	passhash: { type: 'string', json: true },
};

def.json = true;

import { Self as UserInfo } from '{Server/User/Info}';

/*
| Creates a UserInfoSkid from a ServerUserInfo.
*/
def.static.FromUserInfo =
	function( user )
{
	const skid =
		Self.create(
			'mail',     user.mail,
			'name',     user.name,
			'news',     user.newsletter,
			'passhash', user.passhash
		);
	ti2c.aheadValue( skid, 'asUser', user );
	return skid;
};

/*
| Creates a ServerUserInfo from the skid.
*/
def.lazy.asUser =
	function( )
{
	return(
		UserInfo.create(
			'mail',       this.mail,
			'name',       this.name,
			'newsletter', this.news,
			'passhash',   this.passhash
		)
	);
};
