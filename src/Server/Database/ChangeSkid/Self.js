/*
| A change list for database storage.
*/
def.attributes =
{
	// date of the change
	date: { type: 'integer', json: true },

	// changes
	changes: { type: 'ot:Change/Sequence', json: true },

	// the user who issued the change
	user: { type: 'string', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

/*
| Creates a changeSkid from a changeWrap.
|
| ~changeWrap: the change wrap to turn into a skid
| ~username:   the username that sends the changeWrap
| ~date:       if defined use this date
*/
def.static.ChangeWrap =
	function( changeWrap, username, date )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( username ) !== 'string' ) throw new Error( );
/**/}

	if( !changeWrap.changes ) return undefined;

	return(
		Self.create(
			'changes', changeWrap.changes,
			'user', username,
			'date', date,
		)
	);
};
