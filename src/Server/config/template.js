/*
| Boilerplate for server configs.
*/
import cfg from 'config:template';

// username of admin
cfg.admin =
{
	type: [ 'undefined', 'string' ],
};


/*
| Database settings.
*/
cfg.database = { };

// if true establishes a new database if missing
cfg.database.establish =
{
	type: 'boolean',
	defaultValue: false,
};

// name of the databse
cfg.database.name =
{
	type: [ 'undefined', 'string' ],
};


/*
| Https settings.
*/
cfg.https = { };

// https enabled
cfg.https.enabled =
{
	type: 'boolean',
	defaultValue: false,
};

// https cert
cfg.https.cert =
{
	type: [ 'undefined', 'string' ],
};

// http key
cfg.https.key =
{
	type: [ 'undefined', 'string' ],
};


/*
| Network settings.
*/
cfg.network = { };

// listens on this IP (default all)
cfg.network.listen =
{
	type: [ 'string', 'undefined' ],
};

/*
| Network settings for main server.
*/
cfg.network.main = { };

// http or https?
cfg.network.main.protocol =
{
	type: 'string',
	defaultValue: 'http',
};

// port to listen on
// 0 defaults to 8833 when http and 443 when https
cfg.network.main.port =
{
	type: 'integer',
	defaultValue: 0,
};

/*
| Network setting for redirect server.
*/
cfg.network.redirect = { };

// http or "" (meaning no redirect server)
cfg.network.redirect.protocol =
{
	type: 'string',
	defaultValue: '',
};

// port to listen on
cfg.network.redirect.port =
{
	type: 'integer',
	defaultValue: 80,
};

// where to redirect them to
cfg.network.redirect.destination =
{
	type: 'string',
	defaultValue: 'https://',
};


/*
| Server settings.
*/
cfg.server = { };

// if false each client is told not to cache http requests.
// TODO move to http/s
cfg.server.cache =
{
	type: 'boolean',
	defaultValue: false,
};

// if true enables checking code on server.
cfg.server.check =
{
	type: 'boolean',
	defaultValue: true,
};

/*
| Delays all ajax requests by this amount of milliseconds.
| Used for network behavior debugging.
*/
cfg.server.delayAjax =
{
	type: 'number',
	defaultValue: 0,
};

// if true writes sourcemap to disk
cfg.server.report =
{
	type: 'boolean',
	defaultValue: false,
};

// if true the server will die on bad requests from client
cfg.server.sensitive =
{
	type: 'boolean',
	defaultValue: false,
};

// server will watch file system for changes.
cfg.server.watch =
{
	type: 'boolean',
	defaultValue: true,
};


/*
| Shell settings.
*/
cfg.shell = { };

// milliseconds after mouse down, dragging starts
cfg.shell.dragTime =
{
	type: 'number',
	defaultValue: 400,
};

// pixels after mouse down and move, dragging starts
cfg.shell.dragBox =
{
	type: 'number',
	defaultValue: 10,
};

// enables eruda.js
cfg.shell.eruda =
{
	type: 'boolean',
	defaultValue: false,
};

// maximum size of a glint graphic cache
// in width * height
cfg.shell.glintCacheLimit =
{
	type: 'number',
	defaultValue: 32767,
};

// distance to which lines react on clicks
cfg.shell.lineClickDistance =
{
	type: 'number',
	defaultValue: 15,
};

// distance to which the split reacts on clicks
cfg.shell.splitClickDistance =
{
	type: 'number',
	defaultValue: 7,
};

// max. number of undo events queued
cfg.shell.maxUndo =
{
	type: 'number',
	defaultValue: 1000,
};

// if > 1 render in higher resolution than display
cfg.shell.superSampling =
{
	type: 'number',
	defaultValue: 1,
};

// pixels to scroll on a wheel event
cfg.shell.textWheelSpeed =
{
	type: 'number',
	defaultValue: 60,
};

// zooming settings
cfg.shell.zoomMin =
{
	type: 'number',
	defaultValue: -150,
};

cfg.shell.zoomMax =
{
	type: 'number',
	defaultValue: 150,
};


/*
| Bundle settings.
*/
cfg.shell.bundle = { };

// enables beautifying of minified code
// only applicable with minify
cfg.shell.bundle.beautify =
{
	type: 'boolean',
	defaultValue: false,
};

// enables checking
cfg.shell.bundle.check =
{
	type: 'boolean',
	defaultValue: false,
};

// enables the bundle
cfg.shell.bundle.enable =
{
	type: 'boolean',
	defaultValue: true,
};

// enables sourceMap
// only applicable with minify
cfg.shell.bundle.sourceMap =
{
	type: 'boolean',
	defaultValue: true,
};

// enables the failScreen
cfg.shell.bundle.failScreen =
{
	type: 'boolean',
	defaultValue: true,
};

// enables minifying
cfg.shell.bundle.minify =
{
	type: 'boolean',
	defaultValue: true,
};


/*
| Shell devel settings.
*/
cfg.shell.devel = { };

// if enabled this access is provided
cfg.shell.devel.enable =
{
	type: 'boolean',
	defaultValue: true,
};

// enables checking
cfg.shell.devel.check =
{
	type: 'boolean',
	defaultValue: true,
};

// enables the failScreen
cfg.shell.devel.failScreen =
{
	type: 'boolean',
	defaultValue: false,
};

