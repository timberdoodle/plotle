/*
| The test suite root.
*/
import fs from 'node:fs/promises';
import vm from 'node:vm';

/*
| The read template.
*/
let template = { };

/*
| Reads 'filename' and returns it as string.
| Throws error if not existing.
*/
async function read( filename )
{
	let data;
	try
	{
		data = await fs.readFile( filename, { encoding: 'utf8' } );
	}
	catch( e )
	{
		console.log( 'Cannot read file "' + filename + '"' );
		throw new Error( );
	}

	// converts Buffer to string
	return data + '';
}

/*
| Linker for config files.
*/
async function linkerConfig( proxy, specifier, referencingModule )
{
	switch( specifier )
	{
		case 'config':
		{
			const exportNames = [ 'default' ];
			const moduleConfig =
				new vm.SyntheticModule(
					exportNames,
					( ) =>
					{
						for( let key of exportNames )
						{
							moduleConfig.setExport( key, proxy );
						}
					},
					{
						identifier: specifier,
						context: referencingModule.context,
					}
				);
			return moduleConfig;
		}

		default:
			throw new Error( 'invalid import in config file' );
	}
}

/*
| Linker for template files.
*/
async function linkerTemplate( specifier, referencingModule )
{
	if( specifier !== 'config:template' )
	{
		throw new Error( 'invalid import in config template file' );
	}

	const exportNames = [ 'default' ];
	const moduleConfig =
		new vm.SyntheticModule(
			exportNames,
			( ) =>
			{
				for( let key of exportNames )
				{
					moduleConfig.setExport( key, template );
				}
			},
			{
				identifier: specifier,
				context: referencingModule.context,
			}
		);
	return moduleConfig;
}

/*
| Loads a template.
*/
async function loadTemplateFile( filename )
{
	const code = await read( filename );
	const stm =
		new vm.SourceTextModule(
			code,
			{
				identifier: filename,
				context: vm.createContext( ),
			}
		);
	await stm.link( linkerTemplate );
	await stm.evaluate( );
}

/*
| Builds the config tree from template defaults.
|
| ~cfg: position in the config tree.
| ~tpl: position in the template tree.
*/
function buildDefault( cfg, tpl )
{
	for( let key of Object.keys( tpl ) )
	{
		const tpls = tpl[ key ];

		if( tpls.type )
		{
			const dv = tpls.defaultValue;
			//testType( tpls.type, dv );
			cfg[ key ] = dv;
		}
		else
		{
			const cfgs = cfg[ key ] = { };
			buildDefault( cfgs, tpls );
		}
	}
}

/*
| Builds a proxy interface.
|
| ~cfg: config object put proxies into.
| ~tpl: template
*/
function buildProxy( cfg, tpl )
{
	const attrs = { };
	const pxSub = { };

	for( let key of Object.keys( tpl ) )
	{
		const tpls = tpl[ key ];
		if( tpls.type )
		{
			attrs[ key ] = tpls.type;
		}
		else
		{
			pxSub[ key ] = buildProxy( cfg[ key ], tpls );
		}
	}

	return(
		new Proxy(
			{ },
			{
				get: ( trg, prop, recv ) =>
				{
					if( attrs[ prop ] )
					{
						return cfg[ prop ];
					}
					else if( pxSub[ prop ] )
					{
						return pxSub[ prop ];
					}
					else
					{
						throw new Error( 'unknown config option "' + prop + '"' );
					}
				},
				set: ( trg, prop, val ) =>
				{
					if( attrs[ prop ] )
					{
						// TODO type check
						cfg[ prop ] = val;
						return true;
					}
					else
					{
						throw new Error( 'invalid config assignment "' + prop + '"' );
					}
				}
			},
		)
	);
}

/*
| Loads a configuration.
|
| ~filename: filename of the config file
| ~code:     if defined does not read the config filename
*/
async function loadConfigFile( filename, code )
{
	if( code === undefined )
	{
		code = await read( filename );
	}

	const stm =
		new vm.SourceTextModule(
			code,
			{
				identifier: filename,
				context: vm.createContext( ),
			}
		);

	let proxy = buildProxy( config, template );

	await stm.link( linkerConfig.bind( undefined, proxy ) );
	await stm.evaluate( );
}

/*
| Loads a configuration.
|
| ~filenameTemplate: filename of the template
| ~filenameConfig:   filename of the config file
| ~codeConfig:       if defined uses this as content of config file
|                    instead of reading it
*/
export async function loadConfig( filenameTemplate, filenameConfig, codeConfig )
{
	await loadTemplateFile( filenameTemplate );
	globalThis.config = { };
	buildDefault( config, template );
	await loadConfigFile( filenameConfig, codeConfig );
}
