/*
| The upsleep manager
*/
def.abstract = true;

import timers from 'node:timers';

import { Self as GroupUpSleep   } from '{group@Server/UpSleep/Self}';
import { Self as Handler        } from '{Server/Handler}';
import { Self as MomentSpace    } from '{Shared/Dynamic/Moment/Space}';
import { Self as MomentUserInfo } from '{Shared/Dynamic/Moment/UserInfo}';
import { Self as ReplyUpdate    } from '{Shared/Reply/Update}';
import { Self as UpSleep        } from '{Server/UpSleep/Self}';

// A group of all clients waiting for an update.
let _upSleeps;

// id for next upsleep
let _nextSleepId;

/*
| A sleeping update closed prematurely.
*/
def.static.closeSleep =
	function( sleepId )
{
	const sleep = _upSleeps.get( sleepId );
	// maybe it just had expired at the same time
	if( !sleep ) return;
	clearTimeout( sleep.timer );
	_upSleeps = _upSleeps.remove( sleepId );
};

/*
| Initializes the upsleep manager.
*/
def.static.init =
	function( )
{
	_upSleeps = GroupUpSleep.Empty;
	_nextSleepId = 1;
};

/*
| Puts a request to sleep.
|
| ~moments: the list of moments of dynamics the client is sleeping for
| ~result:   the node result handler of the clients request
*/
def.static.putToSleep =
	function( moments, result )
{
	// if not an immediate answer, the request is put to sleep
	const sleepId = '' + _nextSleepId++;
	const timer = timers.setTimeout( Self._expireUpdateSleep, 60000, sleepId );
	_upSleeps =
		_upSleeps.set(
			sleepId,
			UpSleep.create(
				'moments', moments,
				'result', result,
				'timer', timer
			)
		);
	result.sleepId = sleepId;
};

/*
| Wakes up any sleeping updates and gives them data if applicatable.
|
| FUTURE allow multiple wakings at once.
|
| ~type: type of thing to wake for
| ~spec: specifier of the reference to wake for (uid or username)
*/
def.static.wake =
	async function( type, spec )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( spec ) !== 'string' ) throw new Error( );
/**/}

	// TODO cache change lists
	// to answer the same to multiple clients.
	for( let key of _upSleeps.keys )
	{
		const sleep = _upSleeps.get( key );
		const moments = sleep.moments;

		let match;
		for( let moment of moments )
		{
			if(
				moment.ti2ctype === type
				&& (
					( type === MomentSpace && moment.uid === spec )
					|| ( type === MomentUserInfo && moment.username === spec )
				)
			)
			{
				match = true;
				break;
			}
		}

		// none of the moments matched
		if( !match ) continue;

		// this sleep needs to be waked
		const asw = await Handler.conveyUpdate( sleep.moments );
		if( !asw ) continue;

		timers.clearTimeout( sleep.timer );
		_upSleeps = _upSleeps.remove( key );

		const result = sleep.result;
		result.writeHead(
			200,
			{
				'content-type' : 'application/json',
				'cache-control' : 'no-cache',
				'date' : new Date().toUTCString()
			}
		);
		result.end( asw.jsonfy( ) );
	}
};

/*
| A sleeping update expired.
*/
def.static._expireUpdateSleep =
	function( sleepId )
{
	const sleep = _upSleeps.get( sleepId );
	// maybe it just had expired already
	if( !sleep ) return;

	_upSleeps = _upSleeps.remove( sleepId );
	const asw = ReplyUpdate.create( );
	const result = sleep.result;
	result.writeHead(
		200,
		{
			'Content-Type' : 'application/json',
			'Cache-Control' : 'no-cache',
			'Date' : new Date().toUTCString()
		}
	);

	result.end( asw.jsonfy( ) );
};

