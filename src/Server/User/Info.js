/*
| Extended user info.
*/
def.attributes =
{
	// the users email address
	mail: { type: 'string', defaultValue: '""' },

	// the username
	name: { type: 'string' },

	// if the user checked okay with news emails
	newsletter: { type: 'boolean' },

	// password hash
	passhash: { type: 'string' },

	// if defined, the user info for clients
	dynamicUserInfo: { type: [ 'undefined', 'Server/Dynamic/UserInfo' ] },
};
