/*
| Holds and manages all user infos.
*/
def.abstract = true;

import { Self as ChangeListMove   } from '{ot:Change/List/Move}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as DynamicUserInfo  } from '{Server/Dynamic/UserInfo}';
import { Self as GroupUserInfo    } from '{group@Server/User/Info}';
import { Self as ListRefSpace     } from '{list@Shared/Ref/Space}';
import { Self as MomentUserInfo   } from '{Shared/Dynamic/Moment/UserInfo}';
import { Self as RefSpace         } from '{Shared/Ref/Space}';
import { Self as Repository       } from '{Server/Database/Repository}';
import { Self as SharedUserInfo   } from '{Shared/User/Info}';
import { Self as ServerUserInfo   } from '{Server/User/Info}';
import { Self as Spaces           } from '{Server/Spaces}';
import { Self as TraceRoot        } from '{Shared/Trace/Root}';
import { Self as UpSleeps         } from '{Server/UpSleep/Manager}';

/*
| Table of all cached user infos.
*/
let _cache;

/*
| Next visitors id.
*/
let _nextVisitor = 1000;

/*
| Adds the reference of a space being owned by an user.
*/
def.static.addUserRefSpace =
	async function( refSpace )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const username = refSpace.username;

	let userInfoServer = _cache.get( username );
/**/if( CHECK && !userInfoServer ) throw new Error( );

	let dynUserInfo = userInfoServer.dynamicUserInfo;

	// current user info
	const userInfoShared = dynUserInfo.current;
	const lso = userInfoShared.listSpacesOwned;
	for( let r of userInfoShared.listSpacesOwned )
	{
		if( r === refSpace ) throw new Error( );
	}

	dynUserInfo =
		dynUserInfo.alter(
			ChangeListMove.create(
				'val', refSpace,
				'traceTo',
					TraceRoot.userInfo
					.add( 'listSpacesOwned', lso.length ),
			)
		);

	_cache =
		_cache.set(
			username,
			userInfoServer.create( 'dynamicUserInfo', dynUserInfo ),
		);

	await UpSleeps.wake( MomentUserInfo, username );
};

/*
| Changes a user newsletter setting
*/
def.static.changeNewsletter =
	async function( username, newsletter )
{
	let userInfoServer = _cache.get( username );
	if( userInfoServer.newsletter === newsletter )
	{
		return;
	}

	let dynUserInfo = userInfoServer.dynamicUserInfo;
	dynUserInfo =
		dynUserInfo.alter(
			ChangeTreeAssign.create(
				'prev', userInfoServer.newsletter,
				'val', newsletter,
				'trace', TraceRoot.userInfo.add( 'newsletter' ),
			)
		);

	userInfoServer =
		userInfoServer.create(
			'dynamicUserInfo', dynUserInfo,
			'newsletter', newsletter,
		);
	_cache = _cache.set( username, userInfoServer );

	await Repository.saveUser( userInfoServer );
	await UpSleeps.wake( MomentUserInfo, username );
};

/*
| Changes a user passhash.
*/
def.static.changePasshash =
	async function( username, passhash )
{
	let userInfoServer = _cache.get( username );

	if( userInfoServer.passhash === passhash )
	{
		return;
	}

	userInfoServer = userInfoServer.create( 'passhash', passhash );
	_cache = _cache.set( username, userInfoServer );
	await Repository.saveUser( userInfoServer );
};

/*
| Deletes the reference of a space being owned by an user.
*/
def.static.delUserRefSpace =
	async function( refSpace )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const username = refSpace.username;
	let userInfoServer = _cache.get( username );

	// user not loaded/active?
	if( !userInfoServer ) return;

	let dynUserInfo = userInfoServer.dynamicUserInfo;

	// current user info
	const curUserInfo = dynUserInfo.current;
	const listSpacesOwned = curUserInfo.listSpacesOwned;

	// gets the index of the user space reference
	let a, len;
	for( a = 0, len = listSpacesOwned.length; a < len; a++ )
	{
		if( listSpacesOwned.get( a ) === refSpace )
		{
			break;
		}
	}

	if( !a >= len ) throw new Error( );

	dynUserInfo =
		dynUserInfo.alter(
			ChangeListMove.create(
				'traceFrom',
					TraceRoot.userInfo
					.add( 'listSpacesOwned', a ),
				'val', refSpace,
			)
		);

	_cache =
		_cache.set(
			username,
			userInfoServer.create( 'dynamicUserInfo', dynUserInfo ),
		);

	await UpSleeps.wake( MomentUserInfo, username );
};

/*
| Gets the dynamic user info for clients.
*/
def.static.getDynamicUserInfo =
	async function( userInfoServer )
{
/**/if( CHECK && userInfoServer.ti2ctype !== ServerUserInfo ) throw new Error( );

	let dynUserInfo = userInfoServer.dynamicUserInfo;
	if( dynUserInfo )
	{
		return dynUserInfo;
	}

	const list = [ ];
	const names = await Repository.getSpaceNames( );
	const username = userInfoServer.name;

	for( let name of names )
	{
		const p = name.split( ':' );

		if( p[ 0 ] !== 'spaces' )
		{
			throw new Error( );
		}

		if( p[ 1 ] !== username )
		{
			continue;
		}

		list.push( RefSpace.UsernameTag( p[ 1 ], p[ 2 ] ) );
	}

	dynUserInfo =
		DynamicUserInfo.create(
			'current',
				SharedUserInfo.create(
					'newsletter', userInfoServer.newsletter,
					'listSpacesOwned', ListRefSpace.Array( list ),
				),
			'moment',  MomentUserInfo.SeqUsername( 1, username ),
		);

	_cache =
		_cache.set(
			username,
			userInfoServer.create( 'dynamicUserInfo', dynUserInfo )
		);

	return dynUserInfo;
};

/*
| Returns the user info by username.
*/
def.static.getByName =
	function( username )
{
	return _cache.get( username );
};

/*
| Creates the user nexus from database.
*/
def.static.init =
	async function( )
{
	const names = await Repository.getUserNames( );
	const group = { };

	for( let name of names )
	{
		const ui = await Repository.getUser( name );
		group[ ui.name ] = ui;
	}
	_cache = GroupUserInfo.Table( group );
};

/*
| Creates a new visitor.
*/
def.static.NewVisitor =
	function( userCreds )
{
	if( userCreds.name !== 'visitor' ) return undefined;

	let name;

	do
	{
		name = 'visitor-' + ( ++_nextVisitor );
	}
	while( _cache.get( name ) );

	_cache =
		_cache.set(
			name,
			ServerUserInfo.create(
				'name',       name,
				'passhash',   userCreds.passhash,
				'newsletter', false,
			)
		);

	return userCreds.create( 'name', name );
};

/*
| Signs up a user.
*/
def.static.signUp =
	async function( userInfoServer )
{
/**/if( CHECK && userInfoServer.ti2ctype !== ServerUserInfo ) throw new Error( );

	if( userInfoServer.dynamicUserInfo ) throw new Error( );

	const username = userInfoServer.name;

	if( _cache.get( username ) )
	{
		// user already registered
		return false;
	}

	const dynamicUserInfo =
		DynamicUserInfo.create(
			'current',
				SharedUserInfo.create(
					'newsletter', userInfoServer.newsletter,
					'listSpacesOwned', ListRefSpace.Empty,
				),
			'moment',
				MomentUserInfo.create(
					'seq', 1,
					'username', username,
				)
		);
	userInfoServer = userInfoServer.create( 'dynamicUserInfo', dynamicUserInfo );
	_cache = _cache.set( username, userInfoServer );
	await Repository.saveUser( userInfoServer );
	await Spaces.createSpace( RefSpace.UsernameTag( username, 'home' ) );
	return true;
};

/*
| Tests if the user creds are ok.
|
| If so loads and returns the matching user info.
*/
def.static.testUserCreds =
	async function( userCreds )
{
	const userInfo = Self.getByName( userCreds.name );

	if( !userInfo ) return false;
	if( userInfo.passhash !== userCreds.passhash ) return false;

	Repository.seenUser( userInfo );

	return userInfo;
};
