/*
| The user is moving a stroke handle.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// the fabric item trace hovered upon
	fHover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the visual item trace hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the joint number of stroke being moved
	jointNr: { type: 'number' },

	// moved to this point
	movedPoint: { type: [ 'undefined', 'gleam:Point' ] },

	// mouse down point on drag creation
	pointStart: { type: 'gleam:Point' },

	// the stroke to drag
	stroke: { type: 'Shell/View/Space/Item/Stroke' },
};

import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as Sequence         } from '{ot:Change/Sequence}';

/*
| The changes this action applies on the fabric tree.
*/
def.lazy.changesTransient =
	function( )
{
	const stroke = this.stroke;

	// changed joint
	const jchg = this.fHover || this.movedPoint;
	const jprev = stroke.ancillary.joints.get( this.jointNr ).pos;

	if( !jchg || jprev === jchg )
	{
		return Sequence.Empty;
	}

	return(
		Sequence.Elements(
			ChangeTreeAssign.create(
				'trace', stroke.ancillary.trace.add( 'joints', this.jointNr ).add( 'pos' ),
				'prev', jprev,
				'val', jchg
			)
		)
	);
};

/*
| The opposite stroke joint.
| Used as it should not connect the opposite to itself.
*/
def.lazy.oppositeStrokeJoint =
	function( )
{
	return this.stroke.ancillary.joints.get( 1 - this.jointNr );
};
