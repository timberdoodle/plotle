/*
| The user is dragging a (visual) item.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// the items to drag
	items: { type: [ 'undefined', 'Shell/View/Space/Item/Set' ] },

	// drags the items by this x/y
	moveBy: { type: 'gleam:Point' },

	// mouse down point on drag creation
	pointStart: { type: 'gleam:Point' },

	// the zone of items when started
	// (used for snapping)
	startZone: { type: 'gleam:Rect' },
};

import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Sequence         } from '{ot:Change/Sequence}';

/*
| The changes an ongoing action applies on the fabric tree.
*/
def.lazy.changesTransient =
	function( )
{
	const moveBy = this.moveBy;
	const changes = [ ];

	if( moveBy.x === 0 && moveBy.y === 0 )
	{
		return Sequence.Empty;
	}

	for( let item of this.items )
	{
		switch( item.positioning )
		{
			case 'zone' :
			{
				const iZone = item.ancillary.zone;
				const aZone = iZone.add( moveBy );
				changes.push(
					ChangeTreeAssign.create(
						'trace', item.ancillary.trace.add( 'zone' ),
						'prev', iZone,
						'val', aZone,
					)
				);
				continue;
			}

			case 'stroke' :
			{
				const joints = item.ancillary.joints;
				let j0p = joints.get( 0 ).pos;
				let j1p = joints.get( 1 ).pos;
				if( j0p.ti2ctype === Point )
				{
					changes.push(
						ChangeTreeAssign.create(
							'trace', item.ancillary.trace.add( 'joints', 0 ).add( 'pos' ),
							'prev', j0p,
							'val', j0p.add( moveBy )
						)
					);
				}

				if( j1p.ti2ctype === Point )
				{
					changes.push(
						ChangeTreeAssign.create(
							'trace', item.ancillary.trace.add( 'joints', 1 ).add( 'pos' ),
							'prev', j1p,
							'val', j1p.add( moveBy )
						)
					);
				}
				continue;
			}
		}
	}

	return Sequence.Array( changes );
};

/*
| 'Normal' button ought to be down during this action.
*/
def.proto.normalButtonDown = true;
