/*
| The generic action base.
*/
def.attributes =
{
	// traceV of the action receiver
	traceV: { type: 'ti2c:Trace' },
};

import { Self as Sequence } from '{ot:Change/Sequence}';

/*
| Hook for animation finished handlers.
*/
def.proto.finishAnimation = false;

/*
| If true shows the normal button down when
| the action is active.
*/
def.proto.normalButtonDown = false;

/*
| The changes an ongoing action applies on the fabric tree.
| Default, no changes.
*/
def.lazy.changesTransient =
	( ) =>
	Sequence.Empty;
