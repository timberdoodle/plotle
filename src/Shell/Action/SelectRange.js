/*
| The user is selecting a range with their pointer.
*/
def.extend = 'Shell/Action/Base';

import { Self as Rect  } from '{gleam:Rect}';

/*
| Zone of the action.
*/
def.lazy.zone =
	function( )
{
	return this.pointStart && Rect.Arbitrary( this.pointStart, this.pointTo );
};
