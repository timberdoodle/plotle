/*
| A user is creating a new stroke.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// the center of the bench as visual coords (on screen)
	pCenterV: { type: 'gleam:Point' },

	// label fabric, if to be created
	label: { type: 'Shared/Fabric/Item/Label' },

	// note fabric, if to be created
	note: { type: 'Shared/Fabric/Item/Note' },

	// path fabric, if to be created
	path: { type: 'Shared/Fabric/Item/DocPath/Self' },

	// stroke fabric, if to be created
	stroke: { type: 'Shared/Fabric/Item/Stroke/Self' },

	// subspace fabric, if to be created
	subspace: { type: 'Shared/Fabric/Item/Subspace' },
};
