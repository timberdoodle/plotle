/*
| The user is scrolling a note.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// mouse down point on start of scrolling
	pointStart: { type: 'gleam:Point' },

	// position of the scrollbar on start of scrolling
	startPos: { type: 'number' },
};

/*
| 'Normal' button ought to be down during this action.
*/
def.proto.normalButtonDown = true;
