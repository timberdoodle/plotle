/*
| The user is selecting stuff via a rectangle.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// point at start of operation
	pointStart: { type: [ 'undefined', 'gleam:Point' ] },

	// point the rectangle goes to
	pointTo: { type: [ 'undefined', 'gleam:Point' ] },
};

import { Self as Rect  } from '{gleam:Rect}';

/*
| Returns true if an entity with path is affected by this action.
|
| TODO needed? (dual code to visual space)
*/
def.proto.affectsItem =
	function( vItem )
{
	const zone = this.zone;
	if( !zone )
	{
		return false;
	}
	const tPos = zone.pos;
	const iZone = vItem.ancillary.zone;
	const iPos = iZone.pos;

	return(
		iPos.x >= tPos.x
		&& iPos.y >= tPos.y
		&& iPos.x + iZone.width <= tPos.x + zone.width
		&& iPos.y + iZone.height <= tPos.y + zone.height
	);
};

/*
| Zone of the action.
*/
def.lazy.zone =
	function( )
{
	return this.pointStart && Rect.Arbitrary( this.pointStart, this.pointTo );
};
