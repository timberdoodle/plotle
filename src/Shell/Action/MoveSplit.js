/*
| The user is moving the split ratio.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// mouseDown point on drag creation
	pointStart: { type: 'gleam:Point' },
};
