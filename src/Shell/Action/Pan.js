/*
| The user is panning the background.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// mouse down point on start of scrolling
	pointStart: { type: 'gleam:Point' },

	// offset
	offset: { type: 'gleam:Point' },
};
