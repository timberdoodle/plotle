/*
| The user is resizing an item.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// the (visual) items to resize
	items: { type: [ 'undefined', 'Shell/View/Space/Item/Set' ] },

	// mouseDown point on drag creation
	pointStart: { type: 'gleam:Point' },

	// base the resize to this point
	pBase: { type: [ 'undefined', 'gleam:Point' ] },

	// if true resize proportionally
	// scaleX must be === scaleY
	proportional: { type: [ 'undefined', 'boolean' ] },

	// resize to this direction
	resizeDir: { type: '<gleam:Angle/Types' },

	// scale x by this factor
	scaleX: { type: [ 'undefined', 'number' ] },

	// scale y by this factor
	scaleY: { type: [ 'undefined', 'number' ] },

	// the zones as the resize started
	startZone: { type: 'gleam:Rect' },

	// the zones as the resize started
	startZones: { type: 'group@gleam:Rect' },
};


import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as Point            } from '{gleam:Point}';

/*
| 'Normal' button ought to be down during this action.
*/
def.proto.normalButtonDown = true;

/*
| The changes this action applies on the fabric tree.
*/
def.lazy.changesTransient =
	function( )
{
	const pBase = this.pBase;
	if( !pBase ) return Sequence.Empty;

	const changes = [ ];
	for( let item of this.items )
	{
		switch( item.actionAffects )
		{
			case 'zone':
				this._pushChangesAffectedZone( item, changes );
				continue;

			case 'posfs':
				this._pushChangesAffectedPosFs( item, changes );
				continue;

			case 'joints':
				this._pushChangesAffectedJoints( item, changes );
				continue;

			default: throw new Error( );
		}
	}

	return Sequence.Array( changes );
};

/*
| Pushes the changes for an item with affected zone.
|
| ~item:    affected item
| ~changes: changes array to push upon
*/
def.proto._pushChangesAffectedZone =
	function( item, changes )
{
	const iZone = item.ancillary.zone;
	const aZone = iZone.baseScaleAction( this, 0, 0 ).ensureMinSize( item.sizeMin );

	if( iZone === aZone ) return;

	changes.push(
		ChangeTreeAssign.create(
			'trace', item.ancillary.trace.add( 'zone' ),
			'val', aZone,
			'prev', iZone
		)
	);
};

/*
| Pushes the changes for an item with affected zone.
|
| ~item: affected item
| ~changes: changes array to push upon
*/
def.proto._pushChangesAffectedPosFs =
	function( item, changes )
{
	const posI = item.ancillary.zone.pos;
	const posA = posI.baseScaleAction( this, 0, 0 );

	if( posI !== posA )
	{
		changes.push(
			ChangeTreeAssign.create(
				'trace', item.ancillary.trace.add( 'zone' ).add( 'pos' ),
				'val', posA,
				'prev', posI,
			)
		);
	}
	const iFs = item.ancillary.fontSize;

/**/if( CHECK && this.scaleX !== this.scaleY ) throw new Error( );

	const aFs = iFs * this.scaleY;
	if( iFs !== aFs )
	{
		changes.push(
			ChangeTreeAssign.create(
				'trace', item.ancillary.trace.add( 'fontSize' ),
				'val', aFs,
				'prev', iFs
			)
		);
	}
};

/*
| Pushes the changes for an item with affected joints.
|
| ~item:    affected item
| ~changes: changes array to push upon
*/
def.proto._pushChangesAffectedJoints =
	function( item, changes )
{
	const joints = item.ancillary.joints;

	for( let a = 0, alen = joints.length; a < alen; a++ )
	{
		const pos = joints.get( a ).pos;
		if( pos.ti2ctype !== Point ) continue;

		const posA = pos.baseScaleAction( this, 0, 0 );
		if( pos === posA ) continue;

		changes.push(
			ChangeTreeAssign.create(
				'trace', item.ancillary.trace.add( 'joints', a ).add( 'pos' ),
				'val', posA,
				'prev', pos
			)
		);
	}
};
