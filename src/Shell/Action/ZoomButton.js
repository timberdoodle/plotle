/*
| The user is holding the zoomIn or zoomOut button.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// # of the window this is on.
	atWindow: { type: 'number' },

	// direction, +/- 1
	dir: { type: 'number' },

	// makes further zoom steps
	refire: { type: 'boolean' }
};

import { Self as Shell } from '{Shell/Self}';
import { Self as Trace } from '{ti2c:Trace}';

/*
| Shortcut to create an zoom button action.
|
| ~dir: direction
*/
def.static.createZoom =
	function( atWindow, traceV, dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( traceV.ti2ctype !== Trace ) throw new Error( );
/**/	if( dir !== 1 && dir !== -1 ) throw new Error( );
/**/}

	return(
		Self.create(
			'atWindow', atWindow,
			'dir',      dir,
			'refire',   true,
			'traceV',   traceV,
		)
	);
};

/*
| A zoom animation finished.
*/
def.proto.finishAnimation =
	function( )
{
	if( this.refire )
	{
		Shell.changeSpaceTransformCenter( this.atWindow, this.dir );
		return true;
	}
	else
	{
		return false;
	}
};

/*
| 'Normal' button ought to be down during this action.
*/
def.proto.normalButtonDown = true;

/*
| Starts a drag.
|
| ~p:      cursor point
| ~screen: the screen for this operation
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, screen, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	throw new Error( );
};
