/*
| A facet of an element.
|
| For example hover, focus or down.
*/
def.attributes =
{
	border:
	{
		type:
		[
			'undefined',
			'Shell/Facet/Border',
			'list@Shell/Facet/Border'
		]
	},

	fill:
	{
		type:
		[
			'undefined',
			'gleam:Color',
			'gleam:Gradient/Askew',
			'gleam:Gradient/Radial'
		]
	},

	specs: { type: 'group@boolean', defaultValue: 'import("group@boolean").Empty' },
};

import { Self as Color       } from '{gleam:Color}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';

/*
| Shortcuts.
*/
def.static.Border =
	( border ) =>
	Self.create( 'border', border );

def.static.BorderFill =
	( border, fill ) =>
	Self.create( 'border', border, 'fill', fill );

def.static.Color =
	( color ) =>
	Self.create( 'color', color );

def.static.Fill =
	( fill ) =>
	Self.create( 'fill', fill );

/*
| Converts the facet for a figure into a glint.
*/
def.proto.glint =
	function( figure, nogrid, name )
{
	const border = this.border;
	const fill = this.fill;

	if( !border || border.ti2ctype === FacetBorder )
	{
		// it returns a single glint figure
		if( border && border.distance !== 0 )
		{
			figure = figure.envelope( border.distance );
		}

		return(
			GlintFigure.create(
				'color',  border?.color,
				'fill',   fill,
				'figure', figure,
				'name',   name,
				'nogrid', nogrid,
				'width',  border && border.width,
			)
		);
	}
	else
	{
		const list = [ ];
		let didB0;
		if( fill )
		{
			let b0;
			// finds the border with zero distance
			for( b0 of border )
			{
				if( b0.distance === 0 )
				{
					break;
				}
			}

			if( b0.distance === 0 )
			{
				list.push(
					GlintFigure.create(
						'figure', figure,
						'fill', fill,
						'nogrid', nogrid,
					)
				);
			}
			else
			{
				didB0 = true;

				list.push(
					GlintFigure.create(
						'color', b0.color,
						'fill', fill,
						'figure', figure,
						'nogrid', nogrid,
						'width', b0.width
					)
				);
			}
		}

		for( let b of border )
		{
			if( didB0 && b.distance === 0 )
			{
				continue;
			}

			list.push(
				GlintFigure.create(
					'color', b.color,
					'fill', fill,
					'figure', figure.envelope( b.distance ),
					'nogrid', nogrid,
					'width', b.width
				)
			);
		}

		return ListGlint.Array( list );
	}
};

/*
| Converts the facet for a figure into a border only glint.
*/
def.proto.glintBorder =
	function( figure, nogrid )
{
	const border = this.border;

	if( border.ti2ctype === FacetBorder || border.length === 1 )
	{
		// it returns a single GlintFigure
		if( border && border.distance !== 0 ) figure = figure.envelope( border.distance );
		return(
			GlintFigure.create(
				'color', border && border.color,
				'figure', figure,
				'nogrid', nogrid,
				'width', border && border.width
			)
		);
	}
	else
	{
		const list = [ ];
		for( let b of border )
		{
			list.push(
				GlintFigure.create(
					'color', b.color,
					'figure', figure.envelope( b.distance ),
					'nogrid', nogrid,
					'width', b.width
				)
			);
		}
		return ListGlint.Array( list );
	}
};

/*
| Converts the facet for a figure into a fill only glint.
*/
def.proto.glintFill =
	function( figure, nogrid )
{
	return(
		GlintFigure.create(
			'figure', figure,
			'fill', this.fill,
			'nogrid', nogrid,
		)
	);
};

/*
| A simple black fill.
*/
def.staticLazy.blackFill =
	( ) =>
	Self.create( 'fill', Color.black );

/*
| A simple black stroke.
*/
def.staticLazy.blackStroke =
	( ) =>
	Self.create( 'border', FacetBorder.Color( Color.black ) );
