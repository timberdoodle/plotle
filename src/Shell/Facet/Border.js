/*
| A border.
*/
def.attributes =
{
	// color of the border
	color: { type: 'gleam:Color', defaultValue: 'import("gleam:Color").black' },

	// distance from shape
	distance: { type: 'number', defaultValue: '0' },

	// border width
	width: { type: 'number', defaultValue: '1' },
};

import { Self as Color } from '{gleam:Color}';

/*
| Shorcuts.
*/
def.static.Color = ( color ) =>
	Self.create( 'color', color );

def.static.ColorWidth = ( color, width ) =>
	Self.create( 'color', color, 'width', width );

def.static.ColorDistance = ( color, distance ) =>
		Self.create( 'color', color, 'distance', distance );

def.static.ColorDistanceWidth = ( color, distance, width ) =>
		Self.create( 'color', color, 'distance', distance, 'width', width );

/*
| A simple black border.
*/
def.staticLazy.simpleBlack =
	( ) =>
	Self.create( 'color', Color.black );
