/*
| A list of facets.
|
| Display prioritiy is from last to first.
*/
def.extend = 'list@Shell/Facet/Self';

/*
| Returns the facet with highest index matching
| the specification in arguments given by
|
| 'name', value pairs
*/
def.proto.getFacet =
	function( ...args )
{
	const alen = args.length;
	for( let f of this.reverse( ) )
	{
		let matches = 0;
		for( let a = 0; a < alen; a += 2 )
		{
			if( f.specs.get( args[ a ] ) === args[ a + 1 ] )
			{
				matches++;
			}
		}

		if( matches === f.specs.size )
		{
			return f;
		}
	}

	return undefined;
};
