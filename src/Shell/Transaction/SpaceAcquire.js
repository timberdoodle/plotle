/*
| Acquiring a space.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/SpaceAcquire' },
};

import { Self as Plan                } from '{Shared/Trace/Plan}';
import { Self as RefSpace            } from '{Shared/Ref/Space}';
import { Self as ReplyError          } from '{Shared/Reply/Error}';
import { Self as ReplySpaceAcquire   } from '{Shared/Reply/SpaceAcquire}';
import { Self as RequestSpaceAcquire } from '{Shared/Request/SpaceAcquire}';
import { Self as UserCreds           } from '{Shared/User/Creds}';

/*
| Starts the transaction.
*/
def.static.start =
	function( userCreds, refSpace, createMissing )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( refSpace.ti2ctype !== RefSpace ) throw new Error( );
/**/	if( typeof( createMissing ) !== 'boolean' ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestSpaceAcquire.create(
					'userCreds', userCreds,
					'createMissing', createMissing,
					'refSpace', refSpace,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplySpaceAcquire.FromJson( json, Plan.space );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};

