/*
| Requesting a new user to be signed up / registered.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/SignUp' },
};

import { Self as ReplyError    } from '{Shared/Reply/Error}';
import { Self as ReplySignUp   } from '{Shared/Reply/SignUp}';
import { Self as RequestSignUp } from '{Shared/Request/SignUp}';
import { Self as UserCreds     } from '{Shared/User/Creds}';

/*
| Starts the transaction.
*/
def.static.start =
	function( userCreds, mail, newsletter )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( typeof( mail ) !== 'string' ) throw new Error( );
/**/	if( typeof( newsletter ) !== 'boolean' ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestSignUp.create(
					'userCreds',  userCreds,
					'mail',       mail,
					'newsletter', newsletter,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplySignUp.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};
