/*
| Requesting spaces to be altered.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/Alter' },
};

import { Self as ListUpdate   } from '{list@Shared/Dynamic/Update/Space}';
import { Self as ReplyAlter   } from '{Shared/Reply/Alter}';
import { Self as ReplyError   } from '{Shared/Reply/Error}';
import { Self as RequestAlter } from '{Shared/Request/Alter}';
import { Self as UserCreds    } from '{Shared/User/Creds}';

/*
| Starts a transaction.
*/
def.static.start =
	function( userCreds, updates )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( updates.ti2ctype !== ListUpdate ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestAlter.create(
					'userCreds', userCreds,
					'updates',   updates,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyAlter.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};
