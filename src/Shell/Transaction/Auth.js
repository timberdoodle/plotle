/*
| Request authentication to be checked,
| or to be assigned a visitor-id.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/Auth' },
};

import { Self as ReplyAuth   } from '{Shared/Reply/Auth}';
import { Self as ReplyError  } from '{Shared/Reply/Error}';
import { Self as RequestAuth } from '{Shared/Request/Auth}';
import { Self as UserCreds   } from '{Shared/User/Creds}';

/*
| Starts the transaction.
*/
def.static.start =
	function( userCreds )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestAuth.create(
					'userCreds', userCreds,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyAuth.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};

