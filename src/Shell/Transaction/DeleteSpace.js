/*
| Deleting a space.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/DeleteSpace' },
};

import { Self as ReplyError         } from '{Shared/Reply/Error}';
import { Self as ReplyDeleteSpace   } from '{Shared/Reply/DeleteSpace}';
import { Self as RequestDeleteSpace } from '{Shared/Request/DeleteSpace}';
import { Self as UserCreds          } from '{Shared/User/Creds}';

/*
| Starts the transaction.
*/
def.static.start =
	function( userCreds, uidSpace, confirmation )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( typeof( uidSpace ) !== 'string' ) throw new Error( );
/**/	if( typeof( confirmation ) !== 'string' ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestDeleteSpace.create(
					'userCreds',    userCreds,
					'confirmation', confirmation,
					'uid',          uidSpace,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyDeleteSpace.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};
