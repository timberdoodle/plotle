/*
| Starts up the shell.
*/
def.abstract = true;

import { Self as FontRoot } from '{gleam:Font/Root}';
import { Self as System   } from '{Shell/System}';

if( !NODE )
{
	window.onload =
		async function( )
	{
		ti2c_onload( );

		if( !globalThis.PUPPET )
		{
			globalThis.PUPPET = false;
		}

		await Promise.all( [
			FontRoot.load( 'DejaVuSans-Regular' ),
			FontRoot.load( 'NotoSans-Regular' ),
		] );

		System.startup( );
	};
}
