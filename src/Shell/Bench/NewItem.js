/*
| The new item bench.
*/
def.extend = 'Shell/Bench/Base';

def.attributes =
{
	// the new item action
	action: { type: 'Shell/Action/NewItem' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// current view size
	sizeView: { type: 'gleam:Size' },

	// the visual trace
	traceV: { type: 'ti2c:Trace' },

	// the window this view is for
	window: { type: 'Shell/Window/Space' },
};

import { Self as Angle          } from '{gleam:Angle}';
import { Self as ChangeTreeGrow } from '{ot:Change/Tree/Grow}';
import { Self as Color          } from '{gleam:Color}';
import { Self as DesignFont     } from '{Shell/Design/Font}';
import { Self as Ellipse        } from '{gleam:Ellipse}';
import { Self as Facet          } from '{Shell/Facet/Self}';
import { Self as FacetBorder    } from '{Shell/Facet/Border}';
import { Self as FacetList      } from '{Shell/Facet/List}';
import { Self as GlintFigure    } from '{gleam:Glint/Figure}';
import { Self as GlintPane      } from '{gleam:Glint/Pane}';
import { Self as GlintString    } from '{gleam:Glint/String}';
import { Self as GlintWindow    } from '{gleam:Glint/Window}';
import { Self as GroupBoolean   } from '{group@boolean}';
import { Self as Hover          } from '{Shell/Result/Hover}';
import { Self as ListGlint      } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret      } from '{Shared/Mark/Caret}';
import { Self as MarkItems      } from '{Shared/Mark/Items}';
import { Self as Point          } from '{gleam:Point}';
import { Self as Rect           } from '{gleam:Rect}';
import { Self as RingSector     } from '{gleam:RingSector}';
import { Self as Shell          } from '{Shell/Self}';
import { Self as Size           } from '{gleam:Size}';
import { Self as TraceRoot      } from '{Shared/Trace/Root}';
import { Self as Uid            } from '{Shared/Session/Uid}';

/*
| Checks if the frame has been clicked.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl  key was pressed
*/
def.proto.click =
	function( p, shift, ctrl )
{
	// not even near anything?
	if( !this._zoneWindowT.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( this._zoneWindowT.pos );

	const keysSector = Self._keysSector;
	for( let a = 0, alen = keysSector.length; a < alen; a++ )
	{
		const rs = this._ringSector( a );
		if( rs.within( pz ) )
		{
			const keySector = keysSector[ a ];
			const window = this.window;
			const uidSpace = window.uid;
			const keyNew = Uid.newUid( );
			const traceItem = TraceRoot.space( uidSpace ).add( 'items', keyNew );

			Shell.change(
				uidSpace,
				ChangeTreeGrow.create(
					'rank',  0,
					'trace', traceItem,
					'val',   this.action[ keySector ],
				),
			);

			let mark;

			switch( keySector )
			{
				case 'label':
				{
					mark =
						MarkCaret.TraceV(
							this.traceV
							.forward( 'space' )
							.add( 'items', keyNew )
							.add( 'text' )
							.add( 'paras', 0 )
							.add( 'string' )
							.add(
								'offset',
								this.action[ keySector ].text.paras.get( 0 ).string.length
							)
						);
					break;
				}

				case 'note':
				{
					mark =
						MarkCaret.TraceV(
							this.traceV
							.forward( 'space' )
							.add( 'items', keyNew )
							.add( 'text' )
							.add( 'paras', 0 )
							.add( 'string' )
							.add( 'offset', 0 )
						);
					break;
				}

				case 'path':
				{
					mark =
						MarkCaret.TraceV(
							this.traceV
							.forward( 'space' )
							.add( 'items', keyNew )
							.add( 'widgets', 'name' )
							.add( 'value' )
							.add( 'offset', this.action[ keySector ].name.length )
						);
					break;
				}

				case 'stroke':
				{
					mark =
						MarkItems.TraceVs(
							this.traceV.forward( 'space' ).add( 'items', keyNew )
						);
					break;
				}

				case 'subspace':
				{
					mark =
						MarkCaret.TraceV(
							this.traceV
							.forward( 'space' )
							.add( 'items', keyNew )
							.add( 'widgets', 'link' )
							.add( 'value' )
							.add( 'offset', this.action[ keySector ].link.length )
						);
					break;
				}

				default: throw new Error( );
			}

			Shell.alter(
				'action', undefined,
				'mark',   mark,
			);
		}
	}

	return undefined;
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	return undefined;
};

/*
| The frames glint.
*/
def.lazy.glint =
	function( )
{
	return(
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane', this._glintPane,
			'pos',  this._zoneWindowT.pos,
		)
	);
};

/*
| Pointing device hover.
|
| Returns true if the pointing device hovers over anything.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const zwt = this._zoneWindowT;

	// if not in the windows zone
	// or within the mask
	if( !zwt.within( p ) )
	{
		if( !this._zoneWindowExtraT.within( p ) )
		{
			Shell.alter( 'action', undefined );
		}
		else
		{
			return undefined;
		}
	}

	const pz = p.sub( zwt.pos );

	const keysSector = Self._keysSector;
	for( let a = 0, alen = keysSector.length; a < alen; a++ )
	{
		const rs = this._ringSector( a );
		if( rs.within( pz ) )
		{
			const key = keysSector[ a ];
			return Hover.CursorPointer.create( 'traceV', this.traceV.add( key ) );
		}
	}

	return undefined;
};

/*
| A button has been pushed.
|
| ~traceV: traceV of the button
| ~shift:  true if shift was held
| ~ctrl:   true if ctrl/meta was held
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	/*
	const buttonKey = traceV.backward( 'widgets' ).last.key;

	switch( buttonKey )
	{
		default: throw new Error( );
	}
	*/
};

/*
| Facets ring sector.
*/
def.staticLazyFunc._facetsRingSector =
	( light ) =>
	FacetList.Elements(
		// default state
		Facet.create(
			'fill',
				light
				? Color.RGBA( 255, 250, 230, 0.85 )
				: Color.RGBA( 240, 224, 160, 0.85 ),
			'border',
				light
				? FacetBorder.Color( Color.RGBA( 255, 208, 123, 0.9 ) )
				: FacetBorder.Color( Color.RGBA( 158, 121,  56, 0.9 ) ),
			'specs', GroupBoolean.Empty,
		),
		// hover
		Facet.create(
			'fill',
				light
				? Color.RGBA( 255, 188, 88, 0.85 )
				: Color.RGBA( 255, 188, 88, 0.85 ),
			'border',
				light
				? FacetBorder.Color( Color.RGBA( 255, 208, 123, 0.9 ) )
				: FacetBorder.Color( Color.RGBA( 158, 121,  56, 0.9 ) ),
			'specs', GroupBoolean.Table( { hover: true } ),
		),
	);

/*
| The frame glint holds all unmasked stuff.
*/
def.lazy._glintPane =
	function( )
{
	const light = this.light;
	const resolution = this.resolution;
	const sactz = this._shapeAreaCenterTZ;

	const list =
	[
		GlintFigure.FigureColorFill(
			this._shapeAreaOuterTZ,
			Color.RGBA( 255, 220, 157, 0.9 ),
			Color.RGBA( 255, 255, 180, 0.955 ),
		),
		GlintFigure.FigureColorFill(
			sactz,
			Color.RGBA( 255, 220, 157, 0.9 ),
			Color.RGB( 255, 188, 88 ),
		),
		GlintString.create(
			'align',     'center',
			'base',      'middle',
			'fontColor',  DesignFont.Size( 18 ),
			'p',          sactz.pc,
			'resolution', resolution,
			'string',     'New',
		),
	];

	const keys = Self._keysSector;
	const sectors = Self._sectors;
	const hover = this.hover;

	for( let a = 0, alen = keys.length; a < alen; a++ )
	{
		const rs = this._ringSector( a );
		const sector = sectors[ a ];
		const key = sector.key;
		const isHover = !!( hover && hover.last.name === key );

		list.push(
			Self._facetsRingSector( light )
			.getFacet( 'hover', isHover )
			.glint( rs, false, this.traceV.add( key ).asString )
		);

		const str = sector.string;

		if( str.indexOf( '\n' ) < 0 )
		{
			const pcs = rs.pCenterSector;
			list.push(
				GlintString.create(
					'align',     'center',
					'base',      'middle',
					'fontColor',  DesignFont.Size( 16 ),
					'p',          pcs,
					'resolution', resolution,
					'string',     str,
				),
			);
		}
		else
		{
			const split = str.split( '\n' );
			const newline = 20 * resolution.ratio;
			const pcs = rs.pCenterSector.add( 0, -( split.length - 1 ) / 2 * newline );

			for( let a = 0, alen = split.length; a < alen; a++ )
			{
				list.push(
					GlintString.create(
						'align',     'center',
						'base',      'middle',
						'fontColor',  DesignFont.Size( 16 ),
						'p',          pcs.add( 0, newline * a ),
						'resolution', resolution,
						'string',     split[ a ],
					),
				);
			}
		}
	}

	return(
		GlintPane.create(
			'glint',      ListGlint.Array( list ),
			'resolution', resolution,
			'size',       this._zoneWindowT.size.add( 2 ),
		)
	);
};

/*
| The 'nr'-th ring sector.
*/
def.lazyFunc._ringSector =
	function( nr )
{
	const rads = 2 * Math.PI / Self._sectors.length;
	const saotz = this._shapeAreaOuterTZ;
	const sactz = this._shapeAreaCenterTZ;
	const ri = sactz.width / 2;
	const ro = saotz.width / 2;

	const r90 = Math.PI / 2;
	const angle0 = Angle.Radians( r90 + rads * nr );
	const angle1 = Angle.Radians( r90 + rads * ( nr + 1 ) );

	return(
		RingSector.create(
			'angle0',  angle0,
			'angle1',  angle1,
			'pCircle', sactz.pc,
			'ri',      ri,
			'ro',      ro,
		)
	);
};

/*
| Definition of the sectors.
*/
def.staticLazy._sectors =
	function( )
{
	return Object.freeze( [
		{ key: 'note',     string: 'Note'         },
		{ key: 'stroke',   string: 'Stroke',      },
		{ key: 'subspace', string: 'Sub-\nspace', },
		{ key: 'path',     string: 'Path',        },
		{ key: 'label',    string: 'Label',       },
	] );
};

/*
| An array of sector keys.
*/
def.staticLazy._keysSector =
	function( )
{
	const sectors = Self._sectors;
	const list = [ ];
	for( let a = 0, alen = sectors.length; a < alen; a++ )
	{
		list.push( sectors[ a ].key );
	}
	return Object.freeze( list );
};

/*
| Returns a table to get the sector number by key.
*/
def.staticLazy._sectorNrs =
	function( )
{
	const sectors = Self._sectors;
	const table = { };
	for( let a = 0, alen = sectors.length; a < alen; a++ )
	{
		table[ sectors[ a ].key ] = a;
	}
	return Object.freeze( table );
};

/*
| Shape of the center area; transformed and zero positioned.
*/
def.lazy._shapeAreaCenterTZ =
	function( )
{
	const zwt = this._zoneWindowT;
	const ratio = this.resolution.ratio;
	const sc = Self._sizeCenter.scale( ratio );

	return Ellipse.CenterSize( zwt.zeroPos.pc, sc );
};

/*
| Shape of the bench area; transformed and zero positioned.
*/
def.lazy._shapeAreaOuterTZ =
	function( )
{
	const zwt = this._zoneWindowT;

	return(
		Ellipse.create(
			'pos',     Point.zero,
			'height',  zwt.height,
			'width',   zwt.width,
		)
	);
};

/*
| Size of the center circle.
*/
def.staticLazy._sizeCenter =
	( ) =>
	Size.WH( 76, 76 );


/*
| Size of the outer circle.
*/
def.staticLazy._sizeOuter =
	( ) =>
	Size.WH( 190, 190 );

/*
| The transformed zone of the window.
*/
def.lazy._zoneWindowT =
	function( )
{
	const sizeOuter = Self._sizeOuter.scale( this.resolution.ratio );

	return(
		Rect.PosSize(
			this.action.pCenterV
			.sub( sizeOuter.width / 2, sizeOuter.height / 2 ),
			sizeOuter
		)
	);
};

/*
| The transformed zone of the window.
*/
def.lazy._zoneWindowExtraT =
	function( )
{
	const sizeOuter = Self._sizeOuter.scale( this.resolution.ratio );

	return(
		Rect.PosWidthHeight(
			this.action.pCenterV
			.sub( sizeOuter.width * 1.25, sizeOuter.height * 1.25 ),
			2.5 * sizeOuter.width,
			2.5 * sizeOuter.height,
		)
	);
};


