/*
| Bench base.
*/
def.abstract = true;

def.attributes =
{
	// true if light color scheme
	light: { type: 'boolean' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// the visual trace of the view the bench is in
	traceV: { type: 'ti2c:Trace' },

	// the window this view is for
	window: { type: 'Shell/Window/Space' },
};

import { Self as Color        } from '{gleam:Color}';
import { Self as Facet        } from '{Shell/Facet/Self}';
import { Self as FacetList    } from '{Shell/Facet/List}';
import { Self as GroupBoolean } from '{group@boolean}';

/*
| The button facets.
*/
def.staticLazyFunc.facetsButton =
	( light ) =>
	FacetList.Elements(
		// default state.
		Facet.create(
			'specs', GroupBoolean.Empty,
		),
		// hover
		Facet.create(
			'fill', Color.RGB( 255, 221, 170 ),
			'specs', GroupBoolean.Table( { hover: true } ),
		),
		// down
		Facet.create(
			'fill', Color.RGB( 255, 188, 88 ),
			'specs', GroupBoolean.Table( { down: true } ),
		),
		// down and hover
		Facet.create(
			'fill', Color.RGB( 255, 188, 88 ),
			'specs', GroupBoolean.Table( { down: true, hover: true } ),
		)
	);
