/*
| Result of a pointingHover call.
|
| Containts the path of the component being hovered over
| As well the shape the cursor should get.
*/
def.attributes =
{
	// the cursor to display
	cursor: { type: 'string' },

	// the visualTrace to the entity being hovered upon
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },
};

/*
| Shortcuts.
*/
def.staticLazy.CursorCrosshair = ( ) => Self.create( 'cursor', 'crosshair' );
def.staticLazy.CursorDefault   = ( ) => Self.create( 'cursor', 'default'   );
def.staticLazy.CursorGrabbing  = ( ) => Self.create( 'cursor', 'grabbing'  );
def.staticLazy.CursorGrab      = ( ) => Self.create( 'cursor', 'grab'      );
def.staticLazy.CursorMove      = ( ) => Self.create( 'cursor', 'move'      );
def.staticLazy.CursorEwResize  = ( ) => Self.create( 'cursor', 'ew-resize' );
def.staticLazy.CursorNeResize  = ( ) => Self.create( 'cursor', 'ne-resize' );
def.staticLazy.CursorNsResize  = ( ) => Self.create( 'cursor', 'ns-resize' );
def.staticLazy.CursorNwResize  = ( ) => Self.create( 'cursor', 'nw-resize' );
def.staticLazy.CursorPointer   = ( ) => Self.create( 'cursor', 'pointer'   );
def.staticLazy.CursorSeResize  = ( ) => Self.create( 'cursor', 'se-resize' );
def.staticLazy.CursorSwResize  = ( ) => Self.create( 'cursor', 'sw-resize' );
def.staticLazy.CursorText      = ( ) => Self.create( 'cursor', 'text'      );
