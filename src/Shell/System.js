/*
| This is a wrapper around HTML5 browsers,
| creating a more comfortable interface for
| the shell.
*/
def.abstract = true;

import { Self as Color           } from '{gleam:Color}';
import { Self as Display         } from '{gleam:Display/Canvas}';
import { Self as Point           } from '{gleam:Point}';
import { Self as Resolution      } from '{gleam:Display/Canvas/Resolution}';
import { Self as Shell           } from '{Shell/Self}';
import { Self as Size            } from '{gleam:Size}';
import { Self as SysMode         } from '{Shell/Result/SysMode}';

/*
| Catches all thrown errors.
| Also corrects hover and attention steering.
|
| ~func:     event function to wrap
| ~steering: if true do stear hovering/attention
*/
def.static.transmitter =
	function( func, steering )
{
	return function( ...args )
	{
		if( _failScreen ) return;

		if( !FAILSCREEN )
		{
			func.apply( this, args );
			if( func !== _draw )
			{
				if( steering )
				{
					_repeatHover( );
					_steerAttention( );
				}

				window.requestAnimationFrame( _transmitterDraw );
			}
		}
		else
		{
			try
			{
				func.apply( this, args );

				if( func !== _draw )
				{
					if( steering )
					{
						_repeatHover( );
						_steerAttention( );
					}

					window.requestAnimationFrame( _transmitterDraw );
				}
			}
			catch( e )
			{
				try
				{
					const message =
						'OOPS! Internal failure, ' + e.name + ': ' + e.message + '\n\n'
						+ 'stack: ' + e.stack + '\n\n'
						+ 'Please report to contact@plotle.com';

					Self.failScreen( message );
				}
				catch( ee )
				{
					console.log( 'error in error:' + ee );
				}
			}
		}
	};
};

/*==============================
| Error fetching function calls.
*==============================*/

/*
| Used to transmit animation events
*/
let _transmitterAnimation;

/*
| Used to transmit draw on request animation.
*/
let _transmitterDraw;

/*
| Used for starting drags after timeout.
*/
let _transmitterAtweenTime;

/*
| If true there is currently one or more animations
| to be processed.
*/
let _animating = false;

/*==============================
| Atween is the state where the mouse button went down,
| and its yet unsure if this is a click or drag.
| if the mouse moves out of the atweenBox or the atweenTimer ticks its
| a drag, if it goes up before either happens, its a click
*==============================*/

/*
| Status of ctrl when atween state starts.
*/
let _atweenCtrl = false;

/*
| Move position where cursor moved to in atween state.
*/
let _atweenMove;

/*
| Position where mouse went down.
*/
let _atweenPos;

/*
| Status of shift when atween state starts.
*/
let _atweenShift = false;

/*
| Timer of the atween state.
*/
let _atweenTimer;

/*
| The canvas everything is drawn upon.
*/
let _canvas;

/*
| Hidden span element to hold focus when not suggesting a virtual keyboard.
*/
let _span;

/*
| When true the system dropped down to show a fail screen.
*/
let _failScreen = false;

/*
| The hidden input taking text input.
*/
let _hiddenInput;
let _hFocus;

/*
| Hover is getting repeated by events that change
| the root, so it can react properly.
*/
let _hoverShift = false;
let _hoverCtrl = false;
let _hoverP;

/*
| false, 'atween' or 'drag'
*/
let _pointingState = false;

/*
| Display resolution.
*/
let _resolution;

/*
| Just had a word been inputed (by mobile keyboard).
*/
let _inputWord = false;

/*
| Color scheme preferences.
*/
let _prefersDark;

const _keyCodeNames =
	Object.freeze( new Map( [
		[  8, 'backspace' ],
		[  9, 'tab'       ],
		[ 13, 'enter'     ],
		[ 16, 'shift'     ],
		[ 17, 'ctrl'      ],
		[ 27, 'esc'       ],
		[ 33, 'pageup'    ],
		[ 34, 'pagedown'  ],
		[ 35, 'end'       ],
		[ 36, 'pos1'      ],
		[ 37, 'left'      ],
		[ 38, 'up'        ],
		[ 39, 'right'     ],
		[ 40, 'down'      ],
		[ 46, 'del'       ]
	] ) );

const _keyCodeNamesCtrl =
	Object.freeze( new Map( [
		[  16, 'shift' ],
		[  17, 'ctrl'  ],
		[  65, 'a'     ],
		[  89, 'y'     ],
		[  90, 'z'     ],
		[ 188, ','     ],
		[ 190, '.'     ]
	] ) );

/*
| Cancels an interval timer.
*/
def.static.cancelInterval =
	function( id )
{
	window.clearInterval( id );
};

/*
| Cancels a single timer.
*/
def.static.cancelTimer =
	function( id )
{
	window.clearTimeout( id );
};

/*
| If not already animating, starts doing so.
*/
def.static.doAnimation =
	function( )
{
	if( _animating ) return;
	_animating = true;
	window.requestAnimationFrame( _transmitterAnimation );
};

/*
| Replaces the shell by a failscreen
*/
def.static.failScreen =
	function( message )
{
	if( _failScreen ) return;

	_failScreen = true;
	const body = document.body;
	body.removeChild( _canvas );
	body.removeChild( _hiddenInput );
	const divWrap = document.createElement( 'div' );
	const divContent = document.createElement( 'div' );
	const divMessage = document.createElement( 'div' );
	const butReload = document.createElement( 'button' );
	body.appendChild( divWrap );
	const bodyStyle = body.style;
	bodyStyle.backgroundColor = 'rgb(32,32,32)';
	bodyStyle.color = 'rgb(155,155,155)';
	bodyStyle[ 'font-family' ] = 'sans-serif';

	document.getElementById( 'viewport' ).content =
		'width=device-width, initial-scale=1, maximum-scale=1';

	divWrap.appendChild( divContent );
	divContent.appendChild( divMessage );
	divContent.appendChild( butReload );

	divWrap.style.display = 'table';
	divWrap.style.height = '100%';
	divWrap.style.marginLeft = 'auto';
	divWrap.style.marginRight = 'auto';

	divContent.style.display = 'table-cell';
	divContent.style.verticalAlign = 'middle';

	divMessage.textContent = message;
	divMessage.style.whiteSpace = 'pre-wrap';

	butReload.textContent = 'Reload';
	butReload.style.width = '100%';
	butReload.style.marginTop = '20px';
	butReload.onclick =
		( ) =>
		location.reload( );
};

/*
| Sets an interval timer.
|
| Handles error catching.
|
| Return the timer id.
*/
def.static.setInterval =
	function( time, callback )
{
	return window.setInterval( Self.transmitter( callback, true ), time );
};

/*
| Sets a timer.
|
| Handles error catching.
|
| Return the timer id.
*/
def.static.setTimer =
	function( time, callback )
{
	return window.setTimeout( Self.transmitter( callback, true ), time );
};

/*
| System starts up ( pages loades )
*/
def.static.startup =
	function( )
{
	window.root = undefined;
	const startup = Self.transmitter( Self._startup );
	startup( );
};

/*
| Stops animating.
*/
def.static.stopAnimation =
	function( )
{
	_animating = false;
};

/*
| Does an animation frame.
*/
function _animation( time )
{
	if( !_animating ) return;
	Shell.animationFrame( time );
	window.requestAnimationFrame( _transmitterAnimation );
}

/*
| Redraws in a animation frame.
*/
function _draw( )
{
	Shell.draw( );
}

/*
| Starts up the system.
*/
def.static._startup =
	function( )
{
	_canvas = document.getElementById( 'canvas' );
	_span = document.getElementById( 'span' );
	_prefersDark =
		!!( window.matchMedia && window.matchMedia( '(prefers-color-scheme: dark)' ).matches );

	_resolution = Resolution.CurrentDevice( Self._superSampling( ) );
	const screenSize = Size.InnerWindow( );
	const background = _prefersDark ? Color.black : Color.white;
	const display =
		Display.AroundHTMLCanvas(
			_canvas,
			screenSize,
			_resolution,
			pass,
			background
		);

	const transmitter = Self.transmitter;
	_transmitterAnimation = transmitter( _animation );
	_transmitterDraw = transmitter( _draw );
	_transmitterAtweenTime = transmitter( _atweenTime );

/**/if( PUPPET )
/**/{
/**/	globalThis.redraw = _transmitterDraw;
/**/}

	// hidden input that forwards all events
	_hiddenInput = document.getElementById( 'input' );
	//_hiddenInput.autocomplete = 'off';
	_hFocus = false;

	// iPad sometimes starts just somewhere
	window.scrollTo( 0, 0 );

	const evo = Object.freeze( { capture: true, passive: false, } );

	window.matchMedia( '(prefers-color-scheme: dark)' )
	.addEventListener( 'change', transmitter( _onColorScheme ), evo );

	_hiddenInput.addEventListener( 'blur',       transmitter( _onInputBlur        ), evo );
	window.addEventListener( 'resize',           transmitter( _onResize           ), evo );
	window.addEventListener( 'focus',            transmitter( _onSystemFocus      ), evo );
	window.addEventListener( 'blur',             transmitter( _onSystemBlur       ), evo );
	window.addEventListener( 'popstate',         transmitter( _onPopstate         ), evo );
	window.addEventListener( 'keyup',            transmitter( _onKeyUp            ), evo );
	window.addEventListener( 'keydown',          transmitter( _onKeyDown          ), evo );
	window.addEventListener( 'beforeinput',      transmitter( _onBeforeInput      ), evo );
	window.addEventListener( 'input',            transmitter( _onInput            ), evo );
	window.addEventListener( 'compositionstart', transmitter( _onCompositionStart ), evo );
	window.addEventListener( 'compositionend',   transmitter( _onCompositionEnd   ), evo );
	window.addEventListener( 'copy',             transmitter( _onCopy             ), evo );
	window.addEventListener( 'cut',              transmitter( _onCut              ), evo );
	window.addEventListener( 'paste',            transmitter( _onPaste            ), evo );

	// in case of not using eruda debugging simly grab all the window events
	// for eruda be more restrictve so it keeps working
	if( !ERUDA )
	{
		window.addEventListener( 'mousedown',   transmitter( _onMouseDown   ), evo );
		window.addEventListener( 'mousemove',   transmitter( _onMouseMove   ), evo );
		window.addEventListener( 'mouseup',     transmitter( _onMouseUp     ), evo );
		window.addEventListener( 'touchstart',  transmitter( _onTouchStart  ), evo );
		window.addEventListener( 'touchmove',   transmitter( _onTouchMove   ), evo );
		window.addEventListener( 'touchend',    transmitter( _onTouchEnd    ), evo );
		window.addEventListener( 'wheel',       transmitter( _onWheel       ), evo );
		window.addEventListener( 'contextmenu', transmitter( _onContextMenu ), evo );
	}
	else
	{
		_canvas.addEventListener( 'mousedown',   transmitter( _onMouseDown   ), evo );
		_canvas.addEventListener( 'mousemove',   transmitter( _onMouseMove   ), evo );
		_canvas.addEventListener( 'mouseup',     transmitter( _onMouseUp     ), evo );
		_canvas.addEventListener( 'touchstart',  transmitter( _onTouchStart  ), evo );
		_canvas.addEventListener( 'touchmove',   transmitter( _onTouchMove   ), evo );
		_canvas.addEventListener( 'touchend',    transmitter( _onTouchEnd    ), evo );
		_canvas.addEventListener( 'wheel',       transmitter( _onWheel       ), evo );
		_canvas.addEventListener( 'contextmenu', transmitter( _onContextMenu ), evo );
	}

	Shell.startup( display, _prefersDark );
};


/*
| Timeout after mouse down so dragging starts.
*/
function _atweenTime( )
{
/**/if( CHECK && _pointingState !== 'atween' ) throw new Error( );

	_pointingState = 'drag';

	Shell.dragStart( _atweenPos, _atweenShift, _atweenCtrl );
	Shell.dragMove( _atweenMove, _atweenShift, _atweenCtrl );

	_resetAtweenState( );
	_repeatHover( );
	_steerAttention( );
}

/*
| Start of text composition event (on mobiles).
| .. ignored
*/
function _onCompositionStart( event )
{
	//console.log( 'CSTART', event );
}

/*
| End of composition event (on mobiles)
*/
function _onCompositionEnd( event )
{
	_hiddenInput.value = '';
	// console.log( 'CEND', event );
	const data = event.data;

	if( typeof( data ) === 'string' )
	{
		// this is hacky and likely false, but dunno better
		// on mobile virtual composition keyboard the input shoult NOT be called
		// and locale seems to be '', on desktop dead-keys like 'a will trigger
		// this even and input needs to called (and locale is undefined?)
		if( event.locale === undefined )
		{
			Shell.input( event.data );
		}
	}
}

/*
| Disables context menues.
*/
function _onContextMenu( event )
{
	event.preventDefault( );
	return false;
}

/*
| Prefered color scheme changed.
*/
function _onColorScheme( event )
{
	_prefersDark =
		!!( window.matchMedia && window.matchMedia( '(prefers-color-scheme: dark)' ).matches );
	Shell.alter( 'systemPrefersDark', _prefersDark );
}

/*
| On copy event.
*/
function _onCopy( event )
{
	event.clipboardData.setData( 'text/plain', Shell.getClipboard( ) );
	event.preventDefault( );
	return false;
}

/*
| On cut event.
*/
function _onCut( event )
{
	event.preventDefault( );
	event.clipboardData.setData( 'text/plain', Shell.getClipboard( ) );
	Shell.input( '' );
	return false;
}

/*
| Event fired before input is handled.
*/
function _onBeforeInput( event )
{
	// this is used by the composed on mobile
	// if the composed word changes something prior
	if( event.inputType === 'deleteContentBackward' )
	{
		const alen = event.target.selectionEnd - event.target.selectionStart;
		for( let a = 0; a < alen; a++ )
		{
			// TODO make something more elegant than repeatedly
			// calling backspace
			Shell.specialKey( 'backspace', false, false, false );
		}
	}
}

/*
| Input on hidden input field.
*/
function _onInput( event )
{
	event.preventDefault( );
	// separates multiple words with space.
	const data = event.data;

	if( event.isComposing ) return;

	if( event.inputType === 'deleteContentBackward' )
	{
		// handled in onBeforeInput
		return;
	}

	if( typeof( data ) === 'string' )
	{
		//const eSpace = inputWord && data.length > 1 ? ' ' : '';
		Shell.input( event.data );
		_inputWord = data.length > 1 ? data : false;
	}

	_repeatHover( );
	_steerAttention( );
	//_hiddenInput.value = '';
}

/*
| Input blur
*/
function _onInputBlur( )
{
	window.scrollTo( 0, 0 );
}

/*
| Key down on hidden input field.
*/
function _onKeyDown( )
{
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;
	const keyCode = event.keyCode;
	const key =
		ctrl
		? _keyCodeNamesCtrl.get( keyCode )
		: _keyCodeNames.get( keyCode  );
	if( key )
	{
		event.preventDefault( );
		Shell.specialKey( key, shift, ctrl, _inputWord );
		_inputWord = false;
		_repeatHover( );
		_steerAttention( );
	}
}

/*
| Hidden input key up.
*/
function _onKeyUp( event )
{
	event.preventDefault( );

	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;
	const keyCode = event.keyCode;

	const key =
		ctrl
		? _keyCodeNamesCtrl.get( keyCode )
		: _keyCodeNames.get( keyCode  );

	if( key ) Shell.releaseSpecialKey( key, shift, ctrl );

	_repeatHover( );
	_steerAttention( );
}

/*
| Mouse down event.
*/
function _onMouseDown( event )
{
	event.preventDefault( );
	if( event.button !== undefined && event.button !== 0 ) return;

	const p = Point.FromEvent( event, _canvas, _resolution );
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;
	_probeClickDrag( p, shift, ctrl );
	_pointingHover( p, shift, ctrl );
}

/*
| Mouse move event.
*/
function _onMouseMove( event )
{
	const p = Point.FromEvent( event, _canvas, _resolution );
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;

	switch( _pointingState )
	{
		case false:
			_pointingHover( p, shift, ctrl );
			break;

		case 'atween':
		{
			const dragbox = config.dragBox;

			if(
				( Math.abs( p.x - _atweenPos.x ) > dragbox )
				|| ( Math.abs( p.y - _atweenPos.y ) > dragbox )
			)
			{
				// moved out of dragbox -> start dragging
				clearTimeout( _atweenTimer );
				_pointingState = 'drag';

				Shell.dragStart( _atweenPos, shift, ctrl );
				Shell.dragMove( p, shift, ctrl );

				_pointingHover( p, shift, ctrl );
				_resetAtweenState( );
				//_captureEvents( );
				_steerAttention( );
			}
			else
			{
				// saves position for possible atween timeout
				_atweenMove = p;
			}

			break;
		}

		case 'drag':
		{
			Shell.dragMove( p, shift, ctrl );
			_pointingHover( p, shift, ctrl );
			break;
		}

		default: throw new Error( );
	}
}

/*
| Mouse up event.
*/
function _onMouseUp( event )
{
	event.preventDefault( );
	//_releaseEvents( );
	const p = Point.FromEvent( event, _canvas, _resolution );
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;

	switch( _pointingState )
	{
		case false:
			break;

		case 'atween':
			// A click is a mouse down followed within dragtime by 'mouseup' and
			// not having moved out of 'dragbox'.
			clearTimeout( _atweenTimer );
			Shell.click( _atweenPos, shift, ctrl );
			_pointingHover( _atweenPos, shift, ctrl );
			_steerAttention( );
			_resetAtweenState( );
			_pointingState = false;
			break;

		case 'drag':
		{
			Shell.dragStop( p, shift, ctrl );

			_pointingHover( p, shift, ctrl );
			_steerAttention( );

			_pointingState = false;
			break;
		}

		default:
			throw new Error( );
	}
}

/*
| The wheel is being turned.
*/
function _onWheel( event )
{
	event.preventDefault( );
	const p = Point.FromEvent( event, _canvas, _resolution );

	let dir;
	if( event.wheelDelta !== undefined )
	{
		dir = event.wheelDelta > 0 ? 1 : -1;
	}
	else if( event.detail !== undefined )
	{
		dir = event.detail > 0 ? -1 : 1;
	}
	else
	{
		console.log( 'invalid wheel event' );
		return;
	}

	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;

	Shell.mousewheel( p, dir, shift, ctrl );

	_pointingHover( p, shift, ctrl );
	_steerAttention( );
}

/*
| Paste event.
*/
function _onPaste( event )
{
	event.preventDefault( );

	const cd = event.clipboardData;
	const itp = cd.types.indexOf( 'text/plain' );
	if( itp === undefined ) return;

	const string = cd.getData( 'text/plain' );
	Shell.paste( 'text/plain', string );
}

/*
| Popstate event
*/
function _onPopstate( event )
{
	event.preventDefault( );

	Shell.popstate( event.target.location.pathname );
}

/*
| Window is being resized.
*/
function _onResize( event )
{
	_resolution = Resolution.CurrentDevice( Self._superSampling( ) );
	Shell.resize( Size.InnerWindow( ), _resolution );
}

/*
| Lost focus.
*/
function _onSystemBlur( )
{
	Shell.setSystemFocus( false );
}

/*
| Got focus.
*/
function _onSystemFocus( )
{
	Shell.setSystemFocus( true );
}

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
function _probeClickDrag( p, shift, ctrl )
{
	_pointingState = Shell.probeClickDrag( p, shift, ctrl );

	switch( _pointingState )
	{
		case 'atween':
			_atweenPos = p;
			_atweenMove = p;
			_atweenShift = shift;
			_atweenCtrl = ctrl;
			_atweenTimer = Self.setTimer( config.dragTime, _transmitterAtweenTime );
			return;

		case 'drag':
			Shell.dragStart( p, shift, ctrl );
			return;

		case false:
			return;

		default: throw new Error( );
	}
}

/*
| Handles hovering of the pointing device.
*/
function _pointingHover( p, shift, ctrl )
{
	_hoverP = p;
	_hoverShift = shift;
	_hoverCtrl = ctrl;

	_setCursor( Shell.pointingHover( p, shift, ctrl ) );
}

function _resetAtweenState( )
{
	_atweenTimer = undefined;
	_atweenShift = false;
	_atweenCtrl = false;
	_atweenPos = undefined;
	_atweenMove = undefined;
}

/*
| Sets the cursor
*/
function _setCursor( cursor )
{
	if( !cursor ) return;

	switch( cursor )
	{
		case 'grab':
			_canvas.style.cursor = '';
			_canvas.className = 'grab';
			break;
		case 'grabbing':
			_canvas.style.cursor = '';
			_canvas.className = 'grabbing';
			break;
		default:
			_canvas.style.cursor = cursor;
			_canvas.className = '';
			break;
	}
}

/*
| Repeats the last hover.
|
| Used by asyncEvents so the hoveringState is corrected.
*/
function _repeatHover( )
{
	if( !_hoverP ) return;

	_setCursor( Shell.pointingHover( _hoverP, _hoverShift, _hoverCtrl ) );
}

/*
| The user is touching something ( on mobile devices )
*/
function _onTouchStart( event )
{
	_hiddenInput.blur( );
	if( _hFocus ) _hiddenInput.focus( );

	event.preventDefault( );
	// for now ignore multi-touches
	if( event.touches.length !== 1 ) return false;

	_inputWord = false;
	_hiddenInput.value = '';

	const ratio = _resolution.devicePixelRatio;
	const p =
		Point.XY(
			( event.touches[ 0 ].pageX - _canvas.offsetLeft ) * ratio,
			( event.touches[ 0 ].pageY - _canvas.offsetTop ) * ratio
		);
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;
	_probeClickDrag( p, shift, ctrl );
	return false;
}

/*
| The use is moving the touch ( on mobile devices )
*/
function _onTouchMove( event )
{
	event.preventDefault( );

	// for now ignore multi-touches
	if( event.touches.length !== 1 ) return false;

	const ratio = _resolution.devicePixelRatio;
	const p =
		Point.XY(
			( event.touches[ 0 ].pageX - _canvas.offsetLeft ) * ratio,
			( event.touches[ 0 ].pageY - _canvas.offsetTop ) * ratio,
		);

	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;

	switch( _pointingState )
	{
		case false:
			_pointingHover( p, shift, ctrl );
			break;

		case 'atween':
		{
			const dragbox = config.dragBox;
			if(
				( Math.abs( p.x - _atweenPos.x ) > dragbox )
				|| ( Math.abs( p.y - _atweenPos.y ) > dragbox )
			)
			{
				// moved out of dragbox -> start dragging
				clearTimeout( _atweenTimer );

				_pointingState = 'drag';

				Shell.dragStart( _atweenPos, shift, ctrl );
				Shell.dragMove( p, shift, ctrl );

				_pointingHover( p, shift, ctrl );
				_resetAtweenState( );
				//_captureEvents( );
			}
			else
			{
				// saves position for possible atween timeout
				_atweenMove = p;
			}
			break;
		}

		case 'drag':
		{
			Shell.dragMove( p, shift, ctrl );
			//_pointingHover( p, shift, ctrl );
			break;
		}
	}

	return true;
}

/*
| The using is lifting his/her finger ( on mobile devices)
*/
function _onTouchEnd( event )
{
	event.preventDefault( );
	// for now ignore multi-touches
	if( event.touches.length !== 0 ) return false;

	_inputWord = false;
	//_releaseEvents( );
	const ratio = _resolution.devicePixelRatio;
	const p =
		Point.XY(
			( event.changedTouches[ 0 ].pageX - _canvas.offsetLeft ) * ratio,
			( event.changedTouches[ 0 ].pageY - _canvas.offsetTop ) * ratio
		);
	const shift = event.shiftKey;
	const ctrl = event.ctrlKey || event.metaKey;

	switch( _pointingState )
	{
		case false:
			break;

		case 'atween':
		{
			// A click is a mouse down followed within dragtime by 'mouseup' and
			// not having moved out of 'dragbox'.
			clearTimeout( _atweenTimer );
			Shell.click( p, shift, ctrl );

			_pointingHover( p, shift, ctrl );
			_steerAttention( );
			_resetAtweenState( );

			_pointingState = false;
			break;
		}

		case 'drag':
		{
			Shell.dragStop( p, shift, ctrl );

			_pointingHover( p, shift, ctrl );
			_steerAttention( );

			_pointingState = false;
			break;
		}

		default: throw new Error( );
	}

	return false;
}

/*
| This is mainly used on mobiles.
|
| Checks if the virtual keyboard should be suggested
| and if takes care the caret is scrolled into
| visible screen area.
*/
function _steerAttention( )
{
	let sm = Shell.getSysMode( );

/**/if( CHECK && sm.ti2ctype !== SysMode ) throw new Error( );

	if( sm.keyboard === false )
	{
		_hiddenInput.style.top = '0';
		window.scrollTo( 0, 0 );
		_hiddenInput.blur( );
		_span.focus( );
		_hFocus = false;
	}
	else
	{
		// TODO disabled anyway because buggy and privacy violation
		//ac = EMath.limit( 0, ac, root._display.size.height - 15 );
		//_hiddenInput.style.top = ac + 'px';
		_hiddenInput.style.top = '0';
		_hiddenInput.focus( );
		if( sm.password )
		{
			_hiddenInput.setAttribute( 'type', 'password' );
		}
		else
		{
			_hiddenInput.setAttribute( 'type', 'text' );
		}
		_hFocus = true;
	}
}

/*
| Renders stuff at this level oversized;
*/
def.static._superSampling =
	( ) =>
	config.superSampling;

