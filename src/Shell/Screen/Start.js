/*
| The start screen.
| Shown before any server communication.
*/
def.extend = 'Shell/Screen/Base';

import { Self as DesignFont    } from '{Shell/Design/Font}';
import { Self as FormBase      } from '{Shell/Form/Base}';
import { Self as GlintString   } from '{gleam:Glint/String}';
import { Self as ListGlint     } from '{list@<gleam:Glint/Types}';
import { Self as SysMode       } from '{Shell/Result/SysMode}';

/*
| A click.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	return false;
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	return '';
};

/*
| Cycles the focus in a form.
*/
def.proto.cycleFocus =
	function( dir )
{
	// nothing
};

/*
| Drag moves.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Stops an operation with the mouse button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| A button has been drag-started.
*/
def.proto.dragStartButton =
	function( trace )
{
	// nothing
};

/*
| Returns the glint.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;
	const sdzr = this.sizeDisplay.zeroRect;

	return(
		ListGlint.Elements(
			FormBase.designBackground( light ).glint( sdzr ),
			GlintString.create(
				'align',     'center',
				'fontColor',  DesignFont.NameSizeLight( 'form', 28, light ),
				'p',          sdzr.pc,
				'resolution', this.resolution,
				'string',    'loading',
			)
		)
	);
};

/*
| User is inputting characters.
*/
def.proto.input =
	function( str )
{
	// nothing
};

/*
| Mouse wheel.
|
| ~p:     cursor point
| ~dir:   wheel direction, >0 for down, <0 for up
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	// nothing
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	// nothing
};

/*
| Mouse hover.
| Returns a ResultHover with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.proto.probeClickDrag =
	function( p, shift, ctrl )
{
	return false;
};

/*
| A button has been pushed.
|
| ~traceV: of the button pushed
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	// nothing
};

/*
| Performs some maintenance.
*/
def.proto.rectify =
	function( )
{
	return this;
};

/*
| Scrolls current mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	// nothing
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	// nothing
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	return SysMode.Blank;
};

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	// nothing
};

/*
| Transforms a mark.
*/
def.proto.transformMark =
	function( mark, changeX )
{
	return mark;
};

/*
| A select widget changed.
|
| ~traceV: of the select widget pushed
| ~value:  value to change it to.
*/
def.proto.widgetSelectChanged =
	function( traceV, value )
{
	// nothing
};
