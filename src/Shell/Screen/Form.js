/*
| Shows a form as current screen.
*/
def.extend = 'Shell/Screen/Base';

def.attributes =
{
	// "system", "light" or "dark"
	colorScheme: { type: 'string' },

	// space is public readable
	publicReadable: { type: [ 'undefined', 'boolean' ] },

	// form seams
	seam: { type: 'Shell/Seam/Form/Root' },

	// show space grid
	showGrid: { type: 'boolean' },

	// show space guides
	showGuides: { type: 'boolean' },

	// space has snapping
	snapping: { type: 'boolean' },

	// current user info
	userInfo: { type: [ 'undefined', 'Shared/User/Info' ] },
};

import { Self as ActionScrolly        } from '{Shell/Action/Scrolly}';
import { Self as FormChangePwd        } from '{Shell/Form/ChangePwd}';
import { Self as FormDeleteSpace      } from '{Shell/Form/DeleteSpace}';
import { Self as FormDone             } from '{Shell/Form/Done}';
import { Self as FormLogin            } from '{Shell/Form/Login}';
import { Self as FormMoveTo           } from '{Shell/Form/MoveTo}';
import { Self as FormOpts             } from '{Shell/Form/Opts}';
import { Self as FormSignUp           } from '{Shell/Form/SignUp}';
import { Self as FormSpace            } from '{Shell/Form/Space}';
import { Self as FormSpaceDeleted     } from '{Shell/Form/SpaceDeleted}';
import { Self as FormWelcome          } from '{Shell/Form/Welcome}';
import { Self as ListGlint            } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret            } from '{Shared/Mark/Caret}';
import { Self as MarkItems            } from '{Shared/Mark/Items}';
import { Self as MarkRange            } from '{Shared/Mark/Range}';
import { Self as MarkWidget           } from '{Shared/Mark/Widget}';
import { Self as TraceRoot            } from '{Shared/Trace/Root}';
import { Self as TransformNormal      } from '{gleam:Transform/Normal}';

/*
| Removes empty labels in case they lost the users mark.
|
| ~oldMark: the old mark.
*/
def.proto.checkEmptyLabels =
	function( oldMark )
{
	// does not apply to forms.
};

/*
| A click.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).click( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	return this.form.click( p, shift, ctrl );
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	return this.form.clipboard;
};

/*
| Cycles the focus in a form.
*/
def.proto.cycleFocus =
	function( dir )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	this.form.cycleFocus( dir );
};

/*
| Drag moves.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
	const action = this.action;

	if( !action ) return;

	switch( action.ti2ctype )
	{
		case ActionScrolly:
		{
			const traceV = this.action.traceV;
			const formName = this.mode.formName;

/**/		if( CHECK && traceV.get( 1 ).name !== 'form' ) throw new Error( );

			if( traceV.get( 1 ).key !== formName ) return;

			this.form.dragMove( p, shift, ctrl, action );
			break;
		}
	}
};

/*
| Stops an operation with the mouse button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	const action = this.action;
	if( !action ) return;

	const traceV = action.traceV;

	if( traceV.length < 2 ) return;

	const tv1 = traceV.get( 1 );
	switch( tv1.name )
	{
		case 'form':
			this.form.dragStop( p, shift, ctrl, this.action );
			break;

		case 'panels':
			this._getPanel( tv1.key ).dragStop( p, shift, ctrl, action );
			break;

		case 'view':
			break;

		default: throw new Error( );
	}
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	const action = this.action;
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).dragStart( p, shift, ctrl, action );
		if( bubble !== undefined )
		{
			return;
		}
	}

	this.form.dragStart( p, shift, ctrl );
};

/*
| A button has been drag-started.
*/
def.proto.dragStartButton =
	function( trace )
{
	const tv1 = trace.get( 1 );
	switch( tv1.name )
	{
		case 'panels':
		{
			return this._getPanel( tv1.key ).dragStartButton( trace, false, false );
		}
	}

	return false;
};

/*
| Filters a mark.
|
| TODO check if this cant be done more simply with encompasses( )
| TODO including all other filterMark functions
*/
def.staticLazyFunc.filterMark =
	function( mark )
{
	if( !mark ) return undefined;

	switch( mark.ti2ctype )
	{
		case MarkCaret:
		case MarkWidget:
			if( mark.traceV.backward( 'form' ) )
			{
				return mark;
			}
			else
			{
				return undefined;
			}

		case MarkItems:
			return mark;

		case MarkRange:
			if(
				mark.traceBegin.backward( 'form' )
				|| mark.traceEnd.backward( 'form' )
			)
			{
				return mark;
			}
			else
			{
				return undefined;
			}

		default: throw new Error( );
	}
};

/*
| Current form.
*/
def.lazy.form =
	function( )
{
	const light       = this.light;
	const name        = this.mode.formName;
	const creator     = Self._formCreators[ name ];
	const traceV      = TraceRoot.root.add( 'view', 0 ).add( 'form', name );
	const sizeDisplay = this.sizeDisplay;
	const rectView    = sizeDisplay.zeroRect;
	const windows     = this.windows;
	const window0     = windows.get( 0 );

	let mark = this.mark;
	if( mark && !mark.encompasses( traceV ) )
	{
		mark = undefined;
	}

	let hover = this.hover;
	if( hover && !hover.hasTrace( traceV ) )
	{
		hover = undefined;
	}

	const transform =
		TransformNormal.setScaleOffset(
			this.resolution.ratio,
			sizeDisplay.zeroRect.pc,
		);

	switch( name )
	{
		case 'changePwd':
		{
			return(
				creator.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'systemFocus', this.systemFocus,
					'seam',        this.seam[ name ],
					'sizeDisplay', sizeDisplay,
					'transform',   transform,
					'userCreds',   this.userCreds,
					'traceV',      traceV,
				)
			);
		}

		case 'deleteSpace':
		{
			return(
				creator.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'refSpace',    window0.ref,
					'resolution',  this.resolution,
					'seam',        this.seam[ name ],
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'uidSpace',    window0.uid,
					'traceV',      traceV,
				)
			);
		}

		case 'done':
		{
			return(
				creator.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
				)
			);
		}

		case 'login':
		case 'signUp':
		case 'spaceDeleted':
		{
			return(
				creator.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'seam',        this.seam[ name ],
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
				)
			);
		}

		case 'moveTo':
		{
			const transform = TransformNormal.setScale( this.resolution.ratio );

			return(
				creator.create(
					'distanceLeft',  0,
					'hover',         hover,
					'light',         light,
					'mark',          mark,
					'rectView',      rectView,
					'resolution',    this.resolution,
					'seam',          this.seam[ name ],
					'sizeDisplay',   sizeDisplay,
					'systemFocus',   this.systemFocus,
					'transform',     transform,
					'traceV',        traceV,
					'userInfo',      this.userInfo,
				)
			);
		}

		case 'opts':
		{
			return(
				creator.create(
					'colorScheme', this.colorScheme,
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'seam',        this.seam[ name ],
					'showGrid',    this.showGrid,
					'showGuides',  this.showGuides,
					'sizeDisplay', sizeDisplay,
					'snapping',    this.snapping,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'userCreds',   this.userCreds,
					'userInfo',    this.userInfo,
					'traceV',      traceV,
				)
			);
		}

		case 'space':
		{
			return(
				creator.create(
					'access',         window0.access,
					'hover',          hover,
					'light',          light,
					'mark',           mark,
					'publicReadable', this.publicReadable,
					'rectView',       rectView,
					'refSpace',       window0.ref,
					'resolution',     this.resolution,
					'seam',           this.seam[ name ],
					'sizeDisplay',    sizeDisplay,
					'systemFocus',    this.systemFocus,
					'transform',      transform,
					'traceV',         traceV,
					'window',         window0,
				)
			);
		}

		case 'welcome':
		{
			return(
				creator.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'userCreds',   this.userCreds,
					'traceV',      traceV,
				)
			);
		}

		default: throw new Error( );
	}
};

/*
| Return the space glint.
*/
def.lazy.glint =
	function( )
{
	const form = this.form;
	const list = [ form.glint ];

	for( let key of this._keysPanel.reverse( ) )
	{
		list.push( this._getPanel( key ).glint );
	}

	return ListGlint.Array( list );
};

/*
| User is inputting characters.
*/
def.proto.input =
	function( str )
{
	return this.form.input( str );
};

/*
| Mouse wheel.
|
| ~p:     cursor point
| ~dir:   wheel direction, >0 for down, <0 for up
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).mousewheel( p, dir, shift, ctrl );
		if( bubble ) return bubble;
	}
	return this.form.mousewheel( p, dir, shift, ctrl );
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	return this.form.paste( type, data );
};

/*
| Mouse hover.
| Returns a ResultHover with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const hover = this._getPanel( key ).pointingHover( p, shift, ctrl );
		if( hover ) return hover;
	}

	return this.form.pointingHover( p, shift, ctrl );
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.proto.probeClickDrag =
	function( p, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).probeClickDrag( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	return 'atween';
};

/*
| A button has been pushed.
|
| TODO actually have the button directly deliver the event to its owner.
|
| ~traceV: of the button pushed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const form = this.form;

	const tv1 = traceV.get( 1 );
	switch( tv1.name )
	{
		case 'panels':
			this._getPanel( tv1.key ).pushButton( traceV, shift, ctrl );
			break;

		case 'view':
			form.pushButton( traceV, shift, ctrl );
			break;

		default: throw new Error( );
	}
};

/*
| Performs some maintenance.
*/
def.proto.rectify =
	function( )
{
	return this;
};

/*
| Scrolls current mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	// currently unused.
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	return this.form.specialKey( key, shift, ctrl, inputWord );
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	return this.form.sysMode;
};

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	this.form.toggleCheckbox( traceV );
};

/*
| Transforms a mark.
*/
def.proto.transformMark =
	function( mark, changeX )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	return mark;
};

/*
| A select widget changed.
|
| ~traceV: of the select widget pushed
| ~value:  value to change it to.
*/
def.proto.widgetSelectChanged =
	function( traceV, value )
{
	this.form.widgetSelectChanged( traceV, value );
};

/*
| Creators for the forms
*/
def.staticLazy._formCreators =
	( ) =>
	( {
		changePwd:        FormChangePwd,
		deleteSpace:      FormDeleteSpace,
		done:             FormDone,
		login:            FormLogin,
		moveTo:           FormMoveTo,
		opts:             FormOpts,
		signUp:           FormSignUp,
		space:            FormSpace,
		spaceDeleted:     FormSpaceDeleted,
		welcome:          FormWelcome,
	} );

