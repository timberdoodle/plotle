/*
| Screen base.
*/
def.abstract = true;

def.attributes =
{
	// current action
	action: { type: [ 'undefined', '<Shell/Action/Types' ] },

	// the widget hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// current mode
	mode: { type: [ 'undefined', '<Shell/Mode/Types' ] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// current view size (total screen size)
	sizeDisplay: { type: 'gleam:Size' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// currently logged in user
	userCreds: { type: [ 'undefined', 'Shared/User/Creds' ] },

	// the windows to show
	windows: { type: 'list@<Shell/Window/Types' },
};

import { Self as ListString      } from '{list@string}';
import { Self as PanelTabSpace   } from '{Shell/Panel/TabSpace}';
import { Self as TraceRoot       } from '{Shared/Trace/Root}';
import { Self as TransformNormal } from '{gleam:Transform/Normal}';
import { Self as WindowSpace     } from '{Shell/Window/Space}';

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	// nothing.
};

/*
| Gets a panel.
*/
def.static._getPanel =
def.lazyFunc._getPanel =
	function( key )
{
	let hover = this.hover;
	if( hover )
	{
		const h1 = hover.get( 1 );
		if( h1.name !== 'panels' || h1.key !== key )
		{
			hover = undefined;
		}
	}

	const traceV = TraceRoot.root.add( 'panels', key );
	const sizeDisplay = this.sizeDisplay;

	switch( key )
	{
		case 'tabSpace0':
		{
			const window0 = this.windows.get( 0 );
			const resolution = this.resolution;
			const transform = TransformNormal.setScale( resolution.ratio );

			return(
				PanelTabSpace.create(
					'atWindow',    0,
					'hover',       hover,
					'light',       this.light,
					'mark',        this.mark,
					'mode',        this.mode,
					'rectView',    sizeDisplay.zeroRect,
					'refSpace',    window0.ref,
					'resolution',  resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
					'window',      window0,
				)
			);
		}

		default: throw new Error( );
	}
};

/*
| List of keys of visible panels.
|
| Order is foreground to background.
*/
def.static._keysPanel =
def.lazy._keysPanel =
	function( )
{
	const window0 = this.windows.get( 0 );
	const list = [ ];

	if( window0.ti2ctype === WindowSpace )
	{
		list.push( 'tabSpace0' );
	}

	return ListString.Array( list );
};

