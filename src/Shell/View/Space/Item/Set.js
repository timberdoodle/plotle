/*
| A set of vSpace items.
*/
def.extend = 'set@<Shell/View/Space/Item/Types';

import { Self as GroupRect } from '{group@gleam:Rect}';
import { Self as Point     } from '{gleam:Point}';
import { Self as Rect      } from '{gleam:Rect}';
import { Self as SetTrace  } from '{set@ti2c:Trace}';
import { Self as VDocPath  } from '{Shell/View/Space/Item/DocPath}';
import { Self as VStroke   } from '{Shell/View/Space/Item/Stroke}';

/*
| True if the set is only one item, and it's a doc.
*/
def.lazy.docOnly =
	function( )
{
	return this.trivial?.ti2ctype === VDocPath;
};

/*
| If true the set consits only of strokes.
*/
def.lazy.onlyStrokes =
	function( )
{
	for( let item of this )
	{
		if( item.ti2ctype !== VStroke ) return false;
	}

	return true;
};

/*
| If true one of the set items forces
| proportional resizing.
*/
def.lazy.proportional =
	function( )
{
	for( let item of this )
	{
		if( item.proportional ) return true;
	}

	return false;
};

/*
| The set of traceVs.
*/
def.lazy.traceVs =
	function( )
{
	const set = new Set( );
	for( let item of this )
	{
		set.add( item.traceV );
	}
	return SetTrace.create( 'set:init',  set );
};

/*
| Returns the all encompassing zone.
*/
def.lazy.zone =
	function( )
{
	const size = this.size;

/**/if( CHECK && !( size > 0 ) ) throw new Error( );

	let z, ny, wx, sy, ex;

	for( let item of this )
	{
		if( !z )
		{
			// first iteration
			z = item.ancillary.zone;
			if( size === 1 )
			{
				return z;
			}
			const pos = z.pos;
			ny = pos.y;
			wx = pos.x;
			sy = ny + z.height;
			ex = wx + z.width;
			continue;
		}

		z = item.ancillary.zone;
		const pos = z.pos;
		const zex = pos.x + z.width;
		const zsy = pos.y + z.height;

		if( pos.x < wx )
		{
			wx = pos.x;
		}

		if( zex > ex )
		{
			ex = zex;
		}

		if( pos.y < ny )
		{
			ny = pos.y;
		}

		if( zsy > sy )
		{
			sy = zsy;
		}
	}

	return Rect.PosWidthHeight( Point.XY( wx, ny ), ex - wx, sy - ny );
};

/*
| Returns the group of zones of the items.
*/
def.lazy.zones =
	function( )
{
	const group = { };
	for( let item of this )
	{
		const key = item.ancillary.trace.last.key;

/**/	if( CHECK && group[ key ] ) throw new Error( );

		group[ key ] = item.ancillary.zone;
	}

	return GroupRect.Table( group );
};
