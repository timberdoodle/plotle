/*
| An item with text.
*/
def.abstract = true;

def.extend = 'Shell/View/Space/Item/Base';

import { Self as ActionSelectRange } from '{Shell/Action/SelectRange}';
import { Self as EMath             } from '{Shared/Math/Self}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkRange         } from '{Shared/Mark/Range}';
import { Self as ModeSelect        } from '{Shell/Mode/Select}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as VSpaceItemBase    } from '{Shell/View/Space/Item/Base}';

/*
| Checks if the item is being clicked and reacts.
|
| ~p: point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl: true if ctrl or meta key was held down
| ~mark: current user mark
|        not necessarily identical to ctx.mark since in case of ctrl-clicks
|        an item set with another items should be set as new mark
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	if( ctrl )
	{
		return this._ctrlClick( p, shift, mark );
	}

	if( this.access !== 'rw' )
	{
		return false;
	}

	Shell.alter(
		'action', undefined,
		'mark', this.markForPoint( p, shift ),
	);

	return true;
};

/*
| Handles a potential dragStart event.
|
| ~p:      point where dragging starts
| ~shift:  true if shift key was held down
| ~ctrl:   true if ctrl or meta key was held down
| ~vSpace: current visual space
*/
def.proto.dragStart =
	function( p, shift, ctrl, vSpace )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	// checks if it his the scrollbar
	const sbary = this.scrollbarY;
	const action = vSpace.action;
	if( !action && sbary )
	{
		const bubble = sbary.dragStart( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	switch( this.mark?.ti2ctype )
	{
		case MarkCaret:
		case MarkRange:
		{
			if( !this.within( p ) ) return undefined;

			// TODO actually selecting in readonly mode should work as well.
			if( this.access !== 'rw' ) return undefined;

			let mark = this.markForPoint( p, false );
			Shell.alter(
				'action', ActionSelectRange.create( 'traceV', mark.traceV ),
				'mark', mark
			);

			return true;
		}

		default:
			return VSpaceItemBase.dragStart.call( this, p, shift, ctrl, vSpace );
	}
};

/*
| The user inputs some characters.
*/
def.proto.input =
	function( string )
{
	return this.text.input( string );
};

/*
| Returns the mark for a point
|
| ~p: the point to mark to
| ~doRange: if true possibly make a range
*/
def.proto.markForPoint =
	function( p, doRange )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( p.ti2ctype !== Point ) throw new Error( );
/**/	if( typeof( doRange ) !== 'boolean' ) throw new Error( );
/**/}

	const ancillary = this.ancillary;
	const tp = p.detransform( this.transform );
	const pos = ancillary.zone.pos;
	const pi = tp.sub( pos );
	const text = this.text;
	let para = text.getParaAtPoint( pi );
	let at;
	if( para )
	{
		at = para.getPointOffset( pi.sub( para.pos ) );
	}
	else
	{
		para = text.paras.last;
		at = para.ancillary.string.length;
	}

	const mark = this.mark;
	if( doRange && mark && mark.ti2ctype === MarkCaret )
	{
		return(
			MarkRange.create(
				'traceBegin', mark.traceVCaret,
				'traceEnd', para.offsetTraceV( at ),
			)
		);
	}
	else if( doRange && mark && mark.ti2ctype === MarkRange )
	{
		return mark.create( 'traceEnd', para.offsetTraceV( at ) );
	}
	else
	{
		return MarkCaret.TraceV( para.offsetTraceV( at ) );
	}
};

/*
| A move during a range select on this item.
*/
def.proto.moveSelect =
	function( p )
{
	Shell.alter( 'mark', this.markForPoint( p, true ) );
	this.scrollMarkIntoView( );
};

/*
| User is hovering their pointing device over something.
|
| ~p: point hovered upon
| ~mode: space mode
*/
def.proto.pointingHover =
	function( p, mode )
{
	const sbary = this.scrollbarY;
	if( sbary )
	{
		const bubble = sbary.pointingHover( p );
		if( bubble ) return bubble;
	}

	if( !this.within( p ) ) return undefined;

	let cursor = 'default';
	if( mode.ti2ctype === ModeSelect ) cursor = 'text';

	switch( this.mark?.ti2ctype )
	{
		case MarkCaret:
		case MarkRange:
			cursor = 'text';
			break;
	}

	return(
		Hover.create(
			'cursor', cursor,
			'traceV', this.traceV,
		)
	);
};

/*
| Some TextItems have scrollbars.
*/
def.proto.scrollbarY = undefined;

/*
| Handles a special key.
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	this.text.specialKey( key, shift, ctrl, inputWord );
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	const ancillary = this.ancillary;
	const sp = this.scrollPos;
	const textSysMode = this.text.sysMode( this.mark );

	if( !textSysMode.keyboard ) return textSysMode;

	return(
		SysMode.Keyboard(
			ancillary.zone.pos.y
			+ EMath.limit(
				0,
				textSysMode.attention - ( sp ?  sp.y : 0 ),
				ancillary.zone.height
			)
		)
	);
};

