/*
| Everything there is in a space.
*/
def.abstract = true;

def.attributes =
{
	// users access to current space
	access: { type: 'string' },

	// true if the item is highlighted
	highlight: { type: [ 'undefined', 'boolean' ] },

	// the widget hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// rank of the item (display order)
	rank: { type: 'number' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// transform
	transform: { type: '<gleam:Transform/Types' },

	// the visual trace of the item
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },
};

import { Self as ActionMoveItems } from '{Shell/Action/MoveItems}';
import { Self as MarkItems       } from '{Shared/Mark/Items}';
import { Self as Point           } from '{gleam:Point}';
import { Self as Shell           } from '{Shell/Self}';

/*
| Cycles the focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
|
| ~return: true, the item swallowed the cycle, false cycle on parent.
*/
def.proto.cycleFocus =
	function( dir )
{
	return false;
};

/*
| Dragging start.
| Default behaviour moves the whole item.
|
| ~p:      point where dragging starts
| ~shift:  true if shift key was held down
| ~ctrl:   true if ctrl or meta key was held down
| ~vSpace: current visual space
*/
def.static.dragStart =
def.proto.dragStart =
	function( p, shift, ctrl, vSpace )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) ) return undefined;

	// dragging
	if( this.access !== 'rw' ) return undefined;

	let mark = this.mark;
	if( !mark || mark.ti2ctype !== MarkItems )
	{
		mark = MarkItems.TraceVs( this.traceV );
	}

	const items = vSpace.getSet( mark.itemsMark.traces );

	Shell.alter(
		'action',
			ActionMoveItems.create(
				'items', items,
				'moveBy', Point.zero,
				'pointStart', p.detransform( this.transform ),
				'startZone', this.ancillary.zone,
				'traceV', this.traceV.forward( 'space' ),
			),
		'mark', mark
	);

	return true;
};

/*
| Converts a fabric trace to a visual trace.
| See also traceV2F.
*/
def.proto.traceF2V =
	function( trace )
{
/**/if( CHECK && trace.forward( 'items' ) !== this.ancillary.trace ) throw new Error( );

	let traceV = this.traceV;
	for( let a = 2, alen = trace.length; a < alen; a++ )
	{
		const step = trace.get( a );
		traceV = traceV.addStep( step );
	}

	return traceV;
};

/*
| Nofication when the item lost the users mark.
| Defaults to doing nothing about it.
*/
def.proto.markLost =
	function( )
{
};

/*
| The item's mask defaults to the outline.
*/
def.lazy.mask =
	function( )
{
	return this.outline;
};

/*
| Minimum size of items.
*/
def.proto.sizeMin =
def.static.sizeMin =
	undefined;

/*
| Items by default don't need to be resized proportionally.
*/
def.proto.proportional = false;

/*
| Hook to perform some maintenance.
*/
def.proto.rectify = undefined;

/*
| Scrolls an item note so the caret comes into view.
*/
def.proto.scrollMarkIntoView = undefined;

/*
| The transformed mask.
*/
def.lazy.tMask =
	function( )
{
	return this.mask.transform( this.transform );
};

/*
| The outline in current transform.
*/
def.lazy.tOutline =
	function( )
{
	return this.outline.transform( this.transform );
};

/*
| Zone in current transform.
*/
def.lazy.tZone =
	function( )
{
	return this.ancillary.zone.transform( this.transform );
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	let trace = this.ancillary.trace;
	for( let a = this.traceV.length, alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}
	return trace;
};

/*
| Returns true if a point is within the item.
*/
def.proto.within =
	function( p )
{
	return this.tZone.within( p ) && this.tOutline.within( p );
};

/*
| Generic ctrl click handler.
| Takes care about mutli-selecting item groups by ctrl+click.
|
| ~p:     the point clicked
| ~shift: true if shift key was pressed
|
| ~return: true if it handled the click event.
*/
def.proto._ctrlClick =
	function( p, shift, mark )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	if( this.access !== 'rw' )
	{
		return false;
	}

	if( !mark )
	{
		Shell.alter( 'mark', MarkItems.TraceVs( this.traceV ) );
		return true;
	}
	else
	{
		Shell.alter( 'mark', mark.itemsMark.toggle( this.traceV ) );
		return true;
	}
};
