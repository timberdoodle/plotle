/*
| A stroke (with possible arrow heads)
*/
def.extend = 'Shell/View/Space/Item/Base';

def.attributes =
{
	ancillary: { type: 'Shared/Ancillary/Item/Stroke/Self' },
};

import { Self as ActionMoveItems      } from '{Shell/Action/MoveItems}';
import { Self as Color                } from '{gleam:Color}';
import { Self as GlintFigure          } from '{gleam:Glint/Figure}';
import { Self as Hover                } from '{Shell/Result/Hover}';
import { Self as ListGlint            } from '{list@<gleam:Glint/Types}';
import { Self as MarkItems            } from '{Shared/Mark/Items}';
import { Self as Point                } from '{gleam:Point}';
import { Self as SeamSpaceBenchStroke } from '{Shell/Seam/Space/Bench/Stroke}';
import { Self as Shell                } from '{Shell/Self}';
import { Self as SysMode              } from '{Shell/Result/SysMode}';

/*
| The joints are directly affected by actions.
*/
def.proto.actionAffects = 'joints';

/*
| Checks if the item is being clicked and reacts.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
| ~mark:  current user mark
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	const segment = this.segmentT;

	if( !segment.withinDistance( p, config.lineClickDistance ) )
	{
		return undefined;
	}

	if( this.access !== 'rw' )
	{
		return false;
	}

	if( !mark || !ctrl )
	{
		Shell.alter(
			'action', undefined,
			'mark',   MarkItems.TraceVs( this.traceV ),
			SeamSpaceBenchStroke.traceSeam, SeamSpaceBenchStroke.Clean,
		);
	}
	else
	{
		Shell.alter( 'mark', mark.itemsMark.toggle( this.traceV ) );
	}

	return true;
};

/*
| Handles a potential dragStart event for this item.
|
| ~p:      point where dragging starts
| ~shift:  true if shift key was held down
| ~ctrl:   true if ctrl or meta key was held down
| ~vSpace: current visual space
*/
def.proto.dragStart =
	function( p, shift, ctrl, vSpace )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	const segment = this.segmentT;

	if( !segment.withinDistance( p, config.lineClickDistance ) )
	{
		return undefined;
	}

	if( this.access !== 'rw' )
	{
		return undefined;
	}

	let mark = this.mark;
	if( !mark || mark.ti2ctype !== MarkItems )
	{
		mark = MarkItems.TraceVs( this.traceV );
	}

	const items = vSpace.getSet( mark.itemsMark.traces );

	Shell.alter(
		'action',
			ActionMoveItems.create(
				'items', items,
				'moveBy', Point.zero,
				'pointStart', p.detransform( this.transform ),
				'startZone', this.ancillary.zone,
				'traceV', this.traceV.forward( 'space' ),
			),
		'mark', mark
	);

	return true;
};

/*
| The item's glint.
|
| This cannot be done lazily, since it may
| depend on other items.
*/
def.lazy.glint =
	function( )
{
	const ot = this.tOutline;
	return(
		ListGlint.Elements(
			GlintFigure.FigureFill(       ot, Color.RGB(  255, 225, 40 ) ),
			GlintFigure.FigureColorWidth( ot, Color.RGBA( 255, 225, 80, 0.4 ), 3 ),
			GlintFigure.FigureColor(      ot, Color.RGBA( 200, 100,  0, 0.8 ) ),
		)
	);
};

/*
| No scaling minimum.
*/
def.proto.minScaleX =
def.proto.minScaleY =
	( ) => 0;

/*
| Mouse wheel turned.
*/
def.proto.mousewheel =
	function( p, dir )
{
	return false;
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.outline;
};

/*
| Returns the outline of a joint.
| TODO remove
*/
def.lazyFunc.outlineOfJoint =
	function( index )
{
	switch( index )
	{
		case 0: return this.ancillary.joints.get( 0 ).outline;
		case 1: return this.ancillary.joints.get( 1 ).outline;
		default: throw new Error( );
	}
};

/*
| User is hovering his/her pointing device.
| Checks if this item reacts on this.
|
| ~p: point hovered upon
*/
def.proto.pointingHover =
	function( p )
{
	const segment = this.segmentT;

	if( !segment.withinDistance( p, config.lineClickDistance ) )
	{
		return undefined;
	}

	return Hover.CursorDefault;
};

/*
| Returns the point of a joint.
| TODO remove
*/
def.lazyFunc.pointOfJoint =
	function( index )
{
	const ancillary = this.ancillary;

	switch( index )
	{
		case 0: return ancillary.segment.p0;
		case 1: return ancillary.segment.p1;
		default: throw new Error( );
	}
};

/*
| The positioning style (for example moveItems)
*/
def.proto.positioning = 'stroke';

/*
| Transformed line segment.
*/
def.lazy.segmentT =
	function( )
{
	return this.ancillary.segment.transform( this.transform );
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	return SysMode.Blank;
};

/*
| Transforms the transformed outline of a joint.
*/
def.lazyFunc.tOutlineOfJoint =
	function( index )
{
	const outline = this.outlineOfJoint( index );

	return(
		outline
		? outline.transform( this.transform )
		: undefined
	);
};

/*
| Returns true if a point is within the item.
*/
def.proto.within =
	function( p )
{
	return false;
};
