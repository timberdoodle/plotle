/*
| Extends widget.input to use in an item.
*/
def.extend = 'Shell/Widget/Input';

def.attributes =
{
	// trace of the string in fabric to change
	// TODO rename
	traceFString: { type: [ 'undefined', 'ti2c:Trace' ] },
};

import { Self as ChangeStringMove } from '{ot:Change/String/Move}';
import { Self as Shell            } from '{Shell/Self}';

/*
| Performs an insertion.
|
| Overloads default implementation to change the fabric.
|
| ~at: position to insert at.
| ~str: string to insert.
*/
def.proto.insert =
	function( at, str )
{
	const tfs = this.traceFString;

	Shell.change(
		tfs.first.key,
		ChangeStringMove.create(
			'traceTo', tfs.add( 'offset', at ),
			'val', str,
		),
	);
};

/*
| Performs a removal.
|
| Overloads default implementation to change the fabric.
|
| ~at: position to remove at.
*/
def.proto.remove =
	function( at )
{
	const tfs = this.traceFString;
	const value = this.value;

	Shell.change(
		tfs.first.key,
		ChangeStringMove.create(
			'traceFrom', tfs.add( 'offset', at ),
			'val', value.substring( at, at + 1 ),
		)
	);
};
