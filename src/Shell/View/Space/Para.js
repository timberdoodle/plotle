/*
| A visual paragraph.
*/
def.attributes =
{
	// ancillary fabric of the para
	ancillary: { type: 'Shared/Ancillary/Text/Para' },

	// default color for the font
	colorFontDefault: { type: 'gleam:Color' },

	// size of the font
	fontSize: { type: 'number' },

	// point in north west
	pos: { type: 'gleam:Point' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// scale of the para
	scale: { type: [ 'gleam:Transform/Normal', 'gleam:Transform/Scale' ] },

	// the visual trace of the para
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },

	// width the flow can fill
	widthFlow: { type: 'number' },
};

import { Self as ChangeListMove   } from '{ot:Change/List/Move}';
import { Self as ChangeStringMove } from '{ot:Change/String/Move}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as FabricPara       } from '{Shared/Fabric/Text/Para}';
import { Self as Font             } from '{gleam:Font/Root}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as MarkCaret        } from '{Shared/Mark/Caret}';
import { Self as MarkRange        } from '{Shared/Mark/Range}';
import { Self as ParaPane         } from '{Shell/Para/Pane}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as SysMode          } from '{Shell/Result/SysMode}';

/*
| Maps keys to handlers.
*/
const _keyMap =
	Object.freeze( {
		backspace : '_keyBackspace',
		del       : '_keyDel',
		down      : '_keyDown',
		end       : '_keyEnd',
		enter     : '_keyEnter',
		left      : '_keyLeft',
		pagedown  : '_keyPageDown',
		pageup    : '_keyPageUp',
		pos1      : '_keyPos1',
		right     : '_keyRight',
		up        : '_keyUp'
	} );

/*
| The bland font for this para (without color used for meassuring )
*/
def.lazy.fontBland =
	function( )
{
	return this.fontColor.fontBland;
};

/*
| The fontColor for this para.
*/
def.lazy.fontColor =
	function( )
{
	return DesignFont.SizeColor( this.fontSize, this.colorFontDefault );
};

/*
| Returns the offset closest to a point.
|
| ~point: the point to look for
*/
def.proto.getPointOffset =
	function( point )
{
	if( point.y < 0 )
	{
		return 0;
	}
	const flow = this.ancillary.flow;
	const lines = flow.lines;
	let line;
	for( line = 0; line < lines.length; line++ )
	{
		if( point.y <= lines.get( line ).y ) break;
	}
	if( line >= lines.length )
	{
		line--;
	}
	return this._getOffsetAtLnX( line, point.x );
};

/*
| The para's glint.
*/
def.lazy.glint =
	function( )
{
	return(
		GlintWindow.create(
			'pane', this._pane.glint,
			'pos', this.pos.transform( this.scale ),
		)
	);
};

/*
| The height of the para.
*/
def.lazy.height =
	function( )
{
	const ancillary = this.ancillary;
	return ancillary.flow.height + Math.round( ancillary.fontSize * Font.bottomBox );
};

/*
| A string has been inputed.
*/
def.proto.input =
	function( string, mark )
{
	const changes = [ ];
	const reg = /([^\n]+)(\n?)/g;

	let trace = this.ancillary.trace;
	let at = mark.traceVCaret.last.at;
	let restOfLine;

	for( let rx = reg.exec( string ); rx; rx = reg.exec( string ) )
	{
		const line = rx[ 1 ];

		changes.push(
			ChangeStringMove.create(
				'traceTo', trace.add( 'string' ).add( 'offset', at ),
				'val', line,
			)
		);

		if( rx[ 2 ] )
		{
			if( restOfLine === undefined )
			{
				restOfLine = this.ancillary.string.substring( mark.traceVCaret.last.at );
			}

			const traceN = trace.back.add( 'paras', trace.last.at + 1 );

			changes.push(
				ChangeListMove.create(
					'traceTo', traceN,
					'val', FabricPara.create( 'string', '' ),
				),
				ChangeStringMove.create(
					'traceFrom', trace.add( 'string' ).add( 'offset', at + line.length ),
					'traceTo',   traceN.add( 'string' ).add( 'offset', 0 ),
					'val', restOfLine,
				),
			);

			trace = traceN;
			at = 0;
		}
	}

	Shell.alter( 'clearRetainX', true );
	Shell.change( this.ancillary.trace.first.key, Sequence.Array( changes ) );
};

/*
| True if the para is effectively empty or has only blank characters.
*/
def.lazy.isBlank =
	function( )
{
	return /^\s*$/.test( this.ancillary.string );
};

/*
| Locates the line number of a given offset.
*/
def.lazyFunc.locateOffsetLine =
	function( offset )
{
	this._locateOffset( offset );
	// this is not recursive, it returns
	// the aheaded value set by _locateOffset
	return this.locateOffsetLine( offset );
};

/*
| Locates the line number of a given offset.
*/
def.lazyFunc.locateOffsetPoint =
	function( offset )
{
	this._locateOffset( offset );
	// this is not recursive, it returns
	// the aheaded value set by _locateOffset
	return this.locateOffsetPoint( offset );
};

/*
| Handles a special key.
*/
def.proto.specialKey =
	function( key, vText, mark, shift, ctrl, inputWord )
{
	let at, traceBegin, retainX;

	if( ctrl && key === 'a' )
	{
		const paras = vText.paras;
		const para0 = paras.first;
		const para1 = paras.last;

		Shell.alter(
			'mark',
				MarkRange.create(
					'traceBegin', para0.offsetTraceV( 0 ),
					'traceEnd', para1.offsetTraceV( para1.ancillary.string.length )
				)
		);
		return;
	}

	if( mark.traceVCaret )
	{
		at = mark.traceVCaret.last.at;
		retainX = mark.retainX;
		if( shift )
		{
			traceBegin =
				mark.ti2ctype === MarkRange
				? mark.traceBegin
				: mark.traceV;
		}
	}

	const keyHandler = _keyMap[ key ];
	if( keyHandler )
	{
		this[ keyHandler ]( vText, at, retainX, traceBegin, inputWord );
	}
};

/*
| Returns an offset trace into the string.
*/
def.proto.offsetTraceV =
	function( at )
{
	return this.traceV.add( 'string' ).add( 'offset', at );
};

/*
| The system mode.
*/
def.lazyFunc.sysMode =
	function( mark )
{
	const fs = this.fontSize;
	const p = this.locateOffsetPoint( mark.traceVCaret.last.at );
	return SysMode.Keyboard( this.pos.y + p.y - fs );
};

/*
| Returns the offset by an x coordinate in a flow.
|
| ~ln: line number
| ~x: x coordinate
*/
def.proto._getOffsetAtLnX =
	function( ln, x )
{
	const fontBland = this.fontBland;
	const flow = this.ancillary.flow;
	const line = flow.lines.get( ln );
	const tokens = line.tokens;
	const llen = tokens.length;
	let token, tn;
	for( tn = 0; tn < llen; tn++ )
	{
		token = tokens.get( tn );
		if( x <= token.x + token.width ) break;
	}

	if( tn >= llen && llen > 0 ) token = tokens.get( --tn );
	if( !token ) return 0;

	const dx = x - token.x;
	const word = token.word;
	let a, x1 = 0, x2 = 0;
	for( a = 0; a <= word.length; a++ )
	{
		x1 = x2;
		x2 = fontBland.StringBland( word.substr( 0, a ) ).advanceWidth;
		if( x2 >= dx ) break;
	}
	if( a > word.length ) a = word.length;
	if( dx - x1 < x2 - dx && a > 0 ) a--;

	return token.offset + a;
};

/*
| Backspace pressed.
*/
def.proto._keyBackspace =
	function( vText, at, retainX, traceBegin, inputWord )
{
	if( at > 0 )
	{
		let n = 1;
		if( inputWord ) n = inputWord.length;
		if( n > at ) n = at;

		Shell.alter( 'clearRetainX', true );
		Shell.change(
			this.ancillary.trace.first.key,
			ChangeStringMove.create(
				'traceFrom', this.ancillary.trace.add( 'string' ).add( 'offset', at - n ),
				'val', this.ancillary.string.substring( at - n, at ),
			)
		);
		return;
	}

	const paras = vText.paras;
	const rank = this.ancillary.trace.last.at;

	if( rank === 0 ) return;

	const ve = paras.get( rank - 1 );

	const changes = [ ];
	{
		const val = this.ancillary.string;
		changes.push(
			ChangeStringMove.create(
				'traceFrom',
					this.ancillary.trace.add( 'string' ).add( 'offset', 0  ),
				'traceTo',
					ve.ancillary.trace.add( 'string' ).add( 'offset', ve.ancillary.string.length ),
				'val', val,
			)
		);
	}
	changes.push(
		ChangeListMove.create(
			'traceFrom', this.ancillary.trace,
			'val', FabricPara.create( 'string', '' ),
		)
	);

	Shell.alter( 'clearRetainX', true );
	Shell.change( this.ancillary.trace.first.key, Sequence.Array( changes ) );
};

/*
| Del-key pressed.
*/
def.proto._keyDel =
	function( vText, at, retainX, traceBegin )
{
	if( at < this.ancillary.string.length )
	{
		Shell.alter( 'clearRetainX', true );
		Shell.change(
			this.ancillary.trace.first.key,
			ChangeStringMove.create(
				'traceFrom', this.ancillary.trace.add( 'string' ).add( 'offset', at ),
				'val', this.ancillary.string.substring( at, at + 1 )
			)
		);
		return;
	}

	const paras = vText.paras;
	const trace = this.ancillary.trace;
	const rank = trace.last.at;

	if( rank >= paras.length - 1 ) return;

	const traceN = trace.back.add( 'paras', rank + 1 );
	const str = vText.paras.get( traceN.last.at ).ancillary.string;

	Shell.alter( 'clearRetainX', true );
	Shell.change(
		trace.first.key,
		Sequence.Elements(
			ChangeStringMove.create(
				'traceFrom',
					traceN.add( 'string' ).add( 'offset', 0 ),
				'traceTo',
					trace.add( 'string' ).add( 'offset', this.ancillary.string.length ),
				'val', str,
				'slippyTo', true,
			),
			ChangeListMove.create(
				'traceFrom', traceN,
				'val', FabricPara.create( 'string', '' ),
			),
		),
	);
};

/*
| Down arrow pressed.
*/
def.proto._keyDown =
	function( vText, at, retainX, traceBegin )
{
	const flow = this.ancillary.flow;
	const cPosLine = this.locateOffsetLine( at );
	const cPosP = this.locateOffsetPoint( at );
	const x = retainX !== undefined ? retainX : cPosP.x;

	if( cPosLine < flow.lines.length - 1 )
	{
		// stays within this para
		this._setMark( this._getOffsetAtLnX( cPosLine + 1, x ), x, traceBegin );
		return;
	}
	// goto next para
	const paras = vText.paras;
	const rank = this.ancillary.trace.last.at;

	if( rank < paras.length - 1 )
	{
		const ve = paras.get( rank + 1 );
		at = ve._getOffsetAtLnX( 0, x );
		ve._setMark( at, x, traceBegin );
	}
};

/*
| End-key pressed.
*/
def.proto._keyEnd =
	function( vText, at, retainX, traceBegin )
{
	this._setMark( this.ancillary.string.length, undefined, traceBegin );
};

/*
| Enter-key pressed.
*/
def.proto._keyEnter =
	function( vText, at, retainX, traceBegin )
{
	const trace = this.ancillary.trace;
	const traceN = trace.back.add( 'paras', trace.last.at + 1 );

	const restOfLine =
		// TODO this is this fabric, no?
		vText.paras.get( trace.last.at ).ancillary.string
		.substring( at );

	Shell.change(
		trace.first.key,
		Sequence.Elements(
			ChangeListMove.create(
				'traceTo', traceN,
				'val', FabricPara.create( 'string', '' ),
			),
			ChangeStringMove.create(
				'traceFrom', trace.add( 'string' ).add( 'offset', at ),
				'traceTo', traceN.add( 'string' ).add( 'offset', 0 ),
				'val', restOfLine,
			),
		),
	);
};

/*
| Left arrow pressed.
*/
def.proto._keyLeft =
	function( vText, at, retainX, traceBegin )
{
	if( at > 0 )
	{
		this._setMark( at - 1, undefined, traceBegin );
		return;
	}

	const paras = vText.paras;
	const rank = this.ancillary.trace.last.at;
	if( rank > 0 )
	{
		const ve = paras.get( rank - 1 );
		ve._setMark( ve.ancillary.string.length, undefined, traceBegin );
	}
	else this._setMark( at, undefined, traceBegin );
};

/*
| PageDown key pressed.
*/
def.proto._keyPageDown =
	function( vText, at, retainX, traceBegin )
{
	this._pageUpDown( 1, vText, at, retainX, traceBegin );
};

/*
| PageUp key pressed.
*/
def.proto._keyPageUp =
	function( vText, at, retainX, traceBegin )
{
	this._pageUpDown( -1, vText, at, retainX, traceBegin );
};

/*
| Pos1-key pressed.
*/
def.proto._keyPos1 =
	function( vText, at, retainX, traceBegin )
{
	this._setMark( 0, undefined, traceBegin );
};

/*
| Right arrow pressed.
*/
def.proto._keyRight =
	function( vText, at, retainX, traceBegin )
{
	if( at < this.ancillary.string.length )
	{
		this._setMark( at + 1, undefined, traceBegin );
		return;
	}

	const paras = vText.paras;
	const rank = this.ancillary.trace.last.at;
	if( rank < paras.length - 1 )
	{
		const ve = paras.get( rank + 1 );
		ve._setMark( 0, undefined, traceBegin );
	}
};

/*
| Up arrow pressed.
*/
def.proto._keyUp =
	function( vText, at, retainX, traceBegin )
{
	const cPosLine = this.locateOffsetLine( at );
	const cPosP = this.locateOffsetPoint( at );
	const x = retainX !== undefined ? retainX : cPosP.x;
	if( cPosLine > 0 )
	{
		// stay within this para
		at = this._getOffsetAtLnX( cPosLine - 1, x );
		this._setMark( at, x, traceBegin );
		return;
	}
	// goto prev para
	const paras = vText.paras;
	const rank = this.ancillary.trace.last.at;
	if( rank > 0 )
	{
		const ve = paras.get( rank - 1 );
		at = ve._getOffsetAtLnX( ve.ancillary.flow.lines.length - 1, x );
		ve._setMark( at, x, traceBegin );
	}
};

/*
| Sets the aheadValues for point and line of a given offset.
|
| ~offset: the offset to get the point from.
*/
def.proto._locateOffset =
	function( offset )
{
	const fontBland = this.fontBland;
	const ancillary = this.ancillary;
	const string = ancillary.string;
	const flow = ancillary.flow;
	const lines = flow.lines;
	// determines which line this offset belongs to
	let ln;
	const llen = lines.length - 1;
	for( ln = 0; ln < llen; ln++ )
	{
		if( lines.get( ln + 1 ).offset > offset ) break;
	}
	const line = lines.get( ln );
	const tokens = line.tokens;
	let tn;
	const lLen = tokens.length - 1;
	for( tn = 0; tn < lLen; tn++ )
	{
		if( tokens.get( tn + 1 ).offset > offset ) break;
	}
	let p;
	if( tn < tokens.length )
	{
		const token = tokens.get( tn );
		p =
			Point.XY(
				token.x
				+ fontBland.StringBland( string.substring( token.offset, offset ) ).advanceWidth,
				line.y
			);
	}
	else
	{
		p = Point.XY( 0, line.y );
	}

	ti2c.aheadFunction( this, 'locateOffsetLine', offset, ln );
	ti2c.aheadFunction( this, 'locateOffsetPoint', offset, p );
};

/*
| User pressed page up or down.
|
| FUTURE maintain relative scroll pos
| ~dir: +1 for down, -1 for up
*/
def.proto._pageUpDown =
	function( dir, vText, at, retainX, traceBegin )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( dir !== 1 && dir !== -1 ) throw new Error( );
/**/}

	const p = this.locateOffsetPoint( at );
	const size = vText.clipSize || vText.sizeFull;
	const tp = this.pos.add( retainX ?? p.x, p.y + dir * size.height );
	const tpara =
		vText.getParaAtPoint( tp )
		?? vText.paras.get( dir > 0 ? vText.paras.length - 1 : 0 );
	const tpos = vText.paras.get( tpara.ancillary.trace.last.at ).pos;
	at = tpara.getPointOffset( tp.sub( tpos ) );
	tpara._setMark( at, retainX, traceBegin );
};

/*
| The para pane is independant of it's position.
*/
def.lazy._pane =
	function( )
{
	return(
		ParaPane.create(
			'flow',       this.ancillary.flow,
			'fontColor',  this.fontColor,
			'resolution', this.resolution,
			'scale',      this.scale,
		)
	);
};

/*
| Sets the users caret or range.
|
| ~at: position to mark caret (or end of range)
| ~retainX: retains this x position when moving up/down
| ~traceBegin: begin trace when marking a range
*/
def.proto._setMark =
	function( at, retainX, traceBegin )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const traceV = this.offsetTraceV( at );

	Shell.alter(
		'mark',
			!traceBegin
			? MarkCaret.create(
				'traceV', traceV,
				'retainX', retainX
			)
			: MarkRange.create(
				'traceBegin', traceBegin,
				'traceEnd', traceV,
				'retainX', retainX,
			)
	);
};

/*
| The font for current transform.
*/
def.lazy._tFontFace =
	function( )
{
	return this.design.fontStandardColor( this.scale.d( this.fontSize ) );
};
