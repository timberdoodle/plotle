/*
| A sequence of paragraphs.
*/
def.attributes =
{
	// ancillary fabric of the text
	ancillary: { type: 'Shared/Ancillary/Text/Self' },

	// the visible size of the text, defaults to sizeFull
	clipSize: { type: [ 'undefined', 'gleam:Size' ] },

	// default color for the font
	colorFontDefault: { type: 'gleam:Color' },

	// size of the font (at normal view)
	fontSize: { type: 'number' },

	// inner margin of the text
	innerMargin: { type: [ 'undefined', 'gleam:Margin' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// vertical seperation of paragraphs
	paraSep: { type: [ 'undefined', 'number' ] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// scale of the text
	scale: { type: [ 'gleam:Transform/Normal', 'gleam:Transform/Scale' ] },

	// scroll position of the text
	scrollPos: { type: [ 'undefined', 'gleam:Point' ] },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// the visual trace of the text
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },

	// width available to fill( 0 for labels is infinite)
	widthFlow: { type: [ 'undefined', 'number' ] },
};

import { Self as ChangeListMove   } from '{ot:Change/List/Move}';
import { Self as ChangeStringMove } from '{ot:Change/String/Move}';
import { Self as DesignSelect     } from '{Shell/Design/Select}';
import { Self as FontRoot         } from '{gleam:Font/Root}';
import { Self as FabricPara       } from '{Shared/Fabric/Text/Para}';
import { Self as FigureList       } from '{gleam:Figure/List}';
import { Self as GlintFigure      } from '{gleam:Glint/Figure}';
import { Self as ListGlint        } from '{list@<gleam:Glint/Types}';
import { Self as ListVSpacePara   } from '{list@Shell/View/Space/Para}';
import { Self as MarkCaret        } from '{Shared/Mark/Caret}';
import { Self as MarkRange        } from '{Shared/Mark/Range}';
import { Self as Path             } from '{gleam:Path}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Segment          } from '{gleam:Segment}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as SysMode          } from '{Shell/Result/SysMode}';
import { Self as VSpacePara       } from '{Shell/View/Space/Para}';

/*
| The default font for the text.
*/
/*
def.lazy.fontColor =
	function( )
{
	this.design.fontStandardColor( this.fontSize );
};
*/

/*
| Full size of the text.
|
| Disregards clipping in notes.
*/
def.lazy.sizeFull =
	function( )
{
	return this.ancillary.sizeFull;

	/*
	let height = 0;
	let width = 0;
	const fs = this.fontSize;
	const paraSep = this.paraSep;
	let first = true;

	for( let para of this.paras )
	{
		const flow = para.ancillary.flow;
		width = max( width, flow.width );

		if( !first )
		{
			height += paraSep;
		}
		else
		{
			first = false;
		}

		height += flow.height;
	}

	height += Math.round( fs * FontRoot.bottomBox );
	return Size.WH( width, height );
	*/
};

/*
| Returns the paragraph at point.
*/
def.proto.getParaAtPoint =
	function( p )
{
	const paras = this.paras;
	for( let para of paras )
	{
		if( p.y < para.pos.y + para.ancillary.flow.height ) return para;
	}

	// otherwise
	return undefined;
};

/*
| Return the text's glint.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;
	const list = [ ];
	const mark = this.mark;

	if( mark && this.systemFocus )
	{
		switch( mark.ti2ctype )
		{
			case MarkRange:
				list.push(
					DesignSelect.facetSelectText( light ).glint(
						this._shapeRange.transform( this.scale )
					)
				);
				break;

			case MarkCaret:
				list.push( this._glineCaret );
				break;
		}
	}

	for( let para of this.paras )
	{
		if( para.pos.y + para.height <= 0 ) continue;
		if( para.pos.y > this.sizeFull.height ) continue;

		list.push( para.glint );
	}

	return ListGlint.Array( list );
};

/*
| The user inputs some characters.
|
| ~string: string inputed
*/
def.proto.input =
	function( string )
{
	const mark = this.mark;
	const traceVCaret = mark.traceVCaret;

	if( !traceVCaret ) return false;

	if( mark.ti2ctype === MarkRange && !mark.isEmpty )
	{
		this.removeRange( mark );
		// TODO this is an akward workaround
		Shell.input( string );
		return true;
	}

	return(
		this.paras.get( traceVCaret.backward( 'paras' ).last.at )
		.input( string, mark )
	);
};

/*
| Returns the content of a range.
*/
def.lazyFunc.rangeContent =
	function( range )
{
	const traceFront = range.traceFront;
	const traceBack = range.traceBack;
	const ftp = traceFront.backward( 'paras' );
	const btp = traceBack.backward( 'paras' );
	const paras = this.ancillary.paras;
	const frontRank = ftp.last.at;
	const backRank = btp.last.at;

	if( ftp === btp )
	{
		return(
			paras.get( frontRank ).string
			.substring( traceFront.last.at, traceBack.last.at )
		);
	}

	let content =
		paras.get( frontRank ).string
		.substr( traceFront.last.at );

	for( let r = frontRank + 1, rl = backRank; r < rl; r++ )
	{
		content += '\n' + paras.get( r ).string;
	}

	content += '\n' + paras.get( backRank ).string.substring( 0, traceBack.last.at );
	return content;
};

/*
| Removes a range (spawning over several entities).
*/
def.proto.removeRange =
	function( range )
{
	const traceFront = range.traceFront;
	const traceBack = range.traceBack;

/**/if( CHECK )
/**/{
/**/	if( traceFront.backward( 'text' ) !== this.traceV ) throw new Error( );
/**/	if( traceBack.backward( 'text' ) !== this.traceV ) throw new Error( );
/**/}

	const frontTracePara = traceFront.backward( 'paras' );
	const backTracePara = traceBack.backward( 'paras' );

	const traceParaFrontF = this.traceV2F( frontTracePara );
	const paras = this.paras;

	if ( frontTracePara === backTracePara )
	{
		const val =
			paras.get( frontTracePara.last.at )
			.ancillary.string
			.substring( traceFront.last.at, traceBack.last.at );

		if( val !== '' )
		{
			Shell.change(
				this.ancillary.trace.first.key,
				ChangeStringMove.create(
					'traceFrom',
						traceParaFrontF
						.add( 'string' )
						.add( 'offset', traceFront.last.at ),
					'val',
						paras.get( frontTracePara.last.at )
						.ancillary.string
						.substring( traceFront.last.at, traceBack.last.at )
				)
			);
		}
		return;
	}

	const changes = [ ];
	const frontParaRank = frontTracePara.last.at;
	const backParaRank = backTracePara.last.at;
	const traceFPivot = traceParaFrontF.back;

	{
		// removes the remaining line on begin of the range
		const val =
			paras.get( frontParaRank )
			.ancillary.string
			.substring( traceFront.last.at );

		changes.push(
			ChangeStringMove.create(
				'traceFrom',
					traceParaFrontF
					.add( 'string' )
					.add( 'offset', traceFront.last.at ),
				'val', val,
			)
		);
	}

	// fully deletes all paras inbetween
	for( let r = frontParaRank + 1; r < backParaRank; r++ )
	{
		changes.push(
			ChangeListMove.create(
				'traceFrom', traceFPivot.add( 'paras', frontParaRank + 1 ),
				'val', paras.get( r ).ancillary.asFabric,
			)
		);
	}

	const backParaString = paras.get( backParaRank ).ancillary.string;

	// removes the top part of the last para of the range
	changes.push(
		ChangeStringMove.create(
			'traceFrom',
				traceFPivot
				.add( 'paras', frontParaRank + 1 )
				.add( 'string' )
				.add( 'offset', 0 ),
			'val',
				backParaString.substring( 0, traceBack.last.at ),
		)
	);

	// moves the rest of the last para onto the first para
	changes.push(
		ChangeStringMove.create(
			'traceFrom',
				traceFPivot
				.add( 'paras', frontParaRank + 1 )
				.add( 'string' )
				.add( 'offset', 0 ),
			'traceTo',
				traceParaFrontF
				.add( 'string' )
				.add( 'offset', traceFront.last.at ),
			'val',
				backParaString.substring( traceBack.last.at ),
			'slippyTo', true,
		)
	);

	// removes the last entry which is now empty.
	changes.push(
		ChangeListMove.create(
			'traceFrom', traceFPivot .add( 'paras', frontParaRank + 1 ),
			'val', FabricPara.create( 'string', '' ),
		)
	);

	Shell.change( this.ancillary.trace.first.key, Sequence.Array( changes ) );
};

/*
| Handles a special key.
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	const mark = this.mark;

	if( !mark.hasCaret ) return;

	if( mark.ti2ctype === MarkRange && !mark.isEmpty )
	{
		switch( key )
		{
			case 'backspace':
			case 'del':
				this.removeRange( mark );
				return;
			case 'enter':
				this.removeRange( mark );
				this.specialKey( key, shift, ctrl, inputWord );
				return;
		}
	}

	this.paras.get( mark.traceVCaret.backward( 'paras' ).last.at )
	.specialKey( key, this, mark, shift, ctrl, inputWord );
};

/*
| Returns the sysMode.
*/
def.lazyFunc.sysMode =
	function( mark )
{
	if( !mark || !mark.hasCaret ) return SysMode.Blank;
	const at = mark.traceVCaret.backward( 'paras' ).last.at;
	return this.paras.get( at ).sysMode( mark );
};

/*
| Glint for the caret.
*/
def.lazy._glineCaret =
	function( )
{
	const traceVCaret = this.mark.traceVCaret;
	const scale = this.scale;
	const at = traceVCaret.backward( 'paras' ).last.at;
	const para = this.paras.get( at );
	const fs = this.fontSize;
	const descend = fs * FontRoot.bottomBox;

	let p = para.locateOffsetPoint( traceVCaret.last.at );
	const ppos = para.pos.add( p.x, p.y - fs );
	p = ppos.transform( scale );

	return(
		GlintFigure.FigureColor(
			Segment.P0P1( p, p.add( 0, scale.d( fs + descend ) ) ),
			this.colorFontDefault,
		)
	);
};

/*
| Returns the shape for the selection range.
*/
def.lazy._shapeRange =
	function( )
{
	const mark = this.mark;

/**/if( CHECK && mark.ti2ctype !== MarkRange ) throw new Error( );

	const paras = this.paras;
	const traceFront = mark.traceFront;
	const traceBack = mark.traceBack;
	const frontRank = traceFront.backward( 'paras' ).last.at;
	const backRank = traceBack.backward( 'paras' ).last.at;
	const frontPara = paras.get( frontRank );
	const backPara = paras.get( backRank );
	const frontPos = frontPara.pos;
	const backPos = backPara.pos;
	const fp = frontPara.locateOffsetPoint( traceFront.last.at ).add( frontPos );
	const bp = backPara.locateOffsetPoint( traceBack.last.at ).add( backPos );
	const fLine = frontPara.locateOffsetLine( traceFront.last.at );
	const bLine = backPara.locateOffsetLine( traceBack.last.at );
	const fontSize = this.fontSize;
	const descend = fontSize * FontRoot.bottomBox;
	const ascend = fontSize;
	const innerMargin = this.innerMargin;
	const clipSize = this.clipSize || this.sizeFull;
	const rx = clipSize.width - innerMargin.e;
	const lx = innerMargin.w;
	const frontFlow = frontPara.ancillary.flow;
	const backFlow = backPara.ancillary.flow;

	// f2Rank the para after the front para
	const f2Rank =
		( frontRank + 1 < paras.length )
		? frontRank + 1
		: undefined;
	const f2Para = f2Rank && paras.get( f2Rank );

	if( frontRank === backRank && fLine === bLine )
	{
		// fp o******o bp
		return(
			Path.Plan(
				'pc', ( fp.x + bp.x ) / 2, ( fp.y + bp.y ) / 2,
				'start', fp.add( 0, descend ),
				'line', fp.add( 0, -ascend ),
				'line', bp.add( 0, -ascend ),
				'line', bp.add( 0, descend ),
				'line', 'close'
			)
		);
	}
	else if(
		bp.x < fp.x
		&& (
			frontRank === backRank && fLine + 1 === bLine
			|| (
				f2Rank === backRank
				&& fLine + 1 >= frontFlow.lines.length
				&& bLine === 0
			)
		)
	)
	{
		//         fp o****
		// ****o bp
		return FigureList.Elements(
			Path.Plan(
				'pc', ( fp.x + rx ) / 2, ( 2 * fp.y - ascend + descend ) / 2,
				'start', rx, fp.y - ascend,
				'line', fp.x, fp.y - ascend,
				'line', fp.x, fp.y + descend,
				'line', rx, fp.y + descend,
				'line', 'fly', 'close'
			),
			Path.Plan(
				'pc', ( fp.x + rx ) / 2, ( 2 * fp.y - ascend + descend ) / 2,
				'start', lx, bp.y - ascend,
				'line', bp.x, bp.y - ascend,
				'line', bp.x, bp.y + descend,
				'line', lx, bp.y + descend,
				'line', 'fly', 'close'
			)
		);
	}
	else
	{
		//          6/7            8
		//        fp o------------+
		// fp2  +----+:::::::::::::
		//    5 :::::::::::::::::::
		//      ::::::::::::+-----+  bp2
		//      +-----------o bp   1
		//      4          2/3
		let f2y, b2y;
		if( fLine + 1 < frontFlow.lines.length )
		{
			f2y = frontFlow.lines.get( fLine + 1 ).y + frontPos.y;
		}
		else
		{
			f2y = f2Para.ancillary.flow.lines.get( 0 ).y + f2Para.pos.y;
		}

		if( bLine > 0 )
		{
			b2y = backFlow.lines.get( bLine - 1 ).y + backPos.y;
		}
		else
		{
			const b2Rank = backRank - 1 ;
			const b2Para = paras.get( b2Rank );
			b2y = b2Para.ancillary.flow.lines.last.y + b2Para.pos.y;
		}

		if( traceFront.last.at > 0 )
		{
			return(
				Path.Plan(
					'pc', ( rx + lx ) / 2, ( b2y + descend + f2y - ascend ) / 2,
					'start', rx, b2y + descend,        // 1
					'line', bp.x, b2y + descend,       // 2
					'line', bp.x, bp.y + descend,      // 3
					'line', lx, bp.y + descend,        // 4
					'line', 'fly', lx, f2y - ascend,   // 5
					'line', fp.x, f2y - ascend,        // 6
					'line', fp.x, fp.y - ascend,       // 7
					'line', rx, fp.y - ascend,         // 8
					'line', 'fly', 'close'
				)
			);
		}
		else
		{
			return(
				Path.Plan(
					'pc', ( rx + lx ) / 2, ( b2y + descend + f2y - ascend ) / 2,
					'start', rx, b2y + descend,       // 1
					'line', bp.x, b2y + descend,      // 2
					'line', bp.x, bp.y + descend,     // 3
					'line', lx, bp.y + descend,       // 4
					'line', 'fly', lx, fp.y - ascend, // 7
					'line', rx, fp.y - ascend,        // 8
					'line', 'fly', 'close'
				)
			);
		}
	}
};

/*
| Visual paras.
*/
def.lazy.paras =
	function( )
{
	const list = [ ];
	const paras = this.ancillary.paras;
	let pos, y;
	const innerMargin = this.innerMargin;
	let prev;
	const resolution = this.resolution;
	const traceV = this.traceV;

	for( let a = 0, alen = paras.length; a < alen; a++ )
	{
		const para = paras.get( a );
		const itraceV = traceV?.add( 'paras', a );

		// TODO?:
		y =
			prev
			? prev.pos.y + prev.ancillary.flow.height + this.paraSep
			: innerMargin.n - this.scrollPos.y;

		pos = Point.XY( innerMargin.w, y );

		// TODO weird naming scheme?
		prev =
			VSpacePara.create(
				'ancillary',        para,
				'colorFontDefault', this.colorFontDefault,
				'fontSize',         this.fontSize,
				'pos',              pos,
				'resolution',       resolution,
				'scale',            this.scale,
				'traceV',           itraceV,
				'widthFlow',        this.widthFlow,
			);

		list.push( prev );
	}

	return ListVSpacePara.Array( list );
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK && traceV.forward( 'text' ) !== this.traceV ) throw new Error( );

	let trace = this.ancillary.trace;
	for( let a = traceV.forward( 'text' ).length, alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}

	return trace;
};

