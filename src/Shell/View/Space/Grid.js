/*
| A snapping grid.
*/
def.attributes =
{
	// true if light color scheme
	light: { type: 'boolean' },

	// current view size
	size: { type: 'gleam:Size' },

	// the grid spacing
	spacing: { type: 'gleam:Point' },

	// transform
	transform: { type: [ '<gleam:Transform/Types' ] },
};

import { Self as Color         } from '{gleam:Color}';
import { Self as GlintZoomGrid } from '{gleam:Glint/ZoomGrid}';
import { Self as Point         } from '{gleam:Point}';

/*
| Snaps a point to the grid
*/
def.proto.snap =
	function( p )
{
	const s = this.gSpacing;
	const sx = s.x;
	const sy = s.y;
	const o = this._offset;
	const ox = o.x;
	const oy = o.y;
	return(
		Point.XY(
			Math.round( ( p.x - ox ) / sx ) * sx + ox,
			Math.round( ( p.y - oy ) / sy ) * sy + oy
		)
	);
};

/*
| Spacing with grid factor applied.
*/
def.lazy.gSpacing =
	function( )
{
	const grid = this._grid;
	const spacing = this.spacing;
	return Point.XY( spacing.x * grid, spacing.y * grid );
};

/*
| Spacing with grid factor for visual grid.
*/
def.lazy.gVisualSpacingDetransform =
	function( )
{
	return this.gVisualSpacing.detransform( this.transform.scaleOnly );
};

/*
| Spacing with grid factor for visual grid.
*/
def.lazy.gVisualSpacing =
	function( )
{
	const gsp = this.gSpacing;

	return Point.XY( gsp.x * 4, gsp.y * 4 );
};

/*
| Offset of the major grid.
*/
def.lazy._offset =
	function( )
{
	const transform = this.transform;
	const o = transform.offset;
	const s = this.gVisualSpacing;
	const sx = s.x * 2;
	const sy = s.y * 2;
	return Point.XY( ( ( o.x % sx ) + sx ) % sx, ( o.y % ( sy ) + sy ) % sy );
};

/*
| Returns a glint for this grid.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;

	return(
		GlintZoomGrid.create(
			'colorHeavy',
				light
				? Color.RGB( 160, 160, 160)
				: Color.RGB( 160, 160, 160),
			'colorLight',
				light
				? Color.RGB( 224, 224, 224)
				: Color.RGB(  57,  57,  57),
			'grid', this._grid,
			'offset', this._offset,
			'size', this.size,
			'spacing', this.gVisualSpacing
		)
	);
};

/*
| Normalizes the grid depending on current zoom.
*/
def.lazy._grid =
	function( )
{
	let grid = this.transform.scale;

	while( grid > 1 ) grid /= 2;
	while( grid < 0.5 ) grid *= 2;

	return grid;
};

