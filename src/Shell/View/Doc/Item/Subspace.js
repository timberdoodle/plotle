/*
| DocView of a subspace.
*/
def.extend = 'Shell/View/Doc/Item/Base';

def.attributes =
{
	// the item's fabric
	fabric: { type: [ 'undefined', 'Shared/Fabric/Item/Subspace' ] },

	// margins of text
	innerMargin: { type: 'gleam:Margin' },

	// point in north west
	pos: { type: [ 'undefined', 'gleam:Point' ] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// the width that can be used.
	width: { type: [ 'undefined', 'number' ] },
};

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignFont  } from '{Shell/Design/Font}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as GlintString } from '{gleam:Glint/String}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';
//import { Self as Segment     } from '{gleam:Segment}';
import { Self as Size        } from '{gleam:Size}';

/*
| Checks if the item is being clicked and reacts.
|
| ~p:     point where dragging starts.
| ~shift: true if shift key was held down.
| ~ctrl:  true if ctrl or meta key was held down.
| ~mark:  current users mark.
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) ) return undefined;

	if( this.access !== 'rw' ) return false;

	//mark = this.markForPoint( p, shift ? mark : undefined );
	//Root.alter( 'mark', mark );

	return true;
};

/*
| Handles a potential dragStart event.
|
| ~p: point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl: true if ctrl or meta key was held down
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	// TODO actually selecting in readonly mode should work as well.
	if( this.access !== 'rw' )
	{
		return undefined;
	}

	/*
	let mark = this.markForPoint( p );
	Root.alter(
		'action', ActionSelectRange.create( 'traceV', mark.traceV ),
		'mark', mark
	);
	*/

	return true;
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const glints = [ ];

	const resolution = this.resolution;
	const p = this.pos;
	const fs = Math.round( resolution.ratio * 13 );
	const fontColor = DesignFont.Size( fs );
	const sizeFull = this.sizeFull;
	const viewClip = this.viewClip;

	const paraSep = this.paraSep;
	const yPos = this.pos.y;

	// height, zick-zack
	const hzz = 10 * resolution.ratio;
	const hzz05 = hzz / 2;
	// yPos of separators
	const ySep0 = yPos - paraSep - hzz05;
	const ySep1 = yPos + sizeFull.height + paraSep - hzz05;
	const pathZZ = _pathZZ( ySep0, ySep1, hzz, viewClip.width );

	glints.push(
		GlintFigure.create(
			'figure', pathZZ,
			'fill',   Color.RGB( 100, 100, 100 ),
		),
	);

	glints.push(
		GlintString.create(
			'base', 'middle',
			'fontColor', fontColor,
			'resolution', resolution,
			'string', 'Subspace',
			'p', p.add( 10, sizeFull.height / 2 ),
		)
	);

	glints.push(
		GlintFigure.create(
			'figure', pathZZ,
			'color',  Color.black,
			'width',  1,
		),
	);

	return ListGlint.Array( glints );
};

/*
| Full size of the item without clipping.
*/
def.lazy.sizeFull =
	function( )
{
	const height = 20 * this.resolution.ratio;
	return Size.WH( this.width, height );
};

/*
| Returns true if point 'p' is within the item.
*/
def.proto.within =
	function( p )
{
	const py = p.y;
	const posy = this.pos.y;

	return py >= posy && py <= posy + this.sizeFull.height + this.paraSep;
};

/*
| Helper to create the two path zickzacks
|
| ~yt0: yPos to create the upper at (starts at top)
| ~yt1: yPos to create the lower at (starts at top)
| ~hzz: height of the zickzack
| ~w: width of the zickzack
*/
function _pathZZ( yt0, yt1, hzz, w )
{
	const yb0 = yt0 + hzz;
	const yb1 = yt1 + hzz;

	// side, true top, false bottom:
	let side = true;
	let x = 0;
	const path = [ 'start', Point.XY( 0, yt0 ) ];
	while( x < w )
	{
		x += hzz;
		side = !side;
		path.push( 'line', Point.XY( x, side ? yt0 : yb0 ) );
	}

	x -= hzz;
	side = !side;
	path.push( 'line', 'fly', Point.XY( x, side ? yt1 : yb1 ) );

	while( x > 0 )
	{
		x -= hzz;
		side = !side;
		path.push( 'line', Point.XY( x, side ? yt1 : yb1 ) );
	}

	path.push( 'line', 'fly', 'close' );

	return Path.Plan.apply( undefined, path );
}
