/*
| A doc view item.
*/
def.abstract = true;

def.attributes =
{
	// users access to current space
	access: { type: 'string' },

	// vertical separation of paragraphs
	// double is item separation
	paraSep: { type: 'number' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// the visual trace of the item
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },

	// type of the next item
	typeNext: { type: [ 'undefined', 'protean' ] },

	// type of the prev item
	typePrev: { type: [ 'undefined', 'protean' ] },

	// the clipped rect viewable
	viewClip: { type: 'gleam:Rect' },
};

/*
| Hook to perform some maintenance.
*/
def.proto.rectify = undefined;
