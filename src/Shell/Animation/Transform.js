/*
| The current transform is being animated.
*/
def.attributes =
{
	// the window # to apply the transform to
	atWindow: { type: 'number' },

	// begin time of animation
	timeBegin: { type: 'number' },

	// end time of animation
	timeEnd: { type: 'number' },

	// transformation at begin of animation
	transformBegin: { type: '<gleam:Transform/Types' },

	// transformation at end of animation
	transformEnd: { type: '<gleam:Transform/Types' },
};

import { Self as Point           } from '{gleam:Point}';
import { Self as Shell           } from '{Shell/Self}';
import { Self as TransformNormal } from '{gleam:Transform/Normal}';
import { Self as TraceRoot       } from '{Shared/Trace/Root}';

/*
| Gets the transformation for a frame at time.
*/
def.proto.getTransform =
	function( time )
{
	const timeEnd = this.timeEnd;
	const transformEnd = this.transformEnd;

	if( time >= timeEnd ) return transformEnd;

	const timeBegin = this.timeBegin;
	const transformBegin = this.transformBegin;

	if( time <= timeBegin ) return transformBegin;

	const ratio = ( time - timeBegin ) / ( timeEnd - timeBegin );
	const beginOffset = transformBegin.offset;
	const endOffset = transformEnd.offset;
	const scaleBegin = transformBegin.scale;
	const scaleEnd = transformEnd.scale;

	return(
		TransformNormal.setScaleOffset(
			scaleBegin + ratio * ( scaleEnd - scaleBegin ),
			Point.XY(
				beginOffset.x + ratio * ( endOffset.x - beginOffset.x ),
				beginOffset.y + ratio * ( endOffset.y - beginOffset.y )
			),
		)
	);
};

/*
| Handles a frame for this animation.
|
| ~time: time of the frame
| ~action: current action
*/
def.proto.frame =
	function( time, action )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	Shell.alter(
		TraceRoot.root.add( 'windows', this.atWindow ).add( 'transform' ),
		this.getTransform( time ),
	);

	let going = time < this.timeEnd;
	if( !going )
	{
		if( action && action.finishAnimation )
		{
			going = action.finishAnimation( );
		}
	}

	if( !going )
	{
		Shell.finishAnimation( 'transform' );
	}
};

/*
| Helper for creating a transform animation.
*/
def.static.Now =
	function( atWindow, transformBegin, transformEnd, time )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 )
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/}

	const now = window.performance.now( );

	return(
		Self.create(
			'atWindow',       atWindow,
			'timeBegin',      now,
			'timeEnd',        now + time,
			'transformBegin', transformBegin,
			'transformEnd',   transformEnd,
		)
	);
};

