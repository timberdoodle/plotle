/*
| The animation root is the master of all animations.
*/
def.extend = 'twig@Shell/Animation/Transform';

/*
| Handles a frame for all animations.
|
| ~time: time of the frame
| ~action: current action
*/
def.proto.frame =
	function( time, action )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	let aroot = this;

	for( let a = 0, al = aroot.length; a < al; a++ )
	{
		const key = aroot.getKey( a );
		const anim = aroot.get( key );

		// this animation is finished
		anim.frame( time, action );
	}
};
