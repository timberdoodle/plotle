/*
| A visual text item.
*/
def.attributes =
{
	// scrollPos of scrollbox
	scrollPos: { type: 'gleam:Point' },
};

import { Self as Point } from '{gleam:Point}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'scrollPos': return this.create( 'scrollPos', value );

		default: throw new Error( );
	}
};

/*
| Clean forms.
*/
def.staticLazy.Clean =
	function( )
{
	return Self.create( 'scrollPos', Point.zero );
};
