/*
| Visual space data.
*/
def.attributes =
{
	// bench seam
	bench: { type: 'Shell/Seam/Space/Bench/Self' },

	// text items
	textItems: { type: 'group@Shell/Seam/Space/TextItem' },
};

import { Self as GroupSeamSpaceTextItem } from '{group@Shell/Seam/Space/TextItem}';
import { Self as SeamBench              } from '{Shell/Seam/Space/Bench/Self}';
import { Self as SeamRoot               } from '{Shell/Seam/Root}';
import { Self as SeamSpaceTextItem      } from '{Shell/Seam/Space/TextItem}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 3 );
	const name = base.name;

	switch( name )
	{
		case 'textItems':
		{
			const key = base.key;
			let textItems = this.textItems;
			let item = textItems.get( key );
			if( !item )
			{
				item = SeamSpaceTextItem.Clean;
			}
			item = item.alter( command, value );
			textItems = textItems.set( key, item );
			return this.create( 'textItems', textItems );
		}

		case 'bench':
		{
			return this.create( name, this[ name ].alter( command, value ) );
		}

		default: throw new Error( );
	}
};

/*
| Clean forms.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'bench',     SeamBench.Clean,
			'textItems', GroupSeamSpaceTextItem.Empty,
		)
	);
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamRoot.traceSeam.add( 'space' );
};
