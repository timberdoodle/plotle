/*
| The bench data.
*/
def.attributes =
{
	// stroke bench
	stroke: { type: 'Shell/Seam/Space/Bench/Stroke' },
};

import { Self as SeamSpaceBenchStroke } from '{Shell/Seam/Space/Bench/Stroke}';
import { Self as SeamSpace            } from '{Shell/Seam/Space/Self}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );
	const name = base.name;

	switch( name )
	{
		case 'stroke':
		{
			return(
				this.create(
					name,
					command.length > 5
					? this[ name ].alter( command, value )
					: value
				)
			);
		}

		default: throw new Error( );
	}
};

/*
| Clean forms.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'stroke', SeamSpaceBenchStroke.Clean,
		)
	);
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamSpace.traceSeam.add( 'bench' );
};
