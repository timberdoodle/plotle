/*
| Seam data of the stroke bench.
*/
def.attributes =
{
	// true if the style box is open
	openStyle: { type: 'boolean' },
};

import { Self as SeamSpaceBench } from '{Shell/Seam/Space/Bench/Self}';

/*
| Alters seam data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 5 );
	switch( base.name )
	{
		case 'openStyle':
			return this.create( 'openStyle', value );

		default: throw new Error( );
	}
};

/*
| Clean seam.
*/
def.staticLazy.Clean =
	function( )
{
	return Self.create( 'openStyle', false );
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamSpaceBench.traceSeam.add( 'stroke' );
};
