/*
| Visual seam is extra data on the current view session only.
|
| For example scroll positions of notes.
*/
def.attributes =
{
	// docView seam
	docView: { type: 'Shell/Seam/DocView/Root' },

	// visual forms seam
	forms: { type: 'Shell/Seam/Form/Root' },

	// visual space seam
	space: { type: 'Shell/Seam/Space/Self' },
};

import { Self as SeamDocViewRoot } from '{Shell/Seam/DocView/Root}';
import { Self as SeamFormRoot    } from '{Shell/Seam/Form/Root}';
import { Self as SeamSpaceRoot   } from '{Shell/Seam/Space/Self}';
import { Self as TraceRoot       } from '{Shared/Trace/Root}';

/*
| Alters visual seam.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 2 );
	const name = base.name;
	return this.create( name, this[ name ].alter( command, value ) );
};

/*
| Clean (empty) seam tree.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'docView', SeamDocViewRoot.Clean,
			'forms',   SeamFormRoot.Clean,
			'space',   SeamSpaceRoot.Clean,
		)
	);
};

/*
| Matching seam trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return TraceRoot.root.add( 'seam' );
};
