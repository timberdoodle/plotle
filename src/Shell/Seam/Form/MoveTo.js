/*
| MoveTo form data.
*/
def.attributes =
{
	// scrollPos of scrollbox
	scrollPos: { type: 'gleam:Point' },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';
import { Self as Point } from '{gleam:Point}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'scrollPos':
			return this.create( 'scrollPos', value );

		default:
			throw new Error( );
	}
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'moveTo' );
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'scrollPos', Point.zero,
		)
	);
};
