/*
| Login form data.
*/
def.attributes =
{
	// error message
	errorMessage: { type: 'string' },

	// typed in password
	password: { type: 'string' },

	// typed in username
	username: { type: 'string' },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'errorMessage':
			return this.create( 'errorMessage', value );

		case 'password':
			return this.create( 'password', value );

		case 'username':
			return this.create( 'username', value );

		default:
			throw new Error( );
	}
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'errorMessage', '',
			'password', '',
			'username', '',
		)
	);
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'login' );
};
