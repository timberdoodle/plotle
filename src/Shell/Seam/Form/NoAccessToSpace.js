/*
| No access to space form data.
*/
def.attributes =
{
	// reference of not having access to
	nonRefSpace: { type: [ 'undefined', 'Shared/Ref/Space' ] },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'nonRefSpace':
			return this.create( 'nonRefSpace', value );

		default:
			throw new Error( );
	}
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'noAccessToSpace' );
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return Self.create( );
};
