/*
| Change password form seam.
*/
def.attributes =
{
	// error message
	errorMessage: { type: 'string' },

	// typed in password
	passwordNew1: { type: 'string' },

	// typed in password confirmation
	passwordNew2: { type: 'string' },

	// typed in old password
	passwordOld: { type: 'string' },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';

/*
| Alters visual seam.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'errorMessage':
		case 'passwordNew1':
		case 'passwordNew2':
		case 'passwordOld':
			return this.create( base.name, value );

		default:
			throw new Error( );
	}
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'errorMessage', '',
			'passwordNew1', '',
			'passwordNew2', '',
			'passwordOld',  '',
		)
	);
};

/*
| Matching sesam trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'changePwd' );
};
