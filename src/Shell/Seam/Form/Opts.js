/*
| Space form seam.
*/
def.attributes =
{
	// widgets
	widgets: { type: 'group@<Shell/Seam/Widget/Types' },
};

import { Self as GroupSeamWidget  } from '{group@<Shell/Seam/Widget/Types}';
import { Self as SeamFormRoot     } from '{Shell/Seam/Form/Root}';
import { Self as SeamWidgetSelect } from '{Shell/Seam/Widget/Select}';

/*
| Alters it's data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'widgets':
		{
			const key = base.key;
			let widgets = this.widgets;
			let widget = widgets.get( key );
			widget = widget.alter( command, value );
			widgets = widgets.set( key, widget );
			return this.create( 'widgets', widgets );
		}

		default: throw new Error( );
	}
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'widgets',
				GroupSeamWidget.create(
					'group:set', 'selectColorScheme', SeamWidgetSelect.Clean,
				)
		)
	);
};

/*
| Matching seam trace.
*/
def.staticLazy.traceSeam =
	( ) =>
{
	return SeamFormRoot.traceSeam.add( 'opts' );
};

