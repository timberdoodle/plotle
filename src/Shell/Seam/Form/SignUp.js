/*
| SignUp form seam.
*/
def.attributes =
{
	// typed in email
	email: { type: 'string' },

	// error message
	errorMessage: { type: 'string' },

	// checked newsletter
	newsletter: { type: 'boolean' },

	// typed in password
	password: { type: 'string' },

	// typed in password confirmation
	password2: { type: 'string' },

	// typed in username
	username: { type: 'string' },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';

/*
| Alters visual seam.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'errorMessage':
			return this.create( 'errorMessage', value );

		case 'email':
			return this.create( 'email', value );

		case 'newsletter':
			return this.create( 'newsletter', value );

		case 'password':
			return this.create( 'password', value );

		case 'password2':
			return this.create( 'password2', value );

		case 'username':
			return this.create( 'username', value );

		default:
			throw new Error( );
	}
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'email', '',
			'errorMessage', '',
			'newsletter', false,
			'password', '',
			'password2', '',
			'username', '',
		)
	);
};

/*
| Matching seam trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'signUp' );
};
