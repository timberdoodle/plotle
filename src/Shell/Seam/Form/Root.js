/*
| Form seam.
*/
def.attributes =
{
	// change password seam
	changePwd: { type: 'Shell/Seam/Form/ChangePwd' },

	// login seam
	login: { type: 'Shell/Seam/Form/Login' },

	// delete space seam
	deleteSpace: { type: 'Shell/Seam/Form/DeleteSpace' },

	// move to seam
	moveTo: { type: 'Shell/Seam/Form/MoveTo' },

	// no access to space seam
	noAccessToSpace: { type: 'Shell/Seam/Form/NoAccessToSpace' },

	// opts seam
	opts: { type: 'Shell/Seam/Form/Opts' },

	// signup seam
	signUp: { type: 'Shell/Seam/Form/SignUp' },

	// space seam
	space: { type: 'Shell/Seam/Form/Space' },
};

import { Self as SeamFormChangePwd        } from '{Shell/Seam/Form/ChangePwd}';
import { Self as SeamFormDeleteSpace      } from '{Shell/Seam/Form/DeleteSpace}';
import { Self as SeamFormLogin            } from '{Shell/Seam/Form/Login}';
import { Self as SeamFormMoveTo           } from '{Shell/Seam/Form/MoveTo}';
import { Self as SeamFormNoAccessToSpace  } from '{Shell/Seam/Form/NoAccessToSpace}';
import { Self as SeamFormOpts             } from '{Shell/Seam/Form/Opts}';
import { Self as SeamFormSignUp           } from '{Shell/Seam/Form/SignUp}';
import { Self as SeamFormSpace            } from '{Shell/Seam/Form/Space}';
import { Self as SeamRoot                 } from '{Shell/Seam/Root}';

/*
| Alters visual seam.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 3 );
	const name = base.name;

	if( command.length > 4 )
	{
		return this.create( name, this[ name ].alter( command, value ) );
	}
	else
	{
		return this.create( name, value );
	}
};

/*
| Matching seam trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamRoot.traceSeam.add( 'forms' );
};

/*
| Clean forms.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'changePwd',        SeamFormChangePwd.Clean,
			'deleteSpace',      SeamFormDeleteSpace.Clean,
			'login',            SeamFormLogin.Clean,
			'moveTo',           SeamFormMoveTo.Clean,
			'noAccessToSpace',  SeamFormNoAccessToSpace.Clean,
			'opts',             SeamFormOpts.Clean,
			'signUp',           SeamFormSignUp.Clean,
			'space',            SeamFormSpace.Clean,
		)
	);
};
