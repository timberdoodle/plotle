/*
| DocView data.
*/
def.attributes =
{
	// scrollPos of scrollbox
	scrollPos: { type: 'gleam:Point' },
};

import { Self as SeamRoot } from '{Shell/Seam/Root}';
import { Self as Point } from '{gleam:Point}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 3 );

	switch( base.name )
	{
		case 'scrollPos':
			return this.create( 'scrollPos', value );

		default: throw new Error( );
	}
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamRoot.traceSeam.add( 'docView' );
};

/*
| Clean forms.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'scrollPos', Point.zero,
		)
	);
};
