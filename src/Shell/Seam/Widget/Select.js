/*
| Seam data of a select widget.
*/
def.attributes =
{
	// true of the box is open
	open: { type: 'boolean' },
};

/*
| Alters seam data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 5 );
	switch( base.name )
	{
		case 'open':
			return this.create( 'open', value );

		default: throw new Error( );
	}
};

/*
| Clean seam.
*/
def.staticLazy.Clean =
	function( )
{
	return Self.create( 'open', false );
};
