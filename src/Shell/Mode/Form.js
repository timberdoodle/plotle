/*
| The user is seeing a form.
*/
def.extend = 'Shell/Mode/Base';

def.attributes =
{
	// name of the form
	formName: { type: 'string' },
};

// change password
def.staticLazy.changePwd =
	( ) =>
	Self.create( 'formName', 'changePwd' );

// deleting a space
def.staticLazy.deleteSpace =
	( ) =>
	Self.create( 'formName', 'deleteSpace' );

// a done message
def.staticLazy.done =
	( ) =>
	Self.create( 'formName', 'done' );

// logging in
def.staticLazy.login =
	( ) =>
	Self.create( 'formName', 'login' );

// moving to another space
def.staticLazy.moveTo =
	( ) =>
	Self.create( 'formName', 'moveTo' );

// options view.
def.staticLazy.opts =
	( ) =>
	Self.create( 'formName', 'opts' );

// signing up
def.staticLazy.signUp =
	( ) =>
	Self.create( 'formName', 'signUp' );

// space view
def.staticLazy.space =
	( ) =>
	Self.create( 'formName', 'space' );

// welcome view
def.staticLazy.welcome =
	( ) =>
	Self.create( 'formName', 'welcome' );
