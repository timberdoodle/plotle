/*
| Creating something.
|
| FIXME remove
*/
def.extend = 'Shell/Mode/Base';

def.attributes =
{
	// item type to be created
	nameItemType: { type: [ 'undefined', 'string' ] },
};

import { Self as VDocPath  } from '{Shell/View/Space/Item/DocPath}';
import { Self as VLabel    } from '{Shell/View/Space/Item/Label}';
import { Self as VNote     } from '{Shell/View/Space/Item/Note}';
import { Self as VSubspace } from '{Shell/View/Space/Item/Subspace}';

/*
| Shortcuts.
*/
def.staticLazy.Arrow =
	( ) =>
	Self.create( 'nameItemType', 'arrow' );

def.staticLazy.DocPath =
	( ) =>
	Self.create( 'nameItemType', 'docPath' );

def.staticLazy.Label =
	( ) =>
	Self.create( 'nameItemType', 'label' );

def.staticLazy.Line =
	( ) =>
	Self.create( 'nameItemType', 'line' );

def.staticLazy.None =
	( ) =>
	Self.create( 'nameItemType', 'none' );

def.staticLazy.Note =
	( ) =>
	Self.create( 'nameItemType', 'note' );

def.staticLazy.Relation =
	( ) =>
	Self.create( 'nameItemType', 'relation' );

def.staticLazy.Subspace =
	( ) =>
	Self.create( 'nameItemType', 'subspace' );


/*
| Returns the tim of the item to be created.
*/
def.lazy.ti2cItem =
	function( )
{
	const ti2cItem = Self._nameItemTypeToVType[ this.nameItemType ];
/**/if( CHECK && !ti2cItem ) throw new Error( );
	return ti2cItem;
};

/*
| True if creating a stroke
*/
def.lazy.isStroke =
	function( )
{
	const nit = this.nameItemType;
	return nit === 'arrow' || nit === 'line';
};

/*
| Maps item type names to ti2ctypes.
*/
def.staticLazy._nameItemTypeToVType =
	( ) =>
( {
	docPath:  VDocPath,
	label:    VLabel,
	note:     VNote,
	subspace: VSubspace,
} );

