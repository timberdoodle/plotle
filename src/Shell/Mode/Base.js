/*
| The generic mode base.
|
| The difference to "Action" is, an Action is something that starts and ends with a drag.
| Mode is selected on the panel.
| For example there can be a panning action while the CreateRelation mode is active.
|
| This is nonsense, lets remove Mode.
*/
def.abstract = true;
