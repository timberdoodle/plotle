/*
| Selecting something.
*/
def.extend = 'Shell/Mode/Base';

def.attributes =
{
	// return to this mode, after shift key is released.
	fallbackMode: { type: '<Shell/Mode/Types' },
};
