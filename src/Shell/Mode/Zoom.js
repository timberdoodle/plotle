/*
| Showing the zoom panel.
*/
def.extend = 'Shell/Mode/Base';

def.attributes =
{
	// number of the window this is on.
	atWindow: { type: 'number' },
};

/*
| Shortcut.
*/
def.staticLazyFunc.AtWindow =
	( atWindow ) =>
	Self.create( 'atWindow', atWindow );
