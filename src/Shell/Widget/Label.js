/*
| A label.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// horizonal alignment
	align: { type: 'string', defaultValue: '"left"' },

	// vertical alignment
	base: { type: 'string', defaultValue: '"alphabetic"' },

	// font family, size and color of the string
	fontColor: { type: [ 'undefined', 'gleam:Font/Color' ] },

	// vertical distance of newline
	newline: { type: [ 'undefined', 'number' ] },

	// designed position
	pos: { type: 'gleam:Point' },

	// the label string
	string: { type: 'string' },
};

import { Self as GlintString } from '{gleam:Glint/String}';
import { Self as DesignLabel } from '{Shell/Design/Base/Widget/Label}';

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Creates an actual widget from a design.
|
| ~designWidget: widget design
| ~traceV:       trace of the widget
| ~resolution:   current screen resolution
| ~systemFocus:  true if having the system focus
| ~transform:    transform of the widget
*/
def.static.FromDesign =
	function( designWidget, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( designWidget.ti2ctype !== DesignLabel ) throw new Error( );
/**/}

	return(
		Self.create(
			'align',      designWidget.align,
			'base',       designWidget.base,
			'fontColor',  designWidget.font,
			'newline',    designWidget.newline,
			'pos',        designWidget.pos,
			'resolution', resolution,
			'string',     designWidget.string,
			'transform',  transform,
			'traceV',     traceV,
			'visible',    true,
		)
	);
};

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible ) return undefined;

	return(
		GlintString.create(
			'align',      this.align,
			'base',       this.base,
			'fontColor',  this.fontColor.transform( this.transform ),
			'name',       this.traceV.asString,
			'p',          this._pos,
			'resolution', this.resolution,
			'string',     this.string,
		)
	);
};

/*
| Mouse wheel is being turned.
*/
def.proto.mousewheel =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| User is hovering his/her pointer ( mouse move )
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| The transformed position of the label.
*/
def.lazy._pos =
	function( )
{
	return this.pos.transform( this.transform );
};
