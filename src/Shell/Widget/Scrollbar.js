/*
| A scrollbar.
|
| Used by note and scrollbox.
| Currently there are only vertical scrollbars.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// align the scrollbar to pos as anchor as
	// "left", "center" or "right"
	anchorAlign: { type: 'string' },

	// the size of the bar (the thick part of the total length)
	aperture: { type: 'number' },

	// round cap
	ellipseA: { type: 'number' },
	ellipseB: { type: 'number' },

	// facet of the scrollbar
	facet: { type: 'Shell/Facet/Self' },

	// maximum scroll position
	// minimum is always zero
	max: { type: 'number' },

	// minimum height of the scrollbar
	minHeight: { type: 'number' },

	// position of the scrollbar
	pos: { type: 'gleam:Point' },

	// scroll position
	scrollPos: { type: 'number' },

	// total length of the scrollbar
	size: { type: 'number' },

	// strength/width of the scrollbar
	strength: { type: 'number' },
};

import { Self as ActionScrolly } from '{Shell/Action/Scrolly}';
import { Self as Hover         } from '{Shell/Result/Hover}';
import { Self as RectRound     } from '{gleam:RectRound}';
import { Self as Shell         } from '{Shell/Self}';

/*
| Handles a potential dragStart event.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	if( !this.tOutline.within( p ) ) return undefined;

	Shell.alter(
		'action',
			ActionScrolly.create(
				'traceV', this.traceV.back,
				'pointStart', p,
				'startPos', this.scrollPos
			)
	);

	return true;
};

/*
| The scrollbar's glint.
*/
def.lazy.glint =
	function( )
{
	return this.facet.glint( this.tOutline );
};

/*
| Returns the outline of the scrollbar.
*/
def.lazy.outline =
	function( )
{
	const transform = this.transform;
	const ratio = this.resolution.ratio;

	const pos = this.pos;
	const size = this.size;
	const scrollPos = this.scrollPos;
	const max = this.max;
	const ap = this.aperture * size / max;
	const map = Math.max( ap, this.minHeight );
	const sy = scrollPos * ( ( size - map + ap ) / max );

	// strength ought to be independed of transform, but depended on resolution
	const st = transform.ded( this.strength ) * ratio;

	let ax;
	switch( this.anchorAlign )
	{
		case 'left':   ax = 0; break;
		case 'center': ax = -st / 2; break;
		case 'right':  ax = -st; break;
	}

	return(
		RectRound.create(
			'pos', pos.add( ax, sy ),
			'width', st,
			'height', map,
			'a', transform.ded( this.ellipseA ) * ratio,
			'b', transform.ded( this.ellipseB ) * ratio,
		)
	);
};

/*
| User is hovering his/her pointing device.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	if( !this.tOutline.within( p ) ) return undefined;

	return Hover.CursorNsResize.create( 'traceV', this.traceV );
};

/*
| Returns the value of pos change for d pixels in the current zone.
*/
def.proto.scale =
	function( d )
{
	return d * this.max / this.size;
};

/*
| Outline in current transform.
*/
def.lazy.tOutline =
	function( )
{
	return this.outline.transform( this.transform );
};
