/*
| Manages the shell dynamics
*/
def.abstract = true;

import { Self as ASpace                  } from '{Shared/Ancillary/Space}';
import { Self as ChangeWrap              } from '{Shared/ChangeWrap/Self}';
import { Self as ChangeWrapList          } from '{Shared/ChangeWrap/List}';
import { Self as CurrentUserInfo         } from '{Shared/Dynamic/Current/UserInfo}';
import { Self as DynamicCurrentSpace     } from '{Shared/Dynamic/Current/Space}';
import { Self as DynamicUserInfo         } from '{Shell/Dynamic/UserInfo}';
import { Self as DynamicSpace            } from '{Shell/Dynamic/Space/Self}';
import { Self as GroupDynamicSpace       } from '{Shell/Dynamic/Space/Group}';
import { Self as ListDynamicMoment       } from '{list@<Shared/Dynamic/Moment/Types}';
import { Self as ListUpdateSpace         } from '{list@Shared/Dynamic/Update/Space}';
import { Self as ReplyError              } from '{Shared/Reply/Error}';
import { Self as RefSpace                } from '{Shared/Ref/Space}';
import { Self as ReUndo                  } from '{Shell/ReUndo/Stack}';
import { Self as Shell                   } from '{Shell/Self}';
import { Self as Sequence                } from '{ot:Change/Sequence}';
import { Self as System                  } from '{Shell/System}';
import { Self as TransactionAlter        } from '{Shell/Transaction/Alter}';
import { Self as TransactionSpaceAcquire } from '{Shell/Transaction/SpaceAcquire}';
import { Self as TransactionUpdate       } from '{Shell/Transaction/Update}';
import { Self as UpdateSpace             } from '{Shared/Dynamic/Update/Space}';
import { Self as UpdateUserInfo          } from '{Shared/Dynamic/Update/UserInfo}';
import { Self as UserCreds               } from '{Shared/User/Creds}';


// user info dynamic.
let _dynamicUserInfo;

// the space dynamics
let _spaces;

// ongoing update transaction
let _transactionUpdate;


// currently logged in user
let _userCreds;

/*
| Acquires a space into dynMan.
|
| ~refSpace:      reference of space
| ~createMissing: asks server to create the space if missing
*/
def.static.acquireSpace =
	function( refSpace, createMissing )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( refSpace.ti2ctype !== RefSpace ) throw new Error( );
/**/	if( typeof( createMissing ) !== 'boolean' ) throw new Error( );
/**/}

	if( _transactionUpdate )
	{
		_transactionUpdate.abort( );
		_transactionUpdate = undefined;
	}

	TransactionSpaceAcquire.start( _userCreds, refSpace, createMissing );
};

/*
| The locals as ancillaries.
*/
def.static.ancillaries =
	function( )
{
	return _spaces.ancillaries;
};

/*
| Applies a change to a space dynamic.
|
| ~uidSpace: uid of space to change.
| ~change: change (or sequence of) to applies
*/
def.static.changeSpace =
	function( uidSpace, change )
{
	let seq =
		change.ti2ctype !== Sequence
		? Sequence.Elements( change )
		: change;

	let ds = _spaces.get( uidSpace );
	let fSpace = seq.changeTree( ds.local );
	let aSpace = ASpace.FromFabric( fSpace, uidSpace );
	for(;;)
	{
		const achgs = aSpace.changes( seq.affectedTwigItems );
		if( !achgs ) break;

		fSpace = achgs.changeTree( fSpace );
		aSpace = ASpace.FromFabric( fSpace, uidSpace );

		seq = seq.appendList( achgs );
	}

	const wrap = ChangeWrap.Wrapped( seq );

	ds = ds.enqueue( wrap );
	_spaces = _spaces.set( uidSpace, ds );

	// TODO send changes right here?

	return wrap;
};

/*
| Initializes the dynamic manager.
*/
def.static.init =
	function( )
{
	_spaces = GroupDynamicSpace.Empty;
};

/*
| Current user info.
*/
def.static.userInfo =
	function( )
{
	return _dynamicUserInfo?.current;
};

/*
| Acquired a space.
|
| ~current: current space moment acquried.
*/
def.static.onSpaceAcquire =
	function( current )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( current.ti2ctype !== DynamicCurrentSpace ) throw new Error( );
/**/}

	_spaces =
		_spaces.set( current.moment.uid, DynamicSpace.FromShared( current ) );
};

/*
| Received an update.
*/
def.static.onUpdate =
	function( transaction, reply )
{
	if( reply.ti2ctype === ReplyError )
	{
		System.failScreen( reply.message );
		return;
	}

	for( let update of reply )
	{
		switch( update.ti2ctype )
		{
			case UpdateSpace:
			{
				const uid = update.moment.uid;
				let ds = _spaces.get( uid );
				let rv = ds.update( update );
				ds = rv.space;
				_spaces = _spaces.set( uid, ds );

				if( ds.deleted )
				{
					Shell.spaceDeleted( uid );
				}
				else
				{
					const report = rv.report;
					if( report.length > 0 )
					{
						Shell.updateMarks( report );
						ReUndo.update( report );
					}
				}
				break;
			}

			case UpdateUserInfo:
			{
				_dynamicUserInfo = _dynamicUserInfo.applyUpdate( update );
				break;
			}

			default: throw new Error( 'unexpected dynamic type from server' );
		}
	}

	// sends changes (if not waiting for last batch)
	Self.sendChanges( );

	// issues the following update request
	Self.sendUpdate( );
};

/*
| Sends changes.
*/
def.static.sendChanges =
	function( )
{
	if( !_userCreds )
	{
		console.log( 'trying to send changes while not logged in.' );
		return;
	}

	// first checks if any space is sending, if so
	// wait until those changes are received before sending more
	for( let ds of _spaces )
	{
		if( ds.postbox.length > 0 )
		{
			return;
		}
	}

	const updates = [ ];
	for( let ds of _spaces )
	{
		// nothing to send?
		if( ds.outbox.length === 0 )
		{
			continue;
		}

		const postbox = ds.outbox;

		ds =
			ds.create(
				'postbox', postbox,
				'outbox', ChangeWrapList.Empty,
			);

		_spaces = _spaces.set( ds.moment.uid, ds );

		updates.push(
			UpdateSpace.create(
				'changeWrapList', postbox,
				'moment',         ds.moment,
			)
		);
	}

	TransactionAlter.start( _userCreds, ListUpdateSpace.Array( updates ) );
};

/*
| Sends an update request to the server.
*/
def.static.sendUpdate =
	function( )
{
	const moments = [ ];

	for( let uid of _spaces.keys )
	{
		moments.push( _spaces.get( uid ).moment );
	}

	const momentRefSpacesUser = _dynamicUserInfo?.moment;
	if( momentRefSpacesUser )
	{
		moments.push( momentRefSpacesUser );
	}

	_transactionUpdate =
		TransactionUpdate.start( _userCreds, ListDynamicMoment.Array( moments ) );
};

/*
| Sets the current user.
*/
def.static.setUser =
	function( userCreds, currentUserInfo )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( userCreds && userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( currentUserInfo && currentUserInfo.ti2ctype !== CurrentUserInfo ) throw new Error( );
/**/}

	_userCreds = userCreds;

	_dynamicUserInfo =
		currentUserInfo
		? DynamicUserInfo.FromShared( currentUserInfo )
		: undefined;
};
