/*
| A dynamic space.
*/
def.attributes =
{
	// the current space
	// undefined when deleted
	current: { type: [ 'undefined', 'Shared/Fabric/Space' ] },

	// true if the space has been deleted
	deleted: { type: 'boolean' },

	// Reference to the current moment of the space.
	moment: { type: 'Shared/Dynamic/Moment/Space' },

	// Changes to be send to the server.
	outbox: { type: 'Shared/ChangeWrap/List' },

	// Changes that are currently on the way.
	postbox: { type: 'Shared/ChangeWrap/List' },
};

import { Self as ChangeWrapList      } from '{Shared/ChangeWrap/List}';
import { Self as DynamicCurrentSpace } from '{Shared/Dynamic/Current/Space}';

/*
| Enqueues a change wrap.
*/
def.proto.enqueue =
	function( changeWrap )
{
	return this.create( 'outbox', this.outbox.append( changeWrap ) );
};

/*
| Creates the client dynamic from a current.
*/
def.static.FromShared =
	function( current )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( current.ti2ctype !== DynamicCurrentSpace ) throw new Error( );
/**/}

	return(
		Self.create(
			'current', current.current,
			'deleted', false,
			'moment',  current.moment,
			'outbox',  ChangeWrapList.Empty,
			'postbox', ChangeWrapList.Empty,
		)
	);
};

/*
| Builds the space by queued changes.
*/
def.lazy.local =
	function( )
{
	let space = this.current;
	space = this.postbox.changeTree( space );
	space = this.outbox.changeTree( space );
	return space;
};

/*
| Updates the dynamic.
|
| ~update: the dynamic update
*/
def.proto.update =
	function( update )
{
	const changeWrapList = update.changeWrapList;

/**/if( CHECK )
/**/{
/**/	// the server should never say, there are updates for a space
/**/	// and then doesn't have any
/**/	if( changeWrapList.length === 0 ) throw new Error( );
/**/}

	let report = ChangeWrapList.Empty;
	let postbox = this.postbox;
	let current = this.current;

	for( let cw of changeWrapList )
	{
		if( cw.deleted )
		{
			return( {
				'space':
					this.create(
						'current', undefined,
						'deleted', true,
						'outbox',  ChangeWrapList.Empty,
						'postbox', ChangeWrapList.Empty,
						'moment',
							this.moment.create(
								'seq', this.moment.seq + changeWrapList.length
							),
					),
				'report': false,
			} );
		}

		// applies changes to the space
		current = cw.changeTree( current );

		// if the id is the one in the postbox
		// the shell received its own changes
		if( postbox.length > 0 && postbox.get( 0 ).id === cw.id )
		{
			postbox = postbox.remove( 0 );
		}
		else
		{
			// otherwise it was a foreign change
			report = report.append( cw );
		}
	}

	// transforms the postbox by the updated stuff
	postbox = report.transform( postbox );

	// transforms the outbox by the foreign changes
	const outbox = report.transform( this.outbox );

	// TODO remove temporary return object
	return( {
		'space':
			this.create(
				'current', current,
				'moment',
					this.moment.create(
						'seq', this.moment.seq + changeWrapList.length
					),
				'outbox', outbox,
				'postbox', postbox,
			),
		'report': report,
	} );
};

