/*
| A dynamic user info
*/
def.extend = 'Shared/Dynamic/Current/UserInfo';

import { Self as ChangeWrap } from '{Shared/ChangeWrap/Self}';
import { Self as Sequence   } from '{ot:Change/Sequence}';

/*
| Returns the altered dynamic.
|
| ~arg: change, several changes or array of changes
*/
def.proto.alter =
	function( ...args )
{
	const a0 = args[ 0 ];

	let sq;
	if( a0.ti2ctype === Sequence )
	{
		sq = a0;
	}
	else if( Array.isArray( a0 ) )
	{
		sq = Sequence.Array( a0 );
	}
	else
	{
		sq = Sequence.Array( args );
	}

	const changeWrap = ChangeWrap.Wrapped( sq );
	const moment = this.moment;
	return(
		this.create(
			'current', changeWrap.changeTree( this.current ),
			'moment', moment.create( 'seq', moment.seq + 1 ),
			//'changeWraps', this.changeWraps.append( changeWrap ),
		)
	);
};

/*
| Applies an update
*/
def.proto.applyUpdate =
	function( update )
{
	const moment = this.moment;

/**/if( CHECK && update.moment !== moment ) throw new Error( );

	const changeWrapList = update.changeWrapList;

	return(
		this.create(
			//'changeWraps', this.changeWraps.appendList( changeWrapList ),
			'moment', moment.create( 'seq', moment.seq + changeWrapList.length ),
			'current', changeWrapList.changeTree( this.current ),
		)
	);
};

/*
| Creates the client dynamic from the shared representation.
*/
def.static.FromShared =
	function( shared )
{
	return(
		Self.create(
			//'changeWraps', ChangeWrapList.Empty,
			'current', shared.current,
			'moment',  shared.moment,
		)
	);
};
