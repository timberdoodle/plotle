/*
| The global interface for the shell.
*/
def.abstract = true;

import { Self as Animation                 } from '{Shell/Animation/Self}';
import { Self as AnimationTransform        } from '{Shell/Animation/Transform}';
import { Self as ChangeTreeShrink          } from '{ot:Change/Tree/Shrink}';
import { Self as Color                     } from '{gleam:Color}';
import { Self as DynamicManager            } from '{Shell/Dynamic/Manager}';
import { Self as EMath                     } from '{Shared/Math/Self}';
import { Self as GroupASpace               } from '{group@Shared/Ancillary/Space}';
import { Self as ListWindow                } from '{list@<Shell/Window/Types}';
import { Self as ModeForm                  } from '{Shell/Mode/Form}';
import { Self as ModeNormal                } from '{Shell/Mode/Normal}';
import { Self as ModeSelect                } from '{Shell/Mode/Select}';
import { Self as Point                     } from '{gleam:Point}';
import { Self as RefLocation               } from '{Shared/Ref/Location}';
import { Self as RefSpace                  } from '{Shared/Ref/Space}';
import { Self as ReplyAuth                 } from '{Shared/Reply/Auth}';
import { Self as ReplyError                } from '{Shared/Reply/Error}';
import { Self as ReplySignUp               } from '{Shared/Reply/SignUp}';
import { Self as Resolution                } from '{gleam:Display/Canvas/Resolution}';
import { Self as ReUndo                    } from '{Shell/ReUndo/Stack}';
import { Self as Root                      } from '{Shell/Root}';
import { Self as SeamRoot                  } from '{Shell/Seam/Root}';
import { Self as Sequence                  } from '{ot:Change/Sequence}';
import { Self as SetTrace                  } from '{set@ti2c:Trace}';
import { Self as Size                      } from '{gleam:Size}';
import { Self as System                    } from '{Shell/System}';
import { Self as TraceRoot                 } from '{Shared/Trace/Root}';
import { Self as TransactionAlter          } from '{Shell/Transaction/Alter}';
import { Self as TransactionAuth           } from '{Shell/Transaction/Auth}';
import { Self as TransactionChangePwd      } from '{Shell/Transaction/ChangePwd}';
import { Self as TransactionChangeUserNews } from '{Shell/Transaction/ChangeUserNews}';
import { Self as TransactionDeleteSpace    } from '{Shell/Transaction/DeleteSpace}';
import { Self as TransactionSignUp         } from '{Shell/Transaction/SignUp}';
import { Self as TransactionSpaceAcquire   } from '{Shell/Transaction/SpaceAcquire}';
import { Self as TransactionUpdate         } from '{Shell/Transaction/Update}';
import { Self as TransformNormal           } from '{gleam:Transform/Normal}';
import { Self as UserCreds                 } from '{Shared/User/Creds}';
import { Self as WindowDeleted             } from '{Shell/Window/Deleted}';
import { Self as WindowDocView             } from '{Shell/Window/DocView}';
import { Self as WindowLoading             } from '{Shell/Window/Loading}';
import { Self as WindowNoAccess            } from '{Shell/Window/NoAccess}';
import { Self as WindowNonExisting         } from '{Shell/Window/NonExisting}';
import { Self as WindowSpace               } from '{Shell/Window/Space}';

/*
| The display where everything happens.
*/
let _display;

/*
| The root object.
*/
let root;
//globalThis.root = { };

/*
| If defined a delayed update timer is in place.
*/
let _timerDelayedUpdate;

/*
| TODO missing:
|
| Adjust the current action.
|
| Makes sure the action has not any removed items in them.
| If so the trace are removed from its itemTraceSet.
|
| If no item is left, action is set to none.
*/

/*
| Alters the shell.
|
| Free args:
|
| 'action' [value]
|   Alters current action.
|
| 'clearRetainX' [true]
|   Clears the carets x position reminder.
|
| 'hover' [value]
|   Alters current hover information.
|
| 'mark' [value]
|   Alters users mark (for example caret, range, item selection).
|
| 'mode' [value]
|   Alters current mode.
|
| 'ratioSplit' [0.0 - 1.0]
|   Alters the ratio at which the split screen resides.
|
| 'resolution' [value]
|   Alters the screen resolution.
|
| 'systemFocus' [boolean]
|   Alters the current system focus information.
|   For example the caret is hidden if not having system focus.
|
| 'userCreds' [value]
|   Alters current user.
|
| 'windows' [value]
|   Alters what windows to show
|
| 'visitor' [value]
|   Alters current users visitors alias.
|   If the user logs in and out again their previos visitor value is reclaimed.
*/
def.static.alter =
	function( ...args )
{
	let action             = root.action;
	let clearRetainX;
	let colorScheme        = root.colorScheme;
	let hover              = root.hover;
	let mark               = root.mark;
	let mode               = root.mode;
	let ratioSplit         = root.ratioSplit;
	let resolution         = root.resolution;
	let seam               = root.seam;
	let systemPrefersDark  = root.systemPrefersDark;
	let sizeDisplay        = root.sizeDisplay;
	let systemFocus        = root.systemFocus;
	let userCreds          = root.userCreds;
	let visitor            = root.visitor;
	let windows            = root.windows;
	const markOld = mark;

	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		const command = args[ a ];
		const arg = args[ a + 1 ];

		if( arg === pass ) continue;

		switch( command )
		{
			case 'action':            action            = arg; continue;
			case 'clearRetainX':      clearRetainX      = arg; continue;
			case 'colorScheme':       colorScheme       = arg; continue;
			case 'hover':             hover             = arg; continue;
			case 'mark':              mark              = arg; continue;
			case 'mode':              mode              = arg; continue;
			case 'ratioSplit':        ratioSplit        = arg; continue;
			case 'resolution':        resolution        = arg; continue;
			case 'systemPrefersDark': systemPrefersDark = arg; continue;
			case 'systemFocus':       systemFocus       = arg; continue;
			case 'userCreds':         userCreds         = arg; continue;
			case 'windows':           windows           = arg; continue;
			case 'sizeDisplay':       sizeDisplay       = arg; continue;
			case 'visitor':           visitor           = arg; continue;

			// a trace
			default:
			{
				const base = command.get( 1 );
				switch( base.name )
				{
					case 'seam':
					{
						seam = seam.alter( command, arg );
						break;
					}

					case 'windows':
					{
						const atWindow = command.get( 1 ).at;
						let window = windows.get( atWindow );
						window = command.chop.graft( window, arg );
						windows = windows.set( atWindow, window );
						break;
					}

					default: throw new Error( );
				}
			}
		}
	}

	if( clearRetainX && mark?.retainX !== undefined )
	{
		mark = mark.create( 'retainX', undefined );
	}

	root =
		root.create(
			'action',            action,
			'colorScheme',       colorScheme,
			'mark',              mark,
			'mode',              mode,
			'hover',             hover,
			'ratioSplit',        ratioSplit,
			'resolution',        resolution,
			'seam',              seam,
			'sizeDisplay',       sizeDisplay,
			'systemPrefersDark', systemPrefersDark,
			'systemFocus',       systemFocus,
			'userCreds',         userCreds,
			'visitor',           visitor,
			'windows',           windows,
		);

	// if the mark changed, empty labels are checked
	// if they lost their mark and if so are removed.
	if( markOld && markOld !== mark )
	{
		root.screen.checkEmptyLabels( markOld );
	}

	// possible changes the "seam" tree to fit scrollbars.
	root.screen.rectify( );
};

/*
| Does an animation frame.
|
| ~time: time stamp the animation frame has been fired for
*/
def.static.animationFrame =
	function( time )
{
	root.animation.frame( time, root.action );
};

/*
| Checks with server if user creds are valid.
|
| ~userCreds: user credentials to check.
*/
def.static.auth =
	function( userCreds )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/}

	TransactionAuth.start( userCreds );
};

/*
| Applies (fabric) changes.
|
| ...args:
|	possibly multiple occurances of
|		uidSpace: the uid of the fabric to change
|       ['noTrack']: optionally do not undo/redo track this change
|       changeX: change or list@change
*/
def.static.change =
	function( ...args )
{
	let uidSpace;
	let noTrack;
	let xChange;

	for( let a = 0, alen = args.length; a < alen; a++ )
	{
		uidSpace = args[ a++ ];
		if( args[ a ] === 'noTrack' )
		{
			noTrack = true;
			a++;
		}
		xChange = args[ a ];

		// TODO, allow
		if( a + 1 < alen ) throw new Error( );
	}

	const wrap = DynamicManager.changeSpace( uidSpace, xChange );
	root = root.create( 'ancillaries', DynamicManager.ancillaries( ) );

	let mark = root.mark;
	if( mark )
	{
		mark = root.screen.transformMark( mark, wrap );
	}

	if( !noTrack )
	{
		ReUndo.track( uidSpace, wrap.changes );
	}

	if( mark !== root.mark )
	{
		root = root.create( 'mark', mark );
	}

	// the doc path of a doc view has been deleted?
	// -> close the view
	let windows = root.windows;
	let windowRight = windows.length > 1 && windows.get( 1 );

	const aSpace = root.ancillaries.get( uidSpace );
	if(
		windowRight
		&& windowRight.ti2ctype === WindowDocView
		&& !aSpace.items.get( windowRight.traceBase.forward( 'items' ).last.key )
	)
	{
		windows = windows.remove( 1 );
		root = root.create( 'windows', windows );
	}

	/*
	{
		// if the mark changed, empty labels are checked
		// if they lost their mark and if so are removed.
		if( markOld && markOld !== mark )
		{
			root.screen.checkEmptyLabels( markOld );
		}
	}
	*/

	// possibly changes the "seam" tree to fit scrollbars.
	root.screen.rectify( );

	DynamicManager.sendChanges( );
};

/*
| Sends a change password request.
*/
def.static.changePwd =
	function( passhashNew )
{
	TransactionChangePwd.start( root.userCreds, passhashNew );
};

/*
| Sends the change news opt-in request.
*/
def.static.changeUserNewsletter =
	function( newsletter )
{
	let userInfo = root.userInfo;
	userInfo = userInfo.create( 'newsletter', newsletter );
	root = root.create( 'userInfo', userInfo );

	TransactionChangeUserNews.start( root.userCreds, newsletter );
};

/*
| Draws everything.
*/
//let lastDrawnRoot;
def.static.draw =
	function( )
{
//	TODO disabled due to black screen issue on mobile
//	if( lastDrawnRoot === this ) return;
//	lastDrawnRoot = this;

	const glint = root.screen.glint;
	const background =
		root.light
		? Color.RGB( 251, 251, 251 )
		: Color.RGB(  50,  50,  50 );

	_display =
		_display.create(
			'background', background,
			'glint', glint,
		);

/*** assisting automatic testing */
/**/if( PUPPET )
/**/{
/**/	PUPPET( { event: 'draw', glint: glint.jsonfy( ) } );
/**/}

	_display.render( );
};

/*
| Changes the space transform so p stays in the same spot on screen.
|
| ~atWindow: which window to change
| ~de:       difference of view zoom exponent
| ~p:        point to keep constant
|
| new offset (ox1, oy1) calculates as:
|
| A: py = y * z0 + oy0
| B: py = y * z1 + oy1
|
| A: py / z0 = y + oy0 / z0
| B: py / z1 = y + oy1 / z1
|
| A - B: py / z0 - py / z1 = oy0 / z0 - oy1 / z1
|
| -> py * ( 1 / z0 - 1 / z1  ) = oy0 / z0 - oy1 / z1
| -> oy1 / z1 = oy0 / z0 - py * ( 1 / z0 - 1 / z1  )
| -> oy1 = z1 * ( oy0 / z0 - py * ( 1 / z0 - 1 / z1  ) )
*/
def.static.changeSpaceTransformPoint =
	function( atWindow, de, p )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( de ) !== 'number' ) throw new Error( );
/**/	if( p.ti2ctype !== Point ) throw new Error( );
/**/}

	const window0 = root.windows.get( atWindow );
	const transform = window0.transform;
	const offset = transform.offset;
	const e1 = EMath.limit( config.zoomMin, window0.exponent + de, config.zoomMax );
	const scale = Math.pow( 1.1, e1 ) * root.resolution.ratio;
	const h = 1 / transform.scale - 1 / scale;

	Self.changeTransformTo(
		atWindow,
		e1,
		transform.setScaleOffset(
			scale,
			Point.XY(
				( offset.x / transform.scale - p.x * h ) * scale,
				( offset.y / transform.scale - p.y * h ) * scale
			),
		),
		40, // zoomStepTime
	);
};

/*
| Changes the zoom factor keeping current center.
|
| ~atWindow: which window to change
| ~dir:      direction of zoom change (+/- 1)
*/
def.static.changeSpaceTransformCenter =
	function( atWindow, dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( dir !== -1 && dir !== 1 ) throw new Error( );
/**/}

	root.screen.changeSpaceTransformCenter( atWindow, dir );
};

/*
| Changed the view so that all items of current space are visible.
*/
def.static.changeSpaceTransformAll =
	function( atWindow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/}

	root.screen.changeSpaceTransformAll( atWindow );
};

/*
| Changed the views zoom to 1 and pans to home.
*/
def.static.changeSpaceTransformHome =
	function( atWindow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/}

	root.screen.changeSpaceTransformHome( atWindow );
};

/*
| Changes the space transform to transform with an animation.
|
| ~atWindow:  which window to change
| ~exp:       exponent of destination transform
| ~transform: destination transform
| ~time:      time of animation
*/
def.static.changeTransformTo =
	function( atWindow, exp, transform, time )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( typeof( exp ) !== 'number' ) throw new Error( );
/**/}


	const windows = root.windows;
	const win = windows.get( atWindow );

	root =
		root.create(
			'animation',
			root.animation.create(
				'twig:set+', 'transform',
				AnimationTransform.Now( atWindow, win.transform, transform, time ),
			),

			'windows',
			windows.set( atWindow, win.create( 'exponent', exp ) ),
		);

	System.doAnimation( );
};

/*
| User clicked.
*/
def.static.click =
	function( p, shift, ctrl )
{
	return root.screen.click( p, shift, ctrl );
};

/*
| Closes the window #atWindow.
*/
def.static.closeWindow =
	function( atWindow )
{
	let windows = root.windows.remove( atWindow );

	if( windows.length > 0 )
	{
		root = root.create( 'windows', windows );
	}
	else
	{
		Self.openSpace( 0, RefSpace.PlotleHome );
	}
};

/*
| Cycles focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
*/
def.static.cycleFocus =
	function( dir )
{
	root.screen.cycleFocus( dir );
};

/*
| Deletes the current space from the server.
|
| ~uidSpace:     unique id of space to delete
| ~confirmation: confirmation (referencing the space)
*/
def.static.deleteSpace =
	function( uidSpace, confirmation )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( uidSpace ) !== 'string' ) throw new Error( );
/**/	if( uidSpace.indexOf( ':' ) >= 0 ) throw new Error( );
/**/	if( typeof( confirmation ) !== 'string' ) throw new Error( );
/**/}

	TransactionDeleteSpace.start( root.userCreds, uidSpace, confirmation );
};

/*
| Moving during an operation with the mouse button held down.
*/
def.static.dragMove =
	function( p, shift, ctrl )
{
	root.screen.dragMove( p, shift, ctrl );
};

/*
| Starts an operation with the pointing device active.
|
| Mouse down or finger on screen.
*/
def.static.dragStart =
	function( p, shift, ctrl )
{
	root.screen.dragStart( p, shift, ctrl );
};

/*
| A button has been drag-started.
*/
def.static.dragStartButton =
	function( trace )
{
	return root.screen.dragStartButton( trace, false, false );
};

/*
| Stops an operation with the mouse button held down.
*/
def.static.dragStop =
	function( p, shift, ctrl )
{
	root.screen.dragStop( p, shift, ctrl );
};

/*
| Expands a relative link (text in subspace) to a new ref.
|
| ~atWindow: window the link is relative to.
| ~link:     the relative link
*/
def.static.expandRefLink =
	function( atWindow, link )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( typeof( link ) !== 'string' ) throw new Error( );
/**/}

	return RefSpace.Link( link, root.windows.get( atWindow ).ref );
};

/*
| Exports current doc path.
'*/
def.static.exportDocPath =
	function( )
{
	root.screen.exportDocPath( );
};

/*
| Finished an animation.
|
| ~name: animation name
*/
def.static.finishAnimation =
	function( name )
{
	if( !root.animation.get( name ) )
	{
		return;
	}

	root =
		root.create(
			'animation', root.animation.create( 'twig:remove', name )
		);

	if( root.animation.length === 0 )
	{
		System.stopAnimation( );
	}
};

/*
| Returns the what the clipboard should hold.
*/
def.static.getClipboard =
	function( )
{
	return root.screen.clipboard;
};

/*
| Returns the system mode.
|
| This is the vertical offset of the caret.
| Used for on mobile so the caret is scrolled into view when the keyboard is visible.
*/
def.static.getSysMode =
	function( )
{
	return root.screen.sysMode;
};

/*
| Hides the doc view.
*/
def.static.hideDocView =
	function( keyBase )
{
	root = root.create( 'windows', root.windows.remove( 1 ) );
};

/*
| User entered character(s).
*/
def.static.input =
	function( string )
{
	root.screen.input( string );
	root.screen.scrollMarkIntoView( );
};

/*
| Logs out the current user.
*/
def.static.logout =
	function( )
{
	// clears the user spaces list
	UserCreds.clearLocalStorage( );
	const visitor = root.visitor;

	if( visitor )
	{
		DynamicManager.setUser( visitor, undefined );
		root =
			root.create(
				'userCreds', visitor,
				'userInfo', undefined,
			);
		Self.openSpace( 0, RefSpace.PlotleHome, 'addHistory', 'closeOther' );
	}
	else
	{
		DynamicManager.setUser( undefined, undefined );
		root = root.create( 'userInfo', undefined );
		Self.auth( UserCreds.NewVisitor( ) );
	}
};

/*
| Mouse wheel is being turned.
*/
def.static.mousewheel =
	function( p, dir, shift, ctrl )
{
	root.screen.mousewheel( p, dir, shift, ctrl );
};

/*
| A network transaction replied.
*/
def.static.onTransactionReply =
	function( transaction, reply )
{
	switch( transaction.ti2ctype )
	{
		case TransactionAlter:
			Self._onAlter( transaction, reply );
			break;

		case TransactionAuth:
			Self._onAuth( transaction, reply );
			break;

		case TransactionChangePwd:
			Self._onChangePwd( transaction, reply );
			break;

		case TransactionChangeUserNews:
			Self._onChangeUserNews( transaction, reply );
			break;

		case TransactionDeleteSpace:
			break;

		case TransactionSignUp:
			Self._onSignUp( transaction, reply );
			break;

		case TransactionSpaceAcquire:
			Self._onSpaceAcquire( transaction, reply );
			break;

		case TransactionUpdate:
		{
			DynamicManager.onUpdate( transaction, reply );
			const userInfo = DynamicManager.userInfo( );
			root =
				root.create(
					'ancillaries', DynamicManager.ancillaries( ),
					'userInfo',    userInfo,
				);
			break;
		}

		default: throw new Error( );
	}
};

/*
| Opens a space.
|
| ~atWindow:          opens the space at this window number.
| ~ref:               RefSpace reference to space to be opened
| ~args:
|    'addHistory':    add the spaces href to browser history
|    'closeOther':    closes all other windows
|    'createMissing': create space if not existing
*/
def.static.openSpace =
	function( atWindow, ref, ...args )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length < 2 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( ref.ti2ctype !== RefSpace ) throw new Error( );
/**/}

	let addHistory = false;
	let closeOther = false;
	let createMissing = false;

	for( let arg of args )
	{
		switch( arg )
		{
			case 'addHistory':    addHistory = true;    break;
			case 'closeOther':    closeOther = true;    break;
			case 'createMissing': createMissing = true; break;
			default: throw new Error( );
		}
	}

	if( addHistory )
	{
		const refLoc = RefLocation.Path( window.location.pathname );

		window.history.pushState(
			{ }, '',
			'/sp/'
			+ ref.fullname
			+ '/'
			+ ( refLoc?.devel ? 'devel.html' : '' )
		);
	}

	let windows = root.windows;
	let refFallback;
	let win;

	if( !closeOther )
	{
		// closes any docViews
		// in case all other windows are closed anyway no need to work through this

		for( let w = 0, wlen = windows.length; w < wlen; w++ )
		{
			win = windows.get( w );
			if( win.ti2ctype === WindowDocView )
			{
				windows = windows.remove( w );
				w--;
				wlen--;
			}
		}
	}

	if( atWindow < windows.length )
	{
		win = windows.get( atWindow );
		switch( win.ti2ctype )
		{
			case WindowLoading: refFallback = win.refFallback; break;
			case WindowSpace:   refFallback = win.ref;         break;
		}
	}

	win =
		WindowLoading.create(
			'ref',         ref,
			'refFallback', refFallback,
		);

	if( closeOther )
	{
		windows = ListWindow.Elements( win );
	}
	else
	{
		windows =
			atWindow < windows.length
			? windows.set( atWindow, win )
			: windows.append( win );
	}

	root =
		root.create(
			'mode', ModeNormal.singleton,
			'windows', windows,
		);

	if( ref )
	{
		if( _timerDelayedUpdate !== undefined )
		{
			System.cancelTimer( _timerDelayedUpdate );
			_timerDelayedUpdate = undefined;
		}

		DynamicManager.acquireSpace( ref, createMissing );
	}
};

/*
| Pop-state event.
*/
def.static.popstate =
	function( pathname )
{
	const refLoc = RefLocation.Path( pathname );

	if( refLoc )
	{
		Self.openSpace( 0, refLoc.ref );
	}
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.static.paste =
	function( type, data )
{
	root.screen.paste( type, data );
};

/*
| User is hovering his/her pointing device.
*/
def.static.pointingHover =
	function( p, shift, ctrl )
{
	const result = root.screen.pointingHover( p, shift, ctrl );

	if( result )
	{
		root = root.create( 'hover', result.traceV );
		return result.cursor;
	}
	else
	{
		return undefined;
	}
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.static.probeClickDrag =
	function( p, shift, ctrl )
{
	return root.screen.probeClickDrag( p, shift, ctrl );
};

/*
| A button has been pushed.
| TODO actually have the button directly deliver the event to its owner.
|
| ~traceV: of the button pushed
*/
def.static.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	root.screen.pushButton( traceV, shift, ctrl );
};

/*
| User is releasing a special key.
*/
def.static.releaseSpecialKey =
	function( key, shift, ctrl )
{
	if( key !== 'shift' ) return;

	const mode = root.mode;
	if( mode.ti2ctype === ModeSelect && root.action === undefined )
	{
		root = root.create( 'mode', mode.fallbackMode );
	}
};

/*
| Removes these items.
|
| All items have to be in same space.
*/
def.static.removeItems =
	function( traceVs )
{
/**/if( CHECK )
/**/{
/**/	if( traceVs.ti2ctype !== SetTrace ) throw new Error( );
/**/}

	const changes = [ ];
	const ranks = [ ];

	let atWindow, fSpace, uid;
	for( let traceV of traceVs )
	{
		atWindow = traceV.forward( 'view' ).last.at;
		uid = root.windows.get( atWindow ).uid;
		const aSpace = root.ancillaries.get( uid );
		fSpace = aSpace.asFabric;
		break;
	}

	for( let traceV of traceVs )
	{
/**/	if( CHECK )
/**/	{
/**/		if( traceV.forward( 'view' ).last.at !== atWindow ) throw new Error( );
/**/	}

		const traceF = root.screen.traceV2F( traceV );
		const rank = fSpace.items.rankOf( traceV.last.key );

		let rc = 0;
		for( let r of ranks )
		{
			if( r <= rank ) rc++;
		}
		ranks.push( rank );

		changes.push(
			ChangeTreeShrink.create(
				'trace', traceF,
				'prev',  traceF.pick( fSpace ),
				'rank',  rank - rc,
			)
		);
	}

	Self.change( uid, Sequence.Array( changes ) );
};

/*
| The window has been resized.
|
| ~size: display (changed?) size
| ~resolution: display (changed?) resolution
*/
def.static.resize =
	function( size, resolution )
{
/**/if( CHECK )
/**/{
/**/	if( size.ti2ctype !== Size ) throw new Error( );
/**/	if( resolution.ti2ctype !== Resolution ) throw new Error( );
/**/}

	if( root.resolution.devicePixelRatio !== resolution.devicePixelRatio )
	{
		console.log( 'devicePixelRatio changed: ' + resolution.devicePixelRatio );
	}

	let windows = root.windows;
	for( let aw = 0, lena = windows.length; aw < lena; aw++ )
	{
		const win = windows.get( aw );
		if( win.ti2ctype !== WindowSpace ) continue;

		const e1 = EMath.limit( config.zoomMin, win.exponent, config.zoomMax );
		const scale = Math.pow( 1.1, e1 ) * resolution.ratio;
		const ts = TransformNormal.setScaleOffset( scale, win.transform.offset );
		windows = windows.set( aw, win.create( 'transform', ts ) );
	}

	_display = _display.resize( size, resolution );

	root =
		root.create(
			'resolution', resolution,
			'sizeDisplay', _display.size,
			'windows',    windows,
		);
};

/*
| Shows a doc path in the doc view.
|
| ~atWindow:  window # for the docView base.
| ~keyBase:   key of the docView base.
*/
def.static.showDocView =
	function( atWindow, keyBase )
{
	let windows = root.windows;

	if( atWindow === 0 )
	{
		const wdv =
			WindowDocView.create(
				'traceBase',
					TraceRoot
					.space( windows.get( 0 ).uid )
					.add( 'items', keyBase ),
			);

		windows = windows.set( 1, wdv );
	}
	else
	{
/**/	if( CHECK && atWindow !== 1 ) throw new Error( );

		const wdv =
			WindowDocView.create(
				'traceBase',
					TraceRoot
					.space( windows.get( 1 ).uid )
					.add( 'items', keyBase ),
			);

		windows = windows.set( 0, windows.get( 1 ) );
		windows = windows.set( 1, wdv );
	}

	root = root.create( 'windows', windows );
};

/*
| Sets the color scheme override.
*/
def.static.setColorScheme =
	function( colorScheme )
{
	switch( colorScheme )
	{
		case 'system':
			window.localStorage.removeItem( 'colorScheme' );
			break;

		case 'light':
		case 'dark':
			window.localStorage.setItem( 'colorScheme', colorScheme );
			break;

		default: throw new Error( );
	}

	root = root.create( 'colorScheme', colorScheme );
};

/*
| Shows the "home" screen.
|
| When a space is loaded, this is space/normal
| otherwise it is the loading screen.
*/
def.static.setModeNormal =
	function( )
{
	root =
		root.create(
			'action', undefined,
			'mode', ModeNormal.singleton,
		);
};

/*
| Shows snapping grids.
*/
def.static.setShowGrid =
	function( showGrid )
{
	window.localStorage.setItem( 'showGrid', showGrid );
	root = root.create( 'showGrid', showGrid );
};

/*
| Shows selection guides.
*/
def.static.setShowGuides =
	function( showGuides )
{
	window.localStorage.setItem( 'showGuides', showGuides );
	root = root.create( 'showGuides', showGuides );
};

/*
| Sets snapping.
*/
def.static.setSnapping =
	function( snapping )
{
	window.localStorage.setItem( 'snapping', snapping );
	root = root.create( 'snapping', snapping );
};

/*
| Sets the transform of a window
*/
def.static.setWindowTransform =
	function( iWin, transform )
{
	let windows = root.windows;
	windows =
		windows.set(
			iWin,
			windows.get( iWin ).create( 'transform', transform )
		);
	root = root.create( 'windows', windows );
};

/*
| Sets if the shell got the system focus
| (that is display the virtual caret)
*/
def.static.setSystemFocus =
	function( focus )
{
	if( root.systemFocus === focus ) return;

	root = root.create( 'systemFocus', focus );
};

/*
| A space has been deletted.
|
| ~uid: uid of deleted space.
*/
def.static.spaceDeleted =
	function( uid )
{
	console.log( 'space "' + uid + '" deleted' );
	let windows = root.windows;
	let found;

	for( let w = 0, wlen = windows.length; w < wlen; w++ )
	{
		const win = windows.get( w );
		if( win.ti2ctype === WindowSpace && win.uid === uid )
		{
			found = true;
			windows = windows.set( w, WindowDeleted.create( 'ref', win.ref ) );
		}
	}

	if( found )
	{
		root =
			root.create(
				'hover',  undefined,
				'mark',   undefined,
				'mode',   ModeNormal.singleton,
				'windows', windows,
			);
	}
};

/*
| User is pressing a special key.
|
| ~key: key pressed (string representing the key)
| ~shift: true if shift was pressed down.
| ~ctrl: true if ctrl or meta was pressed down.
| ~inputWord: the last word (on mobile)
*/
def.static.specialKey =
	function( key, shift, ctrl, inputWord )
{
	if( key === 'shift' )
	{
		const mode = root.mode;

		switch( mode.ti2ctype )
		{
			case ModeNormal:
				root = root.create( 'mode', ModeSelect.create( 'fallbackMode', mode ) );
				return;
		}
	}

	root.screen.specialKey( key, shift, ctrl, inputWord );
	root.screen.scrollMarkIntoView( );
	return;
};

/*
| Startup of shell.
*/
def.static.startup =
	function( display, systemPrefersDark )
{
/**/if( CHECK && root ) throw new Error( );

	_display = display;

	const seam = SeamRoot.Clean;

	let userCreds = UserCreds.createFromLocalStorage( );
	if( !userCreds )
	{
		userCreds = UserCreds.NewVisitor( );
	}

	const resolution = _display.resolution;

	let colorScheme = window.localStorage.getItem( 'colorScheme' );
	if( colorScheme !== 'light' && colorScheme !== 'dark' )
	{
		colorScheme = 'system';
	}

	const showGrid = window.localStorage.getItem( 'showGrid' ) !== 'false';
	const showGuides = window.localStorage.getItem( 'showGuides' ) !== 'false';
	const snapping = window.localStorage.getItem( 'snapping' ) !== 'false';

	const windows =
		ListWindow.Elements(
			WindowLoading.create( 'ref', RefSpace.PlotleHome )
		);

	DynamicManager.init( );

	root =
		Root.create(
			'ancillaries',       GroupASpace.Empty,
			'animation',         Animation.create( ),
			'colorScheme',       colorScheme,
			'mode',              ModeNormal.singleton,
			'ratioSplit',        0.5,
			'resolution',        resolution,
			'seam',              seam,
			'sizeDisplay',       _display.size,
			'showGrid',          showGrid,
			'showGuides',        showGuides,
			'snapping',          snapping,
			'systemFocus',       true,
			'systemPrefersDark', systemPrefersDark,
			'windows',           windows,
		);

	Self.auth( userCreds );
};

/*
| A checkbox has been toggled.
|
| ~trace: trace of the checkbox
*/
def.static.toggleCheckbox =
	function( trace )
{
	root.screen.toggleCheckbox( trace, false, false );
};

/*
| Updates marks from a network change report.
*/
def.static.updateMarks =
	function( report )
{

	let mark = root.mark;
	if( mark )
	{
		mark = root.screen.transformMark( mark, report );
		root = root.create( 'mark', mark );
	}
};

/*
| A select widget changed.
|
| ~traceV: of the select widget pushed
| ~value:  value to change it to.
*/
def.static.widgetSelectChanged =
	function( traceV, value )
{
	root.screen.widgetSelectChanged( traceV, value );
};

/*
| Received a reply of an alter transaction.
*/
def.static._onAlter =
	function( transaction, reply )
{
	if( reply.ti2ctype === ReplyError )
	{
		System.failScreen( reply.message );
		return;
	}

	const results = reply.results;
	for( let message of results )
	{
		if( message !== 'ok' )
		{
			System.failScreen( 'Server not OK: ' + message );
		}
	}
};

/*
| An 'Auth' transaction replied.
|
| TODO unify reply error handling into onTransactionReply.
*/
def.static._onAuth =
	function( transaction, reply )
{
	if( reply.ti2ctype === ReplyAuth )
	{
		const userCreds = reply.userCreds;
		DynamicManager.setUser( userCreds, reply.userInfo );

		const userInfo = DynamicManager.userInfo( );
		root = root.create( 'userInfo', userInfo );
	}

	// if in login form this is an attempted login
	const mode = root.mode;
	if( mode.ti2ctype === ModeForm && mode.formName === 'login' )
	{
		root.screen.form.onAuth( reply );
		return;
	}

	const request = transaction.request;

	// otherwise this is an onload login or logout.
	if( reply.ti2ctype !== ReplyAuth )
	{
		// when logging in with a real user failed
		// take a visitor instead
		if( !request.userCreds.isVisitor )
		{
			Self.auth( UserCreds.NewVisitor( ) );
			return;
		}

		// if even that failed, bail to failScreen
		System.failScreen( reply.message );
		return;
	}

	const userCreds = reply.userCreds;

	root =
		root.create(
			'userCreds', userCreds,
			'visitor',   userCreds.isVisitor ? userCreds : pass,
		);

	if( userCreds.isVisitor )
	{
		UserCreds.clearLocalStorage( );
	}

	const pathname = decodeURI( window.location.pathname );
	const parts = pathname.split( '/' );

	// default to plotle home if pathname parsing fails
	let ref = RefSpace.PlotleHome;

	let len = parts.length;
	if( len >= 3 && parts[ 1 ] === 'sp' )
	{
		switch( parts[ len - 1 ] )
		{
			case 'index.html':
			case 'devel.html':
			case '':
				len--;
				break;
		}
		const link = parts.slice( 2, len ).join( '/' );
		ref = RefSpace.Link( link );
	}

	Self.openSpace( 0, ref );
};

/*
| Received a 'changePwd' reply.
*/
def.static._onChangePwd =
	function( transaction, reply )
{
	if( reply.ti2ctype === ReplyError )
	{
		System.failScreen( 'Error on password change: ' + reply.message );
		return;
	}

	const mode = root.mode;
	if( mode && mode.ti2ctype === ModeForm && mode.formName === 'changePwd' )
	{
		root.screen.form.onChangePwd( transaction.request, reply );
	}

	// changes the new passhash to be used by this client
	const userCreds = root.userCreds.create( 'passhash', transaction.request.passhashNew );
	userCreds.saveToLocalStorage( );
	root = root.create( 'userCreds', userCreds );
};

/*
| Received a reply of a change user newsletter opt-in request.
*/
def.static._onChangeUserNews =
	function( transaction, reply )
{
	if( reply.ti2ctype === ReplyError )
	{
		System.failScreen( 'Server not OK: ' + reply.message );
		return;
	}
};

/*
| Received a 'signUp' reply.
*/
def.static._onSignUp =
	function( transaction, reply )
{
	const mode = root.mode;

	if( mode && mode.ti2ctype === ModeForm && mode.formName === 'signUp' )
	{
		root.screen.form.onSignUp( transaction.request, reply );
	}

	if( reply.ti2ctype === ReplySignUp )
	{
		// errors are handled by the signup form
		const userCreds = transaction.request.userCreds;
		const userInfo = reply.userInfo;

		DynamicManager.setUser( userCreds, userInfo ),
		root =
			root.create(
				'userCreds', userCreds,
				'userInfo', DynamicManager.userInfo( ),
			);

		Self.openSpace( 0, RefSpace.PlotleHome, 'addHistory' );
		root = root.create( 'mode', ModeForm.welcome );
	}

/**/if( PUPPET )
/**/{
/**/	PUPPET( { event: 'onSignUp' } );
/**/}
};

/*
| A space has been acquired.
*/
def.static._onSpaceAcquire =
	function( transaction, reply )
{
	const request = transaction.request;

	switch( reply.status )
	{
		case 'nonexistent':
		{
			let windows = root.windows;

			// finds which windows tried to load that space
			// and replaces them with a WindowNonExisting
			for( let w = 0, wlen = windows.length; w < wlen; w++ )
			{
				const win = windows.get( w );
				if( win.ti2ctype === WindowLoading && win.ref === request.refSpace )
				{
					windows =
						windows.set(
							w,
							WindowNonExisting.create(
								'ref',         win.ref,
								'refFallback', win.refFallback,
							)
						);
				}
			}

			root = root.create( 'windows', windows );
			return;
		}

		case 'no access':
		{
			let windows = root.windows;

			// finds which windows tried to load that space
			// and replaces them with a WindowNoAccess
			for( let w = 0, wlen = windows.length; w < wlen; w++ )
			{
				const win = windows.get( w );
				if( win.ti2ctype === WindowLoading && win.ref === request.refSpace )
				{
					windows =
						windows.set(
							w,
							WindowNoAccess.create(
								'ref',         win.ref,
								'refFallback', win.refFallback,
							)
						);
				}
			}

			root = root.create( 'windows', windows );
			return;
		}

		case 'served': break;

		default: throw new Error( );
	}

	// TODO wrong
	ReUndo.flush( );

	const access = reply.access;
	const owner = reply.owner;
	const uid = reply.space.moment.uid;

	let mode = root.mode;
	if( mode.ti2ctype === ModeForm && mode.formName === 'loading' )
	{
		mode = ModeNormal.singleton;
	}

	const transform = TransformNormal.setScaleOffset( root.resolution.ratio, Point.zero );
	DynamicManager.onSpaceAcquire( reply.space );

	let windows = root.windows;
	for( let w = 0, wlen = windows.length; w < wlen; w++ )
	{
		const win = windows.get( w );
		if( win.ti2ctype === WindowLoading && win.ref === request.refSpace )
		{
			windows =
				windows.set(
					w,
					WindowSpace.create(
						'access',    access,
						'exponent',  0,
						'isOwner',   owner,
						'ref',       request.refSpace,
						'transform', transform,
						'uid',       uid,
					)
				);
		}
	}

	root =
		root.create(
			'ancillaries', DynamicManager.ancillaries( ),
			'mark', undefined,
			'mode', mode,
			'windows', windows,
		);

	// waits a bit before going into update cycle,
	// so safari stops its wheely thing.
	_timerDelayedUpdate =
		System.setTimer(
			500,
			( ) =>
			{
				_timerDelayedUpdate = undefined;
				DynamicManager.sendUpdate( );
			}
		);

/**/if( PUPPET )
/**/{
/**/	PUPPET( { event: 'acquire space', uid: uid } );
/**/}
};
