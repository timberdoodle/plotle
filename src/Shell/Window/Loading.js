/*
| Window for a space while trying to load it.
*/
def.attributes =
{
	// reference of the space to be loaded
	ref: { type: 'Shared/Ref/Space' },

	// reference of the space to be loaded as fallback.
	// if undefined, the window will be closed.
	refFallback: { type: [ 'undefined', 'Shared/Ref/Space'] },
};
