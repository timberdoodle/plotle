/*
| Window is showing a doc view
*/
def.attributes =
{
	// trace to the docview base
	traceBase: { type: 'ti2c:Trace' },
};
