/*
| Window is showing a deleted space.
*/
def.attributes =
{
	// reference of the space
	ref: { type: 'Shared/Ref/Space' },
};
