/*
| Window for user having tried to open a non-existing space.
*/
def.attributes =
{
	// reference of the space not existing.
	ref: { type: 'Shared/Ref/Space' },

	// reference of the space to be loaded as fallback.
	// if undefined, the window will be closed.
	refFallback: { type: [ 'undefined', 'Shared/Ref/Space'] },
};
