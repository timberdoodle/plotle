/*
| A re/undo entry.
*/
def.attributes =
{
	// changes
	changes: { type: 'ot:Change/Sequence', json: true },

	// uid of the space
	uidSpace: { type: 'string', json: true },
};

/*
| Creates a reversed tracker entry.
*/
def.lazy.reversed =
	function( )
{
	const re = this.create( 'changes', this.changes.reversed );
	ti2c.aheadValue( re, 'reversed', this );
	return re;
};
