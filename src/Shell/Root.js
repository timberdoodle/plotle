/*
| The root object of the root.
*/
def.attributes =
{
	// current action
	action: { type: [ 'undefined', '<Shell/Action/Types' ] },

	// current enriched spaces data
	ancillaries: { type: 'group@Shared/Ancillary/Space' },

	// the animations
	animation: { type: 'Shell/Animation/Self' },

	// user's color scheme override
	// "system", "light" or "dark"
	colorScheme: { type: 'string' },

	// the widget hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// current mode
	mode: { type: [ 'undefined', '<Shell/Mode/Types' ] },

	// alters the ratio at which the split screen resides.
	// [0.0 - 1.0]
	ratioSplit: { type: 'number' },

	// display resolution
	resolution: { type: [ 'undefined', 'gleam:Display/Canvas/Resolution' ] },

	// additional data not saved
	seam: { type: 'Shell/Seam/Root' },

	// show space grid
	showGrid: { type: 'boolean' },

	// show space guides
	showGuides: { type: 'boolean' },

	// size of the display
	sizeDisplay: { type: 'gleam:Size' },

	// space has snapping
	snapping: { type: 'boolean' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// true if the system prefers dark mode
	systemPrefersDark: { type: 'boolean' },

	// the visual trace of the space view
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },

	// currently logged in user
	userCreds: { type: [ 'undefined', 'Shared/User/Creds' ] },

	// list of spaces belonging to user
	userInfo: { type: [ 'undefined', 'Shared/User/Info' ] },

	// remembers an acquired visitor user name and
	// passhash so when logging out from a real user
	// the previous visitor id is regained.
	// last acquired visitor credentials
	visitor: { type: [ 'undefined', 'Shared/User/Creds' ] },

	// the windows to be shown
	windows: { type: 'list@<Shell/Window/Types' },
};

import { Self as ModeForm                  } from '{Shell/Mode/Form}';
import { Self as ModeNormal                } from '{Shell/Mode/Normal}';
import { Self as ModeSelect                } from '{Shell/Mode/Select}';
import { Self as ModeZoom                  } from '{Shell/Mode/Zoom}';
import { Self as ScreenForm                } from '{Shell/Screen/Form}';
import { Self as ScreenSpace               } from '{Shell/Screen/Space}';
import { Self as ScreenSplit               } from '{Shell/Screen/Split}';
import { Self as ScreenStart               } from '{Shell/Screen/Start}';
import { Self as WindowSpace               } from '{Shell/Window/Space}';

/*
| True if light color scheme.
*/
def.lazy.light =
	function( )
{
	switch( this.colorScheme )
	{
		case 'system': return !this.systemPrefersDark;
		case 'light':  return true;
		case 'dark':   return false;
		default: throw new Error( );
	}
};

/*
| Builds the current screen.
*/
def.lazy.screen =
	function( )
{
	const userCreds = this.userCreds;
	const mark = this.mark;
	const mode = this.mode;
	const seam = this.seam;
	const windows = this.windows;

	if( !userCreds )
	{
		return(
			ScreenStart.create(
				'light',       this.light,
				'resolution',  this.resolution,
				'sizeDisplay', this.sizeDisplay,
				'systemFocus', this.systemFocus,
				'userCreds',   userCreds,
				'windows',     windows,
			)
		);
	}

	const ancillaries = this.ancillaries;

	switch( mode.ti2ctype )
	{
		case ModeNormal:
		case ModeSelect:
		case ModeZoom:
		{
			if( windows.length > 1 )
			{
				return(
					ScreenSplit
					.create(
						'action',      this.action,
						'ancillaries', ancillaries,
						'docViewSeam', seam.docView,
						'hover',       this.hover,
						'light',       this.light,
						'mark',        ScreenSplit.filterMark( mark ),
						'mode',        mode,
						'ratioSplit',  this.ratioSplit,
						'resolution',  this.resolution,
						'showGrid',    this.showGrid,
						'showGuides',  this.showGuides,
						'snapping',    this.snapping,
						'seamSpace',   seam.space,
						'sizeDisplay', this.sizeDisplay,
						'systemFocus', this.systemFocus,
						'userCreds',   userCreds,
						'windows',     windows,
					)
				);
			}
			else
			{
				return(
					ScreenSpace
					.create(
						'action',      this.action,
						'ancillaries', ancillaries,
						'hover',       this.hover,
						'light',       this.light,
						'mark',        ScreenSpace.filterMark( mark ),
						'mode',        mode,
						'resolution',  this.resolution,
						'seamSpace',   seam.space,
						'showGrid',    this.showGrid,
						'showGuides',  this.showGuides,
						'sizeDisplay', this.sizeDisplay,
						'snapping',    this.snapping,
						'systemFocus', this.systemFocus,
						'userCreds',   userCreds,
						'windows',     windows,
					)
				);
			}
		}

		case ModeForm:
		{
			const win0 = windows.get( 0 );
			let aSpace;
			if( win0.ti2ctype === WindowSpace )
			{
				aSpace = ancillaries.get( win0.uid );
			}

			return(
				ScreenForm
				.create(
					'action',         this.action,
					'colorScheme',    this.colorScheme,
					'hover',          this.hover,
					'light',          this.light,
					'mark',           ScreenForm.filterMark( mark ),
					'mode',           mode,
					'publicReadable', aSpace?.publicReadable,
					'resolution',     this.resolution,
					'seam',           this.seam.forms,
					'showGrid',       this.showGrid,
					'showGuides',     this.showGuides,
					'sizeDisplay',    this.sizeDisplay,
					'snapping',       this.snapping,
					'systemFocus',    this.systemFocus,
					'userCreds',      userCreds,
					'userInfo',       this.userInfo,
					'windows',        windows,
				)
			);
		}

		default: throw new Error( );
	}
};
