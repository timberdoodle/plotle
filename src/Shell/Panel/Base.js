/*
| Abstract parent of panels.
*/
def.abstract = true;

def.attributes =
{
	// facet of the panel
	facet: { type: [ 'undefined', 'Shell/Facet/Self' ] },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// rectangle of view window
	rectView: { type: 'gleam:Rect' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// shape of the panel
	shape: { type: [ 'undefined', '<gleam:Shape/Types' ] },

	// designed size
	size: { type: [ 'undefined', 'gleam:Size' ] },

	// current view size (total screen size)
	sizeDisplay: { type: 'gleam:Size' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// transform
	transform: { type: [ '<gleam:Transform/Types' ] },

	// traceV of the panel
	traceV: { type: 'ti2c:Trace' },
};

import { Self as GlintPane   } from '{gleam:Glint/Pane}';
import { Self as GlintWindow } from '{gleam:Glint/Window}';
import { Self as Hover       } from '{Shell/Result/Hover}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';
import { Self as Point       } from '{gleam:Point}';
import { Self as Segment     } from '{gleam:Segment}';

/*
| Checks if the user clicked something on the panel.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) )
	{
		return undefined;
	}

	const pp = p.sub( tZone.pos );
	if( !this._tOutline.within( pp ) )
	{
		return undefined;
	}

	// this is on the panel
	for( let widget of this.widgets.reverse( ) )
	{
		const bubble = widget.click( pp, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	return true;
};

/*
| Move during a dragging operation.
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Start of a dragging operation.
*/
def.proto.dragStart =
	function( p, shift, ctrl, action )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) ) return undefined;
	const pp = p.sub( tZone.pos );

	if( !this._tOutline.within( pp ) )
	{
		return undefined;
	}

	// it's on the panel
	for( let widget of this.widgets )
	{
		const bubble = widget.dragStart( pp, shift, ctrl );
		if( bubble ) return bubble;
	}

	// the dragging operation is on the panel
	// but it denies it.

	return false;
};

/*
| A button has been dragStarted.
*/
def.proto.dragStartButton =
	function( trace )
{
	return false;
};

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;
	return(
		mark
		? this.widgets.get( mark.traceVWidget.last.key )
		: undefined
	);
};

/*
| The panel's glint.
*/
def.lazy.glint =
	function( )
{
	const resolution = this.resolution;
	const list = [ this.facet.glintFill( this.tZone ) ];

	for( let widget of this.widgets )
	{
		const g = widget.glint;
		if( g ) list.push( g );
	}

	const zone = this.tZone;
	const zoneE1 = zone.envelope( 1 );

	list.push(
		this.facet.glintBorder(
			Segment.P0P1(
				Point.XY( zone.width, 0 ),
				Point.XY( zone.width, zone.height + 1 ),
			)
		)
	);

	return(
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', ListGlint.Array( list ),
					'resolution', resolution,
					'size', zoneE1.size,
				),
			'pos', zoneE1.pos
		)
	);
};

/*
| User inputed characters.
*/
def.proto.input =
	function( string )
{
	// nothing
};

/*
| Mouse wheel.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) )
	{
		return undefined;
	}

	if( !this._tOutline.within( p.sub( tZone.pos ) ) )
	{
		return undefined;
	}

	return true;
};

/*
| Returns hover result if point is on the panel.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	// shortcut if p is not near the panel
	if( !this.tZone.within( p ) )
	{
		return undefined;
	}

	const pp = p.sub( this.tZone.pos );
	if( !this._tOutline.within( pp ) )
	{
		return undefined;
	}
	// this is on the panel

	for( let widget of this.widgets.reverse( ) )
	{
		const bubble = widget.pointingHover( pp, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	return Hover.CursorDefault;
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.proto.probeClickDrag =
	function( p, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) )
	{
		return undefined;
	}

	const pp = p.sub( tZone.pos );
	// if p is not on the panel
	if( !this._tOutline.within( pp ) )
	{
		return undefined;
	}

	return 'atween';
};

/*
| User is pressing a special key.
*/
def.proto.specialKey =
	function( key, shift, ctrl)
{
	// nothing
};
