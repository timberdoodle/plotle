/*
| A shell design.
|
| Specified per space.
*/
def.attributes =
{
	background:       { type: 'gleam:Color' },
	fontDesign:       { type: 'function'    },
	light:            { type: 'boolean'     },
	nameFontStandard: { type: 'string'      },
};

import { Self as Color    } from '{gleam:Color}';
import { Self as FontRoot } from '{gleam:Font/Root}';

/*
| Returns a font of the standard family by size and color.
*/
def.proto.fontStandardBland =
	function( size )
{
	return(
		this._fontStandardFamily
		.FontBland( size )
	);
};

/*
| Returns a font of the standard family by size and color.
*/
def.proto.fontStandardColor =
	function( size, color )
{
	return(
		this.fontStandardBland( size )
		.FontColor( color || Color.black )
	);
};

/*
| The standard font family.
*/
def.lazy._fontStandardFamily =
	function( )
{
	return FontRoot.get( this.nameFontStandard );
};

/*
| The standard font family.
*/
def.lazyFunc._fontFamily =
	function( name )
{
	return FontRoot.get( name );
};
