/*
| A checkbox design.
*/
def.attributes =
{
	// the checked icon
	iconChecked: { type: 'Shell/Design/Icon/Self' },

	// style facets
	facets: { type: 'Shell/Facet/List' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
