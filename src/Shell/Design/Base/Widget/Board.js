/*
| A button design.
*/
def.attributes =
{
	// style facets
	facets: { type: 'Shell/Facet/List' },

	// shape of the button
	// or a 'string' name that calculates the shape from the zone
	shape:
	{
		type:
		[
			'gleam:Ellipse',
			'gleam:RectRound',
			'gleam:RectRoundExt',
			'string',
		],
	},

	// if false not visibile on start
	visible: { type: 'boolean', defaultValue: 'true' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
