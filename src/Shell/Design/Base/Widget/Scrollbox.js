/*
| A scrollbox design.
*/
def.attributes =
{
	// widgets
	widgets: { type: 'twig@<Shell/Design/Base/Widget/Types' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
