/*
| A label design.
*/
def.attributes =
{
	// horizonal alignment
	// "left", "right", "center"
	align: { type: 'string', defaultValue: '"left"' },

	// vertical alignment
	base: { type: 'string', defaultValue: '"alphabetic"' },

	// designed font
	font: { type: [ 'undefined', 'gleam:Font/Color' ] },

	// vertical distance of newline
	newline: { type: [ 'undefined', 'number' ] },

	// designed position
	pos: { type: 'gleam:Point' },

	// the label string
	string: { type: 'string' },
};

const checkAlign = Object.freeze( { left: true, center: true, right: true } );
const checkBase = Object.freeze( { alphabetic: true, middle: true } );

/*
| Exta checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( !checkAlign[ this.align ] ) throw new Error( );
/**/	if( !checkBase[ this.base ] ) throw new Error( );
/**/}
};
