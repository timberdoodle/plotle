/*
| A button design.
*/
def.attributes =
{
	// style facets
	facets: { type: 'Shell/Facet/List' },

	// colorized font of the string
	font: { type: [ 'undefined', 'gleam:Font/Color' ] },

	// icon
	icon: { type: [ 'undefined', 'Shell/Design/Icon/Self' ] },

	// true if the icon rendering is to be not grid fitted
	iconNoGrid: { type: [ 'undefined', 'boolean' ] },

	// shape of the button
	// or a 'string' name that calculates the shape from the zone
	shape:
	{
		type:
		[
			'gleam:Ellipse',
			'gleam:RectRound',
			'gleam:RectRoundExt',
			'string',
		],
	},

	// the string written in the button
	string: { type: 'string', defaultValue: '""' },

	// vertical distance of newline
	stringNewline: { type: [ 'undefined', 'number' ] },

	// rotation of the string
	stringRotation: { type: [ 'undefined', '<gleam:Angle/Types' ] },

	// if false not visibile on start
	visible: { type: 'boolean', defaultValue: 'true' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
