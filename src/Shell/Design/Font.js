/*
| Font designs.
*/
def.abstract = true;

import { Self as Color    } from '{gleam:Color}';
import { Self as FontRoot } from '{gleam:Font/Root}';

/*
| Shortcuts.
*/
def.static.Size =
	( size ) =>
	Self._familyStandard.FontBland( size ).FontColor( Color.black );

def.static.SizeBland =
	( size ) =>
	Self._familyStandard.FontBland( size );

def.static.SizeColor =
	( size, color ) =>
	Self._familyStandard.FontBland( size ).FontColor( color );

/*
| Shortcut.
*/
def.static.NameSizeLight =
	function( name, size, light )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/}

	let color, family;

	switch( name )
	{
		case 'form':
			color =
				light
				? Color.black
				: Color.RGB( 229, 229, 299 );
			family = Self._familyStandard;
			break;

		case 'standard':
			color = Color.black;
			family = Self._familyStandard;
			break;

		case 'panel':
			color = Color.black;
			family = Self._familyDejaVuSans;
			break;

		default:
			throw new Error( );
	}


	return family.FontBland( size ).FontColor( color );
};

/*
| Standard family.
*/
def.staticLazy._familyStandard =
	( ) =>
	FontRoot.get( 'NotoSans-Regular' );

/*
| DejaVuSans family.
*/
def.staticLazy._familyDejaVuSans =
	( ) =>
	FontRoot.get( 'DejaVuSans-Regular' );
