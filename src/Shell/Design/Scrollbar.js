/*
| Scrollbar design.
*/
def.abstract = true;

import { Self as Color           } from '{gleam:Color}';
import { Self as Facet           } from '{Shell/Facet/Self}';
import { Self as FacetBorder     } from '{Shell/Facet/Border}';

/*
| The facet
|
| ~light: if true light variant, otherwise dark.
*/
def.staticLazyFunc.facet =
	( light ) =>
	Facet.create(
		'fill', Color.RGB( 255, 188, 87 ),
		'border', FacetBorder.Color( Color.RGB( 221, 154, 52 ) )
	);

/*
| Ellipse cap.
*/
def.static.ellipseA = 6;
def.static.ellipseB = 6;

/*
| Width of the scrollbar.
*/
def.static.strength = 12;

/*
| Minimum height.
*/
def.static.minHeight = 12;
