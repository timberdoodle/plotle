/*
| Highlighting of items
*/
def.abstract = true;

import { Self as Color        } from '{gleam:Color}';
import { Self as Facet        } from '{Shell/Facet/Self}';
import { Self as FacetBorder  } from '{Shell/Facet/Border}';
import { Self as GroupBoolean } from '{group@boolean}';

def.staticLazy.facet =
	( ) =>
	Facet.create(
		'border', FacetBorder.ColorDistanceWidth( Color.RGBA( 255, 170, 0, 0.45 ), 1, 3 ),
		'specs', GroupBoolean.Table( { highlight: true } ),
	);
