/*
| The "select" icon.
|
|    <ce->
|    <cs>|  <hd>
|    |  ||  |  |
| nw . .--  ----  --. . pne
|     /              \
|    :                :
|
|    |                |
|    |                |
|
|    :                ;
|     \              /
| psw. `-- ---.   --' . pse
|
*/
def.abstract = true;

import { Self as Path  } from '{gleam:Path}';
import { Self as Point } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazy.figure =
	( ) =>
{
	const pc = Point.zero;

	const hd = 2; // half dash line
	const hs = 8; // half size
	const cs = 3; // corner size
	const ce = 4; // corner extension

	const pn = pc.add( 0, -hs );
	const pe = pc.add( hs, 0 );
	const pnw = pc.add( -hs, -hs );
	const pne = pc.add(  hs, -hs );
	const pse = pc.add(  hs,  hs );
	const psw = pc.add( -hs,  hs );
	const ps = pc.add( 0, hs );
	const pw = pc.add( -hs, 0 );

	return(
		Path.Plan(
			'pc', pc,
			'start',       pnw.add(   0,  ce ),
			'line',        pnw.add(   0,  cs ),
			'qbend',       pnw.add(  cs,   0 ),
			'line',        pnw.add(  ce,   0 ),
			'line', 'fly',  pn.add( -hd,   0 ),
			'line',         pn.add(  hd,   0 ),
			'line', 'fly', pne.add( -ce,   0 ),
			'line',        pne.add( -cs,   0 ),
			'qbend',       pne.add(   0,  cs ),
			'line',        pne.add(   0,  ce ),
			'line', 'fly',  pe.add(   0, -hd ),
			'line',         pe.add(   0,  hd ),
			'line', 'fly', pse.add(   0, -ce ),
			'line',        pse.add(   0, -cs ),
			'qbend',       pse.add( -cs,   0 ),
			'line',        pse.add( -ce,   0 ),
			'line', 'fly',  ps.add(  hd,   0 ),
			'line',         ps.add( -hd,   0 ),
			'line', 'fly', psw.add(  ce,   0 ),
			'line',        psw.add(  cs,   0 ),
			'qbend',       psw.add(   0, -cs ),
			'line', 'fly',  pw.add(   0,  hd ),
			'line',         pw.add(   0, -hd ),
			'line', 'fly', 'close'
		)
	);
};
