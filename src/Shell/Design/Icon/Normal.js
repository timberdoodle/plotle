/*
| The "normal" icon.
|
|
|     A
|     **
|     ***
|     ****
|     *****
|     ******
|     *******
|     **F**C*B
|     G   **
|          **
|           ED
|
| Currently unused.
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const ap = Point.XY( -4, -9 );

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
		),

		'shape',
		Path.Plan(
			'pc', Point.zero,
			'start', ap,                // A
			'line', ap.add(  11,  10 ), // B
			'line', ap.add(   6,  11 ), // C
			'line', ap.add(   9,  17 ), // D
			'line', ap.add(   7,  18 ), // E
			'line', ap.add(   4,  12 ), // F
			'line', ap.add(   0,  15 ), // G
			'line', 'close'
		)
	);
};
