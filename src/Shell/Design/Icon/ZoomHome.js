/*
| The zoom home icon.
|
|
|
|  ###    **
|  ###  ******
|  ###*** ** ****
|  ##** ****** ****
| *** ********** ***
|** ************** **
|   **************
|   **************
|   *******   ****
|   *******   ****
|   *******   ****
|   *******   ****
|   *******   ****
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as FigureList  } from '{gleam:Figure/List}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const c = Point.XY( 0, 2 );

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
			'border', FacetBorder.Color( Color.darkRed )
		),

		'shape',
		FigureList.Elements(
			Path.Plan(
				'pc', c,
				'start', c.add( -6,  4 ),
				'line', c.add(  -6, -2 ),
				'line', c.add(   0, -9 ),
				'line', c.add(   6, -2 ),
				'line', c.add(   6,  4 ),
				'line', c.add(   2,  4 ),
				'line', c.add(   2, -4 ),
				'line', c.add(  -2, -4 ),
				'line', c.add(  -2,  4 ),
				'line', 'close'
			),
			Path.Plan(
				'pc', c,
				'start', c.add( -8,  -3 ),
				'line', c.add( -10,  -3 ),
				'line', c.add(  -7,  -7 ),
				'line', c.add(  -7, -13 ),
				'line', c.add(  -4, -13 ),
				'line', c.add(  -4,  -9 ),
				'line', c.add(   0, -12 ),
				'line', c.add(   8,  -3 ),
				'line', c.add(  10,  -3 ),
				'line', c.add(   0, -14 ),
				'line', 'close'
			)
		)
	);
};
