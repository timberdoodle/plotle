/*
| The "check" icon.
|
|                C
|              .*'
|             .*'
|            .*'
|    A.     .*'
|    '*.   .*'
|     '**D**'
|      '***'
|       'B'
*/
def.abstract = true;

import { Self as Color      } from '{gleam:Color}';
import { Self as DesignIcon } from '{Shell/Design/Icon/Self}';
import { Self as Facet      } from '{Shell/Facet/Self}';
import { Self as Path       } from '{gleam:Path}';
import { Self as Point      } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const pc = Point.XY( -2, 0 );

	return DesignIcon.create(
		'facet',
		Facet.create( 'fill', Color.black ),

		'shape',
		Path.Plan(
			'pc', pc,
			'start', pc.add( -5, -3  ), // A
			'line',  pc.add(  2,  5  ), // B
			'line',  pc.add( 14, -12 ), // C
			'line',  pc.add(  2, -1  ), // D
			'line',  'close'            // A
		)
	);
};
