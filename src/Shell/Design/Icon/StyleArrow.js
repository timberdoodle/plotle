/*
| The stroke style as Arrow.
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.designArrow =
	( light ) =>
{
	const pc = Point.zero;
	const pne = pc.add(  6, -6 );
	const psw = pc.add( -6,  6 );
	const pnb = pne.add( -5, 5 );

	return DesignIcon.create(
		'facet',
			Facet.create(
				'border', FacetBorder.simpleBlack,
				'fill',   Color.black,
			),
		'shape',
		Path.Plan(
			'pc', pc,
			'start', psw,
			'line', pnb,
			'line', pne.add(  -5, 2 ),
			'line', pne,
			'line', pne.add(  -2, 5 ),
			'line', pnb,
		),
	);
};

/*
| Design.
*/
def.staticLazyFunc.designArrowDual =
	( light ) =>
{
	const pc = Point.zero;
	const pne = pc.add(  6, -6 );
	const psw = pc.add( -6,  6 );
	const pnb = pne.add( -5,  5 );
	const psb = psw.add(  5, -5 );

	return DesignIcon.create(
		'facet',
			Facet.create(
				'border', FacetBorder.simpleBlack,
				'fill',   Color.black,
			),
		'shape',
		Path.Plan(
			'pc', pc,
			'start', psb,
			'line', psw.add(  5, -2 ),
			'line', psw,
			'line', psw.add(  2, -5 ),
			'line', psb,
			'line', pnb,
			'line', pne.add(  -5, 2 ),
			'line', pne,
			'line', pne.add(  -2, 5 ),
			'line', pnb,
		),
	);
};

/*
| Design.
*/
def.staticLazyFunc.designArrowReverse =
	( light ) =>
{
	const pc = Point.zero;
	const pne = pc.add(  6, -6 );
	const psw = pc.add( -6,  6 );
	const psb = psw.add(  5, -5 );

	return DesignIcon.create(
		'facet',
			Facet.create(
				'border', FacetBorder.simpleBlack,
				'fill',   Color.black,
			),
		'shape',
		Path.Plan(
			'pc', pc,
			'start', psb,
			'line', psw.add(  5, -2 ),
			'line', psw,
			'line', psw.add(  2, -5 ),
			'line', psb,
			'line', pne,
		),
	);
};

/*
| Design.
*/
def.staticLazyFunc.designLine =
	( light ) =>
{
	const pc = Point.zero;
	const pne = pc.add(  6, -6 );
	const psw = pc.add( -6,  6 );

	return DesignIcon.create(
		'facet',
			Facet.create(
				'border', FacetBorder.simpleBlack,
				'fill',   Color.black,
			),
		'shape',
		Path.Plan(
			'pc', pc,
			'start', psw,
			'line', pne,
		),
	);
};
