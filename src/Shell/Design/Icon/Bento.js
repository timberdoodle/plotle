/*
| The bento icon (for menues).
|
|  ---    ---    ---
| |   |  |   |  |   |
|  ---    ---    ---
|  ---    ---    ---
| |   |  |   |  |   |
|  ---    ---    ---
|  ---    ---    ---
| |   |  |   |  |   |
|  ---    ---    ---
*/
def.abstract = true;

import { Self as Color      } from '{gleam:Color}';
import { Self as DesignIcon } from '{Shell/Design/Icon/Self}';
import { Self as Ellipse    } from '{gleam:Ellipse}';
import { Self as Facet      } from '{Shell/Facet/Self}';
import { Self as FigureList } from '{gleam:Figure/List}';
import { Self as Point      } from '{gleam:Point}';
import { Self as Size       } from '{gleam:Size}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const size = Size.WH( 4, 4 );
	const d = 7;

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
		),
		'shape',
		FigureList.Elements(
			Ellipse.CenterSize( Point.XY( -d, -d ), size ),
			Ellipse.CenterSize( Point.XY(  0, -d ), size ),
			Ellipse.CenterSize( Point.XY(  d, -d ), size ),
			Ellipse.CenterSize( Point.XY( -d,  0 ), size ),
			Ellipse.CenterSize( Point.zero,         size ),
			Ellipse.CenterSize( Point.XY(  d,  0 ), size ),
			Ellipse.CenterSize( Point.XY( -d,  d ), size ),
			Ellipse.CenterSize( Point.XY(  0,  d ), size ),
			Ellipse.CenterSize( Point.XY(  d,  d ), size ),
		)
	);
};
