/*
| The options icon, profile and kebab.
|
|               ---
|     ---      |   |
|    |   |      ---
|     ---       ---
|   .-----.    |   |
|  /       \    ---
| '         '   ---
| |         |  |   |
| +---------+   ---
*/
def.abstract = true;

import { Self as Ellipse    } from '{gleam:Ellipse}';
import { Self as FigureList } from '{gleam:Figure/List}';
import { Self as Path       } from '{gleam:Path}';
import { Self as Point      } from '{gleam:Point}';
import { Self as Size       } from '{gleam:Size}';

/*
| Design.
*/
def.staticLazy.figure =
	( ) =>
{
	// size profile body
	const spb = Size.WH( 12, 8 );

	// size profile head
	const sph = 7;

	// size kebab
	const skb = Size.WH( 4, 4 );

	// distance of kebab
	const dk = 6;

	// center point of profile body
	const ppb = Point.XY( -3, 0 );

	// center point of profile head
	const pph = Point.XY( -3, -5 );

	// center point of kebab
	const pk = Point.XY( 7, 0 );

	return(
		FigureList.Elements(
			// profile body
			Path.Plan(
				'start', ppb.add( -spb.width / 2, spb.height ),
				'qbend', ppb,
				'qbend', ppb.add(  spb.width / 2, spb.height ),
				'line', 'close',
			),
			// profile head
			Ellipse.CenterSize( pph, Size.WH( sph, sph ) ),
			// kebab
			Ellipse.CenterSize( pk, skb ),
			Ellipse.CenterSize( pk.add( 0,  dk ), skb ),
			Ellipse.CenterSize( pk.add( 0, -dk ), skb ),
		)
	);
};
