/*
| The zoom all icon.
|
|  ******   ******
|  ****       ****
|  ** **     ** **
|  **  **   **  **
|       ** vw
|        **z        A    A    A
|       ** **       V p  '    '
|  **  **   **  **       '    '
|  ** **     ** **       ' t  '
|  ****       ****       V    ' s
|  ******   ******            V
|
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const c = Point.zero;
	const s = 8;
	const t = s - 1;
	const u = s - 3;
	const n = t - 1;
	const p = 3;
	const v = 1;
	const w = 2;
	const z = 1;

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
			'border', FacetBorder.Color( Color.darkRed )
		),

		'shape',
		Path.Plan(
			'pc', c,
			'start', c.add( -s, -s ),
			'line',  c.add( -p, -s ),
			'line',  c.add( -p, -t ),
			'line',  c.add( -u, -n ),
			'line',  c.add( -v, -w ),

			'line',  c.add(  0, -z ),

			'line',  c.add(  v, -w ),
			'line',  c.add(  u, -t ),
			'line',  c.add(  p, -t ),
			'line',  c.add(  p, -s ),
			'line',  c.add(  s, -s ),
			'line',  c.add(  s, -p ),
			'line',  c.add(  t, -p ),
			'line',  c.add(  n, -u ),
			'line',  c.add(  w, -v ),

			'line',  c.add(  z,  0 ),

			'line',  c.add(  w,  v ),
			'line',  c.add(  t,  u ),
			'line',  c.add(  t,  p ),
			'line',  c.add(  s,  p ),
			'line',  c.add(  s,  s ),
			'line',  c.add(  p,  s ),
			'line',  c.add(  p,  t ),
			'line',  c.add(  u,  n ),
			'line',  c.add(  v,  w ),

			'line',  c.add(  0,  z ),

			'line',  c.add( -v,  w ),
			'line',  c.add( -u,  n ),
			'line',  c.add( -p,  t ),
			'line',  c.add( -p,  s ),
			'line',  c.add( -s,  s ),
			'line',  c.add( -s,  p ),
			'line',  c.add( -t,  p ),
			'line',  c.add( -n,  u ),
			'line',  c.add( -w,  v ),

			'line',  c.add( -z,  0 ),

			'line',  c.add( -w, -v ),
			'line',  c.add( -n, -u ),
			'line',  c.add( -t, -p ),
			'line',  c.add( -s, -p ),
			'line', 'close'
		),
	);
};
