/*
| The hand icon.
|
|        fw
|       <-->
|
|        A              ^
|        *.B            |
|       |  |            |
|       |  |            |
|       |  |..          | fh
|       |  |DE|..       |
|  ..   |  |  |GH|..    |
| | Q'  |  |  |  |JK|   |
| \P  ` |  C  F  I  |   v -- fy
|  \   `|           |
|   *O  '           |
|    .  R          L,
|    .N           M/
|     `------------
|
| Currently unused.
*/
import { Self as Color        } from '{gleam:Color}';
import { Self as GenericShape } from '{gleam:Shape/Generic}';
import { Self as Facet        } from '{Shell/Facet/Self}';
import { Self as FacetBorder  } from '{Shell/Facet/Border}';
import { Self as Point        } from '{gleam:Point}';

def.lazyStatic.facet =
	( ) =>
	Facet.create(
		'fill', Color.RGBA( 255, 255, 180, 0.4 ),
		'border', FacetBorder.Color( Color.black )
	);


def.staticLazy.shape =
	function( )
{
	const ap = Point.XY( -4, -12 );
	const fh = 12;
	const fw = 3;
	const fy = ap.y + fh;
	const bp = ap.add( fw, 0 );
	const cp = bp.create( 'y', fy );
	const dp = cp.add( 0, -6 );
	const ep = dp.add( fw, 0 );
	const fp = ep.create( 'y', fy );
	const gp = fp.add( 0, -5 );
	const hp = gp.add( fw, 0 );
	const ip = hp.create( 'y', fy );
	const jp = ip.add( 0, -4 );
	const kp = jp.add( fw, 0 );
	const lp = ap.create( 'x', kp.x, 'y', ap.y + fh + 8 );
	const mp = lp.add( -3, 3 );
	const np = mp.add( -4 * fw + 4, 0 );
	const op = np.add( -2, -2 );
	const rp = ap.add( 0, fh + 3 );
	const qp = rp.add( -3, -6 );
	const pp = qp.add( -3, 2 );

	return( GenericShape.Plan(
		'pc', Point.zero,
		'start', ap, // A
		'qbend', bp, // B
		'line',  cp, // C
		'line',  dp, // D
		'qbend', ep, // E
		'line',  fp, // F
		'line',  gp, // G
		'qbend', hp, // H
		'line',  ip, // I
		'line',  jp, // J
		'qbend', kp, // K
		'line',  lp, // L
		'qbend', mp, // M
		'line',  np, // N
		'line',  op, // OO
		'line',  pp, // P
		'qbend', qp, // Q
		'line',  rp, // R
		'line', 'close'
	) );
};
