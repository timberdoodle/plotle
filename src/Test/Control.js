/*
| Controls for tests.
*/
def.abstract = true;

import { fork }  from 'node:child_process';
import fs        from 'fs/promises';
import puppeteer from 'puppeteer';

import { Self as FabricSpace } from '{Shared/Fabric/Space}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as GlintMask   } from '{gleam:Glint/Mask}';
import { Self as GlintPane   } from '{gleam:Glint/Pane}';
import { Self as GlintWindow } from '{gleam:Glint/Window}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';
import { Self as Plan        } from '{Shared/Trace/Plan}';
import { Self as Point       } from '{gleam:Point}';
import { Self as Rect        } from '{gleam:Rect}';
import { Self as Trace       } from '{ti2c:Trace}';

// true if in process of aborting the server.
let aborting = false;

// handle of puppeteer browser.
let browser;

// handle of puppeteer main page.
let page;

// server abort control.
let control;

// promise to resolve when waiting on a server message.
let promiseServerCmdResolve;

// handle of server.
let server;

/*
| Data about event queue
| to wait for specific events.
*/
const dataWait =
{
	event:   undefined,
	queue:   [ ],
	resolve: undefined,
	source:  undefined,
};

/*
| Cleans up possibly left over testdb.
*/
async function cleanup( )
{
	console.log( '+++ deleting old testdb' );

	let filenames;
	try
	{
		filenames = await fs.readdir( './testdb' );
	}
	catch( e )
	{
		// ignore
	}

	// if it's undefined there is no testdb.
	if( !filenames || filenames.length === 0 )
	{
		return;
	}

	const proms = [ ];
	for( let file of filenames )
	{
		proms.push( fs.unlink( './testdb/' + file ) );
	}

	await Promise.all( proms );
}

/*
| Performs a click.
*/
def.static.click =
	async function( what, what2 )
{
	switch( what.ti2ctype )
	{
		case Point:
		{
			await page.mouse.click( what.x, what.y );
			return;
		}

		case Trace:
		{
			let [ glintWhat, pos ] = await Self.getGlint( what );

			switch( glintWhat.ti2ctype )
			{
				case GlintFigure:
				{
					pos = pos.add( glintWhat.figure.pc );
					if( what2 )
					{
						pos = pos.add( what2 );
					}
					await page.mouse.click( pos.x, pos.y );
					return;
				}

				case GlintWindow:
				{
					if( what2 )
					{
						pos = pos.add( what2 );
					}
					else
					{
						pos = pos.add( glintWhat.pane.size.zeroRect.pc );
					}

					await page.mouse.click( pos.x, pos.y );
					return;
				}

				default: throw new Error( );
			}
		}

		default: throw new Error( );
	}
};

/*
| Performs a drag.
*/
def.static.drag =
	async function( from, to )
{
	await page.mouse.move( from.x, from.y );
	await page.mouse.down( );
	await page.mouse.move( to.x, to.y );
	await page.mouse.up( );
};

/*
| Gets space fabric from server.
*/
def.static.getSpaceFabric =
	async function( ref )
{
	if( promiseServerCmdResolve ) throw new Error( );

	server.send(
		{
			cmd: 'getSpaceFabric',
			ref: ref,
		}
	);

	const msg =
		await new Promise( ( resolve, reject ) =>
		{
			promiseServerCmdResolve = resolve;
		} );

	return FabricSpace.FromJson( JSON.parse( msg.space ), Plan.space );
};

/*
| Message from server or shell.
*/
function message( msg )
{
	const event = msg.event;
	const source = msg.source;

	if( dataWait.source === source && dataWait.event === event )
	{
		const resolve = dataWait.resolve;
		dataWait.source  = undefined;
		dataWait.event   = undefined;
		dataWait.resolve = undefined;
		resolve( msg );
	}
	else
	{
		const queue = dataWait.queue;

		// overwrites a previous event of same type?
		for( let a = 0, alen = queue.length; a < alen; a++ )
		{
			const msg = queue[ a ];
			if( msg.source === source && msg.event === event )
			{
				queue.splice( a, 1 );
				break;
			}
		}

		queue.push( msg );
	}
}

/*
| Message from shell.
*/
function messageShell( msg )
{
	if( !msg.event ) throw new Error( );

	if( msg.source ) throw new Error( );

	//console.log( 'MESSAGE SHELL', msg.event );

	msg.source = 'shell';
	message( msg );
}

/*
| Message from server.
*/
function messageServer( msg )
{
	if( msg.reply )
	{
		promiseServerCmdResolve( msg );
	}
	else if( msg.event )
	{
		if( msg.source ) throw new Error( );
		msg.source = 'server';
		message( msg );
	}
	else
	{
		throw new Error( );
	}
}

/*
| Clears an event type from the queue.
|
| Returns true if it was cleared or false
| if the event wasn't in the queue
*/
def.static.clearEvent =
	function( source, event )
{
	const queue = dataWait.queue;
	for( let a = 0, alen = queue.length; a < alen; a++ )
	{
		const msg = queue[ a ];

		if( msg.event === event )
		{
			queue.splice( a, 1 );
			return true;
		}
	}

	return false;
};

/*
| Waits for a specific event.
*/
def.static.event =
	async function( source, event )
{
	if(
		arguments.length !== 2
		|| ( source !== 'shell' && source !== 'server' )
	) throw new Error( );

	// if we are already waiting something is broken.
	if( dataWait.resolve ) throw new Error( );

	// looks if it's in the buffer
	const queue = dataWait.queue;
	for( let a = 0, alen = queue.length; a < alen; a++ )
	{
		const msg = queue[ a ];

		if( msg.event === event )
		{
			queue.splice( a, 1 );
			return msg;
		}
	}

	dataWait.source = source;
	dataWait.event  = event;

	return(
		new Promise( ( resolve, reject ) =>
		{
			dataWait.resolve = resolve;
		} )
	);
};

/*
| Finds the glint named 'name'.
|   ~glint: glint to search in
|   ~name:  name of the glint to find
|   ~pos:   offset pos of the glint
|
| Returns [ glint, pos ]:
|   ~glint: the found glint
|   ~pos:   position of the glint
*/
def.static.findGlint =
	function( glint, name, pos )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/}

	switch( glint.ti2ctype )
	{
		case ListGlint:
		{
			for( let g of glint )
			{
				const [ fg, fpos ] = Self.findGlint( g, name, pos );
				if( fg )
				{
					return [ fg, fpos ];
				}
			}
			return [ undefined, pos ];
		}

		case GlintMask:
		case GlintPane:
		{
			if( glint.name === name )
			{
				return [ glint, pos ];
			}
			else
			{
				return Self.findGlint( glint.glint, name, pos );
			}
		}

		case GlintWindow:
		{
			pos = pos.add( glint.pos );

			if( glint.name === name )
			{
				return [ glint, pos ];
			}
			else
			{
				return Self.findGlint( glint.pane, name, pos );
			}
		}

		default:
		{
			return[
				glint.name === name ? glint : undefined,
				pos,
			];
		}
	}
};

/*
| Returns the current glint root.
| For this it redraws the page.
*/
def.static.getGlint =
	async function( what )
{
	Self.clearEvent( 'shell', 'draw' );
	await page.evaluate( 'globalThis.redraw( );' );
	const msg = await Self.event( 'shell', 'draw' );
	const glint = ListGlint.FromJson( JSON.parse( msg.glint ) );

	if( !what )
	{
		return [ glint, Point.zero ];
	}

	switch( what.ti2ctype )
	{
		case Trace:
			return Self.findGlint( glint, what.asString, Point.zero );

		default:
			throw new Error( );
	}
};

/*
| Presses on the keyboard.
*/
def.static.press =
	async function( key )
{
	await page.keyboard.press( key );
};

/*
| Starts the test system
|
| ~head: if true runs puppetter not in headless mode.
*/
def.static.start =
	async function( head )
{
	await cleanup( );

	console.log( '+++ starting server' );

	control = new AbortController( );

	server =
		fork(
			'src/Server/Start.js',
			[ 'puppet' ],
			{
				signal: control.signal,
				stdio: 'pipe',
			}
		);

	server.on( 'error',
		( err ) =>
		{
			if( aborting && err.code === 'ABORT_ERR' )
			{
				// it's fine.
				return;
			}
			else
			{
				// it's not.
				console.log( 'Server error:', err );
				process.exit( 1 );
			}
		}
	);

	server.on( 'close',
		( err, ecode ) =>
		{
			if( aborting )
			{
				aborting = false;
			}
			else
			{
				console.log( 'Server unexpected close', ecode, err );
				process.exit( 1 );
			}
		}
	);

	server.on( 'message', messageServer );

	server.send(
		{
			cmd: 'config',
			config:
			[
				'import cfg from "config";',
				'',
				'cfg.admin                 = "test";',
				'cfg.network.main.protocol = "http";',
				'cfg.shell.bundle.enable   = false;',
				'cfg.shell.superSampling   = 1;',
				'cfg.database.name         = "./testdb";',
				'cfg.database.establish    = true;',
				'cfg.server.sensitive      = true;',
				'cfg.server.cache          = false;',
			].join( '\n' ),
		}
	);

	// waiting for server to be loaded
	await Self.event( 'server', 'running' );

	console.log( '+++ starting puppeteer' );

	browser = await puppeteer.launch(
		{
			args: [ '--no-sandbox', '--disable-setuid-sandbox' ],
			defaultViewport: { width: 1920 / 2, height: 1080 / 2 },
			headless: !head,
		}
	);

	[ page ] = await browser.pages( );

	page.on( 'pageerror',
		( err ) =>
		{
			console.log( 'Shell error:', err );
			if( !head )
			{
				process.exit( 1 );
			}
		}
	);

	console.log( '+++ puppeteer goto shell' );

	await page.exposeFunction( 'PUPPET', messageShell );
	await page.goto( 'http://127.0.0.1:8833/devel.html' );
	await Self.event( 'shell', 'acquire space' );
};

/*
| Stops the test system.
*/
def.static.stop =
	async function( )
{
	console.log( '+++ stoping server' );

	aborting = true;
	control.abort( );
	browser.close( );
};

/*
| Types on the keyboard.
*/
def.static.type =
	async function( string )
{
	await page.keyboard.type( string );
};
