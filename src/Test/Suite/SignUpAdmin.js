/*
| The test signs up the admin user.
*/
def.abstract = true;

import { Self as Control   } from '{Test/Control}';
import { Self as Plan      } from '{Shared/Trace/Plan}';
import { Self as Trace     } from '{ti2c:Trace}';

/*
| Runs the test.
*/
def.static.run =
	async function( )
{
	const traceRoot  = Trace.root( Plan.root );
	const traceView0 = traceRoot.add( 'view', 0 );

	// presses the opts button
	await Control.click( traceRoot.add( 'float', 'opts' ) );

	// presses the signup button
	await Control.click(
		traceView0
		.add( 'form', 'opts' )
		.add( 'widgets', 'buttonSignup' )
	);

	await Control.click(
		traceView0
		.add( 'form', 'signUp' )
		.add( 'widgets', 'inputUsername' )
	);

	await Control.type( 'test' );
	await Control.press( 'Tab' );
	await Control.press( 'Tab' );
	await Control.type( 'abc12' );
	await Control.press( 'Tab' );
	await Control.type( 'abc12' );

	Control.clearEvent( 'shell', 'onSignUp' );
	Control.clearEvent( 'shell', 'acquire space' );

	await Control.click(
		traceView0
		.add( 'form', 'signUp' )
		.add( 'widgets', 'buttonSignup' )
	);

	await Control.event( 'shell', 'onSignUp' );

	{
		const [ glintHeadline ] =
			await Control.getGlint(
				traceView0
				.add( 'form', 'welcome' )
				.add( 'widgets', 'headline' )
			);

		if( glintHeadline.string !== 'Welcome test' )
		{
			throw new Error( );
		}
	}

	const e = await Control.event( 'shell', 'acquire space' );

	await Control.click(
		traceView0
		.add( 'form', 'welcome' )
		.add( 'widgets', 'buttonClose' )
	);

	return e.uid;
};
