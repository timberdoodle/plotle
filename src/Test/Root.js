/*
| The test suite root.
*/
def.abstract = true;

//import timers from 'node:timers/promises';

import { Self as Control         } from '{Test/Control}';
import { Self as TestSignUpAdmin } from '{Test/Suite/SignUpAdmin}';
import { Self as TestCreateStuff } from '{Test/Suite/CreateStuff}';

/*
| Runs the tests.
*/
async function run( )
{
	console.log( '--- test SignUpAdmin' );
	const uidPlotleHome = await TestSignUpAdmin.run( );

	console.log( '--- test CreateStuff' );
//	const tsp =
		await TestCreateStuff.run( uidPlotleHome );
//	console.log( tsp );

	console.log( '+++ OK' );
	await Control.stop( );
}

/*
| Starts the tests.
|
| ~dir: root dir of plotle server.
*/
def.static.init =
	async function( dir )
{
	let head = false;

	if( process.argv.length >= 3 )
	{
		switch( process.argv[ 2 ] )
		{
			case 'head':
				head = true;
				break;

			default: throw new Error( 'invalid argument' );
		}
	}

	await Control.start( head );

	if( head )
	{
		try
		{
			await run( );
		}
		catch( e )
		{
			console.log( 'OOPS' );
			console.log( e );
		}
	}
	else
	{
		await run( );
	}
};
