module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2022": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "CHECK": true,
        "ERUDA": true,
        "FAILSCREEN": true,
        "NODE": true,
        "PUPPET": true,
        "config": true,
        "def": true,
        "docx": true,
        "insight": true,
        "opentype": true,
        "pass": true,
        "Self": true,
        "ti2c": true,
        "ti2c_onload": true
    },
    "parserOptions": {
        "ecmaVersion": 2022,
        "sourceType": "module"
    },
    "rules": {
        "consistent-return": "error",
        "eol-last": "error",
        "indent": [
            "off",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-console": "off",
        "no-empty": [ "error", { "allowEmptyCatch": true } ],
        "no-trailing-spaces": "error",
        "no-unused-vars": [
            "error",
            {
                "args" : "none",
                "varsIgnorePattern" : "^tim_proto$"
            },
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
